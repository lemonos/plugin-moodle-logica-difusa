/**
 * @namespace
 */
M.local_fuzzylogic = M.local_fuzzylogic || {
    api: M.cfg.wwwroot+'/local/fuzzylogic/ajax.php',
    graph_type: null,
    globalcriteria: false
};

M.local_fuzzylogic.drawAssessmentGrapth = function (Y, datasets) {
    var i = 0;
    
    $.each(datasets, function(key, val) {
        val.color = i;
        ++i;
    });

    // insert checkboxes 
    var choiceContainer = $("#choices");
    $.each(datasets, function(key, val) {
        choiceContainer.append("<input type='checkbox' name='" + key +
            "' checked='checked' id='id" + key + "'></input>&nbsp;" +
            "<label for='id" + key + "'>"
            + val.label + "</label>&nbsp;");
    });

    choiceContainer.find("input").click(plotAccordingToChoices);

    function showTooltip(x, y, contents) {
        $("<div id='tooltip'>" + contents + "</div>").css({
            position: "absolute",
            display: "none",
            top: y + 5,
            left: x + 5,
            border: "1px solid #fdd",
            padding: "2px",
            "background-color": "#fee",
            opacity: 0.80
        }).appendTo("body").fadeIn(200);
    }

    var previousPoint = null;
    $("#placeholder").bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.dataIndex) {

                    previousPoint = item.dataIndex;

                    $("#tooltip").remove();
                    var x = item.datapoint[0].toFixed(2),
                    y = item.datapoint[1].toFixed(2);

                    showTooltip(item.pageX, item.pageY,
                        item.series.label + " de " + x + " = " + y);
                }
            } else {
                $("#tooltip").remove();
                previousPoint = null;            
            }

    });


    function plotAccordingToChoices() {

        var data = [];

        choiceContainer.find("input:checked").each(function () {
            var key = $(this).attr("name");
            if (key && datasets[key]) {
                data.push(datasets[key]);
            }
        });

        if (data.length > 0) {
            $.plot("#placeholder", data, {
                yaxis: {
                    min: 0,
                    max: 1
                },
                xaxis: {
                    min: 0,
                    max: 1
                },
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                grid: {
                    hoverable: true,
                    clickable: true
                },
            });
        }
    }

    plotAccordingToChoices();
    
}


M.local_fuzzylogic.drawSurfaceGraph = function (datasets, axislabel) {
   var surfacePlot;
   var data, options, basicPlotOptions, glOptions, numRows, numCols; 

    numRows = numCols = datasets.length;

    var tooltipStrings = new Array();
    
    data = {nRows: numRows, nCols: numCols, formattedValues: datasets};

    surfacePlot =  new SurfacePlot(document.getElementById("entriesplaceholder"));

    // Don't fill polygons in IE < v9. It's too slow.
    var fillPly = true;

    // Define a colour gradient.
    var colour1 = {red:0, green:0, blue:255};
    var colour2 = {red:0, green:255, blue:255};
    var colour3 = {red:0, green:255, blue:0};
    var colour4 = {red:255, green:255, blue:0};
    var colour5 = {red:255, green:0, blue:0};
    var colours = [colour1, colour2, colour3, colour4, colour5];

    // Axis labels.
    var xAxisHeader	= "X-"+axislabel[0];
    var yAxisHeader	= "Y-"+axislabel[1];
    var zAxisHeader	= "Z-Score";

    var renderDataPoints = false;
    var background = '#ffffff';
    var axisForeColour = '#000000';
    var hideFloorPolygons = true;
    var chartOrigin = {x: 150, y:150};

    // Options for the basic canvas pliot.
    basicPlotOptions = {fillPolygons: fillPly, tooltips: tooltipStrings, renderPoints: renderDataPoints }

    // Options for the webGL plot.
    var xLabels = [0, 0.2, 0.4, 0.6, 0.8, 1];
    var yLabels = [0, 0.2, 0.4, 0.6, 0.8, 1];
    var zLabels = [0, 0.2, 0.4, 0.6, 0.8, 1]; // These labels ar eused when autoCalcZScale is false;
    glOptions = {xLabels: xLabels, yLabels: yLabels, zLabels: zLabels, chkControlId: "allowWebGL", autoCalcZScale: false, animate: true};

    // Options common to both types of plot.
    options = {xPos: 0, yPos: 0, width: 600, height: 550, colourGradient: colours, 
        xTitle: xAxisHeader, yTitle: yAxisHeader, zTitle: zAxisHeader, 
        backColour: background, axisTextColour: axisForeColour, hideFlatMinPolygons: hideFloorPolygons, origin: chartOrigin};

    surfacePlot.draw(data, options, basicPlotOptions, glOptions);
    
}


M.local_fuzzylogic.draw2Dentries = function (datasets) {
    var i = 0;
   
    $.each(datasets, function(key, val) {
        val.color = i;
        ++i;
    });


    function showTooltip(x, y, contents) {
        $("<div id='tooltip'>" + contents + "</div>").css({
            position: "absolute",
            display: "none",
            top: y + 5,
            left: x + 5,
            border: "1px solid #fdd",
            padding: "2px",
            "background-color": "#fee",
            opacity: 0.80
        }).appendTo("body").fadeIn(200);
    }

    var previousPoint = null;
    $("#entriesplaceholder").bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.dataIndex) {

                    previousPoint = item.dataIndex;

                    $("#tooltip").remove();
                    var x = item.datapoint[0].toFixed(2),
                    y = item.datapoint[1].toFixed(2);

                    showTooltip(item.pageX, item.pageY,
                        x + " , " + y);
                }
            } else {
                $("#tooltip").remove();
                previousPoint = null;            
            }

    });



    var data = [];

    data.push(datasets);


    if (data.length > 0) {
        $.plot("#entriesplaceholder", data, {
            yaxis: {
                min: 0,
                max: 1
            },
            xaxis: {
                min: 0,
                max: 1
            },
            series: {
                lines: {
                    show: true
                },
                points: {
                    show: true
                }
            },
            grid: {
                hoverable: true,
                clickable: true
            },
        });
    }

    
}



M.local_fuzzylogic.initRulesGraph = function (Y, globalcriteria) {
    M.local_fuzzylogic.globalcriteria = globalcriteria;
    
    var elements = Y.all("#form input[type=text]");
        elements.each( function() {
            this.set("value", '');
            this.set("disabled", true);
        });
        
    Y.one("#form input[type=button]").on('click', M.local_fuzzylogic.get_data );
    
    Y.all("#form input[type=checkbox]").on('click', function(e) {
        var elements = Y.all("#form input[type=checkbox]");
        var numelements = 0;
        var numchecked = 0;
        elements.each( function() {
            numelements++;
            if ( this.get("checked") ) {
                numchecked++;
                this.next("input[type=text]").set("disabled", false);
            } else {
                this.next("input[type=text]").set("disabled", true);
                this.next("input[type=text]").set("value", '');
            }
        });
        if ((numelements-1) == numchecked) {
            var elements = Y.all("#form input[type=checkbox]");
            elements.each( function() {
                if( !this.get("checked") ){
                    this.set("disabled", true);
                    this.next("input[type=text]").set("disabled", true);
                }
            });
        } else {
             var elements = Y.all("#form input[type=checkbox]");
             elements.each( function() {
                if( this.get("disabled") ){
                    this.set("disabled", false);
                }
            });
        }
    } );
    
}
M.local_fuzzylogic.get_data = function () {
    Y.one("#entriesplaceholder").set('innerHTML', '');
    
    var num_ranges = 0;
    
    var elements = Y.all("#form input[type=checkbox]");
    var criteriaid = Y.one("#form input[type=hidden]").get("value")
    var scores = new Array();
    elements.each( function() {
        if ( this.get("checked") ) {
           scores.push( new Array(this.get('value'), this.next("input[type=text]").get("value")) );
        } else {
           scores.push( new Array(this.get('value'), 'range') ); 
           num_ranges++;
        }
    });
    
    
    
    if (scores.length<2) {
        //alert(M.util.get_string('js_minentries', 'local_fuzzylogic'));
        alert('Cómo mínimo tienen que existir dos entradas al criterio.');
        return;
    }
    
    if (scores.length>3) {
        //alert(M.util.get_string('js_maxentries', 'local_fuzzylogic'));
        alert('Cómo máximo pueden existir tres entradas para el criterio.');
        return;
    }
    
    var numscores = 0;
    var numscoresnull = 0;
    for (var i=0; i<scores.length; i++){
        numscores++;
        var const_value = scores[i][1];
        if (isNaN(const_value) || const_value == '' || typeof const_value == 'undefined' || const_value == null) numscoresnull++;
    }
    
    
    for (var i=0; i<scores.length; i++){
        var const_value = scores[i][1];
        
        if (const_value <0 || const_value > 1) {
            //alert(M.util.get_string('js_constantrangeincorrect', 'local_fuzzylogic'));
            alert('Los valores constantes tienen que estar comprendidos en el rango entre los valores 0 y 1');
            return; 
         }

        if (numscores==numscoresnull ) {
            //alert(M.util.get_string('js_noconstantselect', 'local_fuzzylogic'));
            alert('Es necesario seleccionar al menos una entrada de la lista e indicar su valor constante (entre 0 y 1)');
            return;
         }
    }
    
    
    switch (num_ranges) {
        case 1:
            M.local_fuzzylogic.graph_type = '2D';
            break;
        case 2:
            M.local_fuzzylogic.graph_type = '3D';
            break;
            
        default:
            alert('El número de rangos [1..0] definidos para calcular el grafico tiene que estar comprendido entre 1 y 2');
            return;
    }
    
    Y.io(M.local_fuzzylogic.api, {
        method : 'POST',
        data : build_querystring({
            scores: JSON.stringify(scores),
            cid: criteriaid,
            global: M.local_fuzzylogic.globalcriteria

        }),
        on : {
            success : M.local_fuzzylogic.get_data_callback
        },
        context : M.local_fuzzylogic
    });
    
    var btn = Y.one("#form input[type=button]");
    btn.set('value', 'Calculando...');
    btn.set('disabled', true);
    
    

}

M.local_fuzzylogic.get_data_callback =  function(tid, outcome) {
    try {
        var data = Y.JSON.parse(outcome.responseText);
    } catch (ex) {
        return;
    }
    
    var axislabel = new Array();
    var elements = Y.all("#form input[type=checkbox]");
        elements.each( function() {
            if (this.get('checked') == false) {
               var label = this.next("label").get('innerHTML');
              axislabel.push( label ); 
            }
        });
    var btn = Y.one("#form input[type=button]");
    btn.set('value', 'Generar Gráfico');
    btn.set('disabled', false);
    
    

     switch (M.local_fuzzylogic.graph_type){
        case '3D':
            M.local_fuzzylogic.drawSurfaceGraph (data.data, axislabel);
            break;
        case '2D':
            M.local_fuzzylogic.draw2Dentries (data.data);
            break;
        default:
            alert('No hay gráfico definido para este número de entradas al criterio');
    }
    
}

