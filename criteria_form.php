<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The form used at the guide editor page is defined here
 *
 * @package    local_fuzzylogic
 * @copyright  2013 Oscar Ruesga <oscar@ruesga.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/lib/formslib.php');


/**
 * Defines the criterias edit form
 *
 * @package    local_fuzzylogic
 * @copyright  2013 Oscar Ruesga <oscar@ruesga.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class local_fuzzylogic_criteria_form extends moodleform {

    /**
     * Form element definition
     */
    public function definition() {
        $form = $this->_form;
        
        $editoroptions = $this->_customdata['editoroptions'];
        $criteria = $this->_customdata['data'];       
      

        $form->addElement('hidden', 'cid', $this->_customdata['cid']);
        $form->setType('cid', PARAM_INT);
        
        $form->addElement('hidden', 'structureid', $this->_customdata['structureid']);
        $form->setType('structureid', PARAM_INT);
        
        $form->addElement('hidden', 'action', $this->_customdata['action']);
        $form->setType('action', PARAM_ALPHA);
        
        $form->addElement('hidden', 'id');
        $form->setType('id', PARAM_INT);
        
        $form->addElement('hidden', 'g', $this->_customdata['g']);
        $form->setType('id', PARAM_INT);
        
          
        // Entries
        
        
        if ($this->_customdata['g'] == 1) {
          
          //En caso de que el criterio no exista garantizo que tenga structureid
          if ( empty($criteria->id) && empty($criteria->structureid) ){
            $criteria = new stdClass();
            $criteria->structureid = $this->_customdata['structureid'];
          }
          if ( local_fuzzylogic_logic_globalcriteria_exists($criteria) ) {
            $url = new moodle_url('/local/fuzzylogic/criteria.php', array('cid'=>$this->_customdata['cid'], 'action'=>FUZZYLOGIC_ACTION_LIST));
            error(get_string('errorglobalcriteriaexist','local_fuzzylogic'), $url);
          }
          
          $options = array(0=>get_string('entry_global_criteria_selection', 'local_fuzzylogic'));
          
          $criterias = local_fuzzylogic_get_criterias($this->_customdata['structureid']);
          foreach ($criterias['criterias'] as $c){
            //if ($criteria->id != $c->id) 
              $options[$c->id] = '(' . $c->shortname . ') ' . $c->name;
          }
          
          $entrytype = 'criteria';
          $noentries = get_string('nocriteriasentries', 'local_fuzzylogic');
          
          $form->addElement('hidden', 'global', 1);
          $form->setType('global', PARAM_BOOL);
          
          $entriestr = get_string('criteria','local_fuzzylogic');
        } else {
          $options = array(0=>get_string('entry_criteria_selection', 'local_fuzzylogic'));
          
          $structureconcepts = local_fuzzylogic_get_conceptstructure($this->_customdata['structureid'], true);
          
          foreach ($structureconcepts as $structureconcept){
            $options[$structureconcept->id] = '(' . $structureconcept->shortname . ') ' . $structureconcept->name;
          }
          
          $entrytype = 'concept';
          $noentries = get_string('noconceptsentries', 'local_fuzzylogic');
          
          $form->addElement('hidden', 'global', 0);
          $form->setType('global', PARAM_BOOL);
          
          $entriestr = get_string('concept','local_fuzzylogic');
        }
        
        if (count ($options) > 1) {
          $form->addElement('header', 'entryshdr', get_string('entries', 'local_fuzzylogic'));
          $r = array();
          $roptions = array();
          $r[] = $form->createElement('hidden', 'entry_id', $criteria->entry_id);
          $roptions['entry_id']['type'] = PARAM_INT;
          $r[] = $form->createElement('select', 'entry_entryid',  $entriestr . ' {no}', $options);
          $roptions['entry_entryid']['type'] = PARAM_INT;
          $r[] = $form->createElement('hidden', 'entry_entrytype', $entrytype);
          $roptions['entry_entrytype']['type'] = PARAM_ALPHA;
          $r[] = $form->createElement('hidden', 'entry_criteriaid', $criteria->id);
          $roptions['entry_criteriaid']['type'] = PARAM_INT;
          
        $count = local_fuzzylogic_count_criteriaentries($criteria->id, $entrytype)+1;

        $this->repeat_elements($r, $count, $roptions, 'numentry',
                'addentry', 1, get_string('addentry', 'local_fuzzylogic'), true);
        } else {
          $form->addElement('header', 'entryshdr', get_string('entries', 'local_fuzzylogic'));
          $form->addElement('static', 'entrystatic', $noentries);
        }
        
        
        
        
        $form->addElement('header', 'criteriashdr', get_string('criteria', 'local_fuzzylogic'));
         // Name.
        $form->addElement('text', 'name', get_string('name', 'local_fuzzylogic'), array('size'=>52));
        $form->addRule('name', get_string('required'), 'required');
        $form->setType('name', PARAM_TEXT);
        
         // Shortname.
        $form->addElement('text', 'shortname', get_string('shortname', 'local_fuzzylogic'), array('size'=>40));
        $form->addRule('shortname', get_string('required'), 'required');
        $form->setType('shortname', PARAM_TEXT);
        
        // Description.
        $form->addElement('editor', 'description_editor', get_string('description', 'local_fuzzylogic'), null, $editoroptions);
        $form->setType('description_editor', PARAM_RAW);
        
        
        // Assesments
        $repeated = array();
        $repeatedoptions = array();
        $repeated[] = $form->createElement('header', 'assessmenthdr', get_string('assessment', 'local_fuzzylogic') . ' {no}');
        $repeated[] = $form->createElement('text', 'assess_linguistictag', get_string('linguistictag', 'local_fuzzylogic'));
        $repeated[] = $form->createElement('textarea', 'assess_feedback', get_string('feedback', 'local_fuzzylogic'),
                array('rows' => 5));
        $repeatedoptions['assess_feedback']['type'] = PARAM_RAW;
        
        $repeated[] = $form->createElement('hidden', 'assess_id', $id);
        $repeatedoptions['assess_id']['type'] = PARAM_INT;
        $repeated[] = $form->createElement('hidden', 'assess_instanceid', $criteria->id);
        $repeatedoptions['assess_instanceid']['type'] = PARAM_INT;
        $repeated[] = $form->createElement('hidden', 'assess_assesmenttype', 'criteria');
        $repeatedoptions['assess_assesmenttype']['type'] = PARAM_ALPHA;
        
        $repeated[] = $form->createElement('text', 'assess_param_a', get_string('parama', 'local_fuzzylogic'), array('size' => 2));
        $repeated[] = $form->createElement('text', 'assess_param_b', get_string('paramb', 'local_fuzzylogic'), array('size' => 2));
        $repeated[] = $form->createElement('text', 'assess_param_c', get_string('paramc', 'local_fuzzylogic'), array('size' => 2));
        $repeated[] = $form->createElement('text', 'assess_param_d', get_string('paramd', 'local_fuzzylogic'), array('size' => 2));
        $repeated[] = $form->createElement('text', 'assess_param_e', get_string('parame', 'local_fuzzylogic'), array('size' => 2));
        $repeatedoptions['assess_param_a']['rule'] = 'numeric';
        $repeatedoptions['assess_param_b']['rule'] = 'numeric';
        $repeatedoptions['assess_param_c']['rule'] = 'numeric';
        $repeatedoptions['assess_param_d']['rule'] = 'numeric';
        $repeatedoptions['assess_param_e']['rule'] = 'numeric';

        // Get full version (including condition info) of section object 
        $count = local_fuzzylogic_count_assesstments($criteria->id, 'criteria') + 1;

        $this->repeat_elements($repeated, $count, $repeatedoptions, 'numassessments',
                'addassessment', 1, get_string('addassessment', 'local_fuzzylogic'), false);

        
        $this->add_action_buttons();
        
        $this->set_data($criteria);
    }

    /**
     * Setup the form depending on current values. This method is called after definition(),
     * data submission and set_data().
     * All form setup that is dependent on form values should go in here.
     *
     * We remove the element status if there is no current status (i.e. guide is only being created)
     * so the users do not get confused
     */
    public function definition_after_data() {
       
    }

    /**
     * Form validation.
     * If there are errors return array of errors ("fieldname"=>"error message"),
     * otherwise true if ok.
     *
     * @param array $data array of ("fieldname"=>value) of submitted data
     * @param array $files array of uploaded files "element_name"=>tmp_file_path
     * @return array of "element_name"=>"error_description" if there are errors,
     *               or an empty array if everything is OK (true allowed for backwards compatibility too).
     */
    public function validation($data, $files) {
      global $DB;  
      
      $err = parent::validation($data, $files);
      $err = array();
      $form = $this->_form;
 
      if ( local_fuzzylogic_criteria_exists($data) ) {
        $err['shortname'] = get_string('duplicateshortname', 'local_fuzzylogic');
      };
      
      foreach ($data['assess_id'] as $num=>$value){
        $valuea = $data['assess_param_a'][$num];
        if ($valuea<0 || $valuea>1){
          $err['assess_param_a['.$num.']'] = get_string('invalidrange', 'local_fuzzylogic');
        }
        
        $valueb = $data['assess_param_b'][$num];
        if ($valueb<0 || $valueb>1){
          $err['assess_param_b['.$num.']'] = get_string('invalidrange', 'local_fuzzylogic');
        }
        
        if ($valueb < $valuea){
          $err['assess_param_b['.$num.']'] = get_string('minorvalue', 'local_fuzzylogic');
        }
        
        $valuec = $data['assess_param_c'][$num];
        if ($valuec<0 || $valuec>1){
          $err['assess_param_c['.$num.']'] = get_string('invalidrange', 'local_fuzzylogic');
        }
        
        if ($valuec < $valueb){
          $err['assess_param_c['.$num.']'] = get_string('minorvalue', 'local_fuzzylogic');
        }
        
        $valued = $data['assess_param_d'][$num];
        if ($valued<0 || $valued>1){
          $err['assess_param_d['.$num.']'] = get_string('invalidrange', 'local_fuzzylogic');
        }
        
        if ($valued < $valuec){
          $err['assess_param_d['.$num.']'] = get_string('minorvalue', 'local_fuzzylogic');
        }
        
        $valuee = $data['assess_param_e'][$num];
        if (!empty($valuee) && ($valuee<0 || $valuee>1)){
          $err['assess_param_e['.$num.']'] = get_string('invalidrange', 'local_fuzzylogic');
        }
        
      }
        
      return $err;
    }

    /**
     * Return submitted data if properly submitted or returns NULL if validation fails or
     * if there is no submitted data.
     *
     * @return object submitted data; NULL if not valid or not submitted or cancelled
     */
    public function get_data() {
        $data = parent::get_data();

        return $data;
    }

}
