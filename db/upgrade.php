<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Upgrade script for the local fuzzylogic module.
 *
 * @package    local_fuzzylogic
 * @copyright  2013 Oscar Ruesga <oscar@ruesga.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


/**
 * @global moodle_database $DB
 * @param int $oldversion
 * @return bool
 */
function xmldb_local_fuzzylogic_upgrade($oldversion) {
    global $CFG, $DB;

    $dbman = $DB->get_manager();
    
    if ($oldversion < 2013072600) {

        // Define table fuzzylogic_results to be created
        $table = new xmldb_table('fuzzylogic_results');

        // Adding fields to table fuzzylogic_results
        $table->add_field('id', XMLDB_TYPE_INTEGER, '18', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '18', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('structureid', XMLDB_TYPE_INTEGER, '18', null, XMLDB_NOTNULL, null, null);
        $table->add_field('instanceid', XMLDB_TYPE_INTEGER, '18', null, XMLDB_NOTNULL, null, null);
        $table->add_field('type', XMLDB_TYPE_INTEGER, '9', null, XMLDB_NOTNULL, null, null);
        $table->add_field('linguistictag', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('score', XMLDB_TYPE_FLOAT, '12', null, XMLDB_NOTNULL, null, null);
        $table->add_field('date', XMLDB_TYPE_INTEGER, '18', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table fuzzylogic_results
        $table->add_key('id', XMLDB_KEY_PRIMARY, array('id'));

        // Adding indexes to table fuzzylogic_results
        $table->add_index('userid', XMLDB_INDEX_NOTUNIQUE, array('userid'));
        $table->add_index('structureid', XMLDB_INDEX_NOTUNIQUE, array('structureid'));
        $table->add_index('instanceid', XMLDB_INDEX_NOTUNIQUE, array('instanceid'));

        // Conditionally launch create table for fuzzylogic_results
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // fuzzylogic savepoint reached
        upgrade_plugin_savepoint(true, 2013072600, 'local', 'fuzzylogic');
    }
    
    if ($oldversion < 2013081504) {

        // Define field attemptid to be added to fuzzylogic_results
        $table = new xmldb_table('fuzzylogic_results');
        $field = new xmldb_field('attemptid', XMLDB_TYPE_INTEGER, '18', null, XMLDB_NOTNULL, null, '0', 'instanceid');

        // Conditionally launch add field attemptid
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        
        $index = new xmldb_index('attemptid', XMLDB_INDEX_NOTUNIQUE, array('attemptid'));

        // Conditionally launch add index attemptid
        if (!$dbman->index_exists($table, $index)) {
            $dbman->add_index($table, $index);
        }

        // fuzzylogic savepoint reached
        upgrade_plugin_savepoint(true, 2013081504, 'local', 'fuzzylogic');
    }
    
     if ($oldversion < 2013081600) {

        // Define table fuzzylogic_results_log to be created
        $table = new xmldb_table('fuzzylogic_results_log');

        // Adding fields to table fuzzylogic_results_log
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '18', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('structureid', XMLDB_TYPE_INTEGER, '18', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('attemptid', XMLDB_TYPE_INTEGER, '18', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('log', XMLDB_TYPE_TEXT, null, null, null, null, null);
        $table->add_field('globalfeedback', XMLDB_TYPE_TEXT, null, null, null, null, null);
        $table->add_field('date', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table fuzzylogic_results_log
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Adding indexes to table fuzzylogic_results_log
        $table->add_index('userid', XMLDB_INDEX_NOTUNIQUE, array('userid'));
        $table->add_index('structureid', XMLDB_INDEX_NOTUNIQUE, array('structureid'));
        $table->add_index('attemptid', XMLDB_INDEX_NOTUNIQUE, array('attemptid'));

        // Conditionally launch create table for fuzzylogic_results_log
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // fuzzylogic savepoint reached
        upgrade_plugin_savepoint(true, 2013081600, 'local', 'fuzzylogic');
    }
    
     if ($oldversion < 2013081803) {
         $table = new xmldb_table('fuzzylogic_results');
         
        // Define field linguistictag to be dropped from fuzzylogic_results
        $field = new xmldb_field('linguistictag');

        // Conditionally launch drop field linguistictag
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        
         // Define field bestassessmentid to be added to fuzzylogic_results
        $field = new xmldb_field('bestassessmentid', XMLDB_TYPE_INTEGER, '18', null, null, null, null, 'score');

        // Conditionally launch add field bestassessmentid
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }


        // fuzzylogic savepoint reached
        upgrade_plugin_savepoint(true, 2013081803, 'local', 'fuzzylogic');
    }

    if ($oldversion < 2013081901) {

        // Changing precision of field score on table fuzzylogic_results to (4, 2)
        $table = new xmldb_table('fuzzylogic_results');
        $field = new xmldb_field('score', XMLDB_TYPE_FLOAT, '4, 2', null, XMLDB_NOTNULL, null, null, 'type');

        // Launch change of precision for field score
        $dbman->change_field_precision($table, $field);

        // fuzzylogic savepoint reached
        upgrade_plugin_savepoint(true, 2013081901, 'local', 'fuzzylogic');
    }
    
    if ($oldversion < 2013091305) {

        // Define field param_e to be added to fuzzylogic_assessment
        $table = new xmldb_table('fuzzylogic_assessment');
        $field = new xmldb_field('param_e', XMLDB_TYPE_FLOAT, '2, 1', null, null, null, null, 'param_d');

        // Conditionally launch add field param_e
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // fuzzylogic savepoint reached
        upgrade_plugin_savepoint(true, 2013091305, 'local', 'fuzzylogic');
    }


    return true;
}


