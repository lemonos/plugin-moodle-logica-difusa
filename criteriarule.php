<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Criteria rules editor Page
 *
 * @package    local_fuzzylogic
 * @copyright  2013 Oscar Ruesga <oscar@ruesga.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once (dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/lib.php');
require_once(dirname(__FILE__).'/locallib.php');
require_once(dirname(__FILE__).'/criteriarule_form.php');

$contextid = required_param('cid', PARAM_INT);
$criteriaid = required_param('criteriaid', PARAM_INT);
$global = optional_param('g', 0, PARAM_INT);



list($context, $course, $cm) = get_context_info_array($contextid);

require_login($course, true);
require_capability('local/fuzzylogic:manage', $context);

$criteria = (!empty($criteriaid)) ? local_fuzzylogic_get_criteria ($criteriaid) : null;



$criteriaurl = new moodle_url('/local/fuzzylogic/criteria.php', array('cid'=>$context->id,'structureid'=>$criteria->structureid));

$strheading = get_string('criteriarule_definition', 'local_fuzzylogic');
$PAGE->set_url(new moodle_url('/local/fuzzylogic/criteriarule.php', array('cid' => $contextid, 'criteriaid'=>$criteriaid)));
$PAGE->set_title($strheading);
$PAGE->set_heading(get_string('criteria_definition', 'local_fuzzylogic'));
$PAGE->set_pagelayout('standard');
$PAGE->navbar->add(get_string('criteria_definition', 'local_fuzzylogic'), $criteriaurl );


//Obtenemos valoraciones del criterio
$opt_criteria_assess = array();
$criteriaassesments = local_fuzzylogic_get_assessments($criteria->id, 'criteria');
foreach ($criteriaassesments as $criteriaassesment){
  $opt_criteria_assess[$criteriaassesment->id] = $criteriaassesment->linguistictag;
}


//Generamos las reglas actuales
$rulegenerator = new local_fuzzylogic_criteriarule_generator($criteria->id);
$rulelist = $rulegenerator->get_rule_list(true);



//Generamos matriz de reglas
$mform = new local_fuzzylogic_criteriarule_form(null, array(
                                                              'cid'=>$context->id, 
                                                              'criteria'=>$criteria,
                                                              'opt_criteria_assess'=> $opt_criteria_assess,
                                                              'criteriarules'=>$rulelist
                                                            ), 'post');



if ($mform->is_cancelled()) {
        redirect( $criteriaurl );
} else if ($mform->is_submitted() && $mform->is_validated() ) {
  $data = $mform->get_data();
  
  //Almaceno los valores de la t-norma y la t-conorma
  if ($data->criteriaid) {
      $updatecriteria = new stdClass();
      $updatecriteria->id = $data->criteriaid;
      $updatecriteria->t_norma = $data->tnorma;
      $updatecriteria->t_conorma = $data->tconorma;
      local_fuzzylogic_update_criteria($updatecriteria);

  } 
  
  //Almaceno las reglas
  foreach ($data->criteriarule_id as $num=>$value) {
    $rule = new stdClass();
    $rule->id= $data->criteriarule_id[$num];
    $rule->criteriaid= $criteria->id;
    $rule->consequent= $data->criteriarule_consequent[$num];
    
    if (!empty($rule->id)){
      local_fuzzylogic_update_criteriarule($rule);
    } else {
      $rule->id = local_fuzzylogic_add_criteriarule($rule);
    }
    
  }
  
  local_fuzzylogic_create_grade_structure ($structureid);
  
  redirect( $criteriaurl );
}
    
echo $OUTPUT->header();
echo $OUTPUT->heading($strheading);
$mform->display();
echo $OUTPUT->footer();