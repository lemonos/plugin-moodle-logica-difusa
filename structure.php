<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Fuzzy logic structure editor Page
 *
 * @package    local_fuzzylogic
 * @copyright  2013 Oscar Ruesga <oscar@ruesga.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once (dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/lib.php');
require_once(dirname(__FILE__).'/locallib.php');

$contextid = required_param('cid', PARAM_INT);
$action  = optional_param('action', FUZZYLOGIC_ACTION_LIST, PARAM_ALPHA );



list($context, $course, $cm) = get_context_info_array($contextid);

require_login($course, true);
require_capability('local/fuzzylogic:manage', $context);


$PAGE->set_url(new moodle_url('/local/fuzzylogic/structure.php', array('cid' => $contextid, 'action' => $action)));
$PAGE->set_title(get_string('structure_definition', 'local_fuzzylogic'));
$PAGE->set_heading(get_string('structure_definition', 'local_fuzzylogic'));
$PAGE->set_pagelayout('standard');




switch ($action) {
  case FUZZYLOGIC_ACTION_LIST:
    $page = optional_param('page', 0, PARAM_INT);
    $searchquery  = optional_param('search', '', PARAM_RAW);
    $structures = local_fuzzylogic_get_structures($course->id, $page, 25, $searchquery);
    
    echo $OUTPUT->header();
    echo $OUTPUT->heading(get_string('fuzzylogic_structures', 'local_fuzzylogic'));

    // Add search form.
    $search  = html_writer::start_tag('form', array('id'=>'searchstructurequery', 'method'=>'post'));
    $search .= html_writer::start_tag('div');
    $search .= html_writer::label(get_string('search_structure', 'local_fuzzylogic'), 'structure_search_q'); // No : in form labels!
    $search .= html_writer::empty_tag('input', array('id'=>'structure_search_q', 'type'=>'text', 'name'=>'search', 'value'=>$searchquery));
    $search .= html_writer::empty_tag('input', array('id'=>'cid', 'type'=>'hidden', 'name'=>'id', 'value'=>$contextid));
    $search .= html_writer::empty_tag('input', array('type'=>'submit', 'value'=>get_string('search', 'local_fuzzylogic')));
    $search .= html_writer::end_tag('div');
    $search .= html_writer::end_tag('form');
    echo $search;
    
    // Output pagination bar.
    $params = array('page' => $page);
    if ($contextid) {
        $params['cid'] = $contextid;
    }
    if ($search) {
        $params['search'] = $searchquery;
    }

    $params['action'] = FUZZYLOGIC_ACTION_LIST;
    
    $baseurl = new moodle_url('/local/fuzzylogic/structure.php', $params);
    echo $OUTPUT->paging_bar($structures['totalstructures'], $page, 25, $baseurl);
    
    $data = array();
    
    foreach($structures['structures'] as $structure) {
        
        $status_messages = local_fuzzylogic_validatestructure($structure->id, false);
        
        $status = (count($status_messages) > 0) ? get_string('review_elements','local_fuzzylogic') . html_writer::alist($status_messages) : get_string ('ok'); 
         
        $line = array();
        $line[] = format_string($structure->name);
        $line[] = format_text($structure->description, FORMAT_HTML);
        $line[] = $structure->totalconcepts;
        $line[] = $structure->totalcriteria;
        $line[] = $status;

        $buttons = array();
        $buttons[] = html_writer::link(new moodle_url('/local/fuzzylogic/structure.php', array('cid'=>$contextid, 'action'=>FUZZYLOGIC_ACTION_DELETE, 'structureid'=>$structure->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/delete'), 'alt'=>get_string('delete'), 'class'=>'iconsmall')));
        $buttons[] =  html_writer::link(new moodle_url('/local/fuzzylogic/structure.php', array('cid'=>$contextid, 'action'=>FUZZYLOGIC_ACTION_EDIT, 'structureid'=>$structure->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/edit'), 'alt'=>get_string('edit'), 'class'=>'iconsmall')));
        $buttons[] =  html_writer::link(new moodle_url('/local/fuzzylogic/criteria.php', array('cid'=>$contextid, 'structureid'=>$structure->id, 'action'=>FUZZYLOGIC_ACTION_LIST, )), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/unblock'), 'alt'=>get_string('edit'), 'class'=>'iconsmall')));
        if (has_capability('local/fuzzylogic:importstructure', $context) && !local_fuzzylogic_validatestructure($structure->id, true, false ) == false ) {
            $buttons[] =  html_writer::link(new moodle_url('/local/fuzzylogic/structure.php', array('cid'=>$contextid, 'structureid'=>$structure->id, 'action'=>FUZZYLOGIC_ACTION_XML_EXPORT, )), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/backup'), 'alt'=>get_string('exportxmlstructure', 'local_fuzzylogic'), 'class'=>'iconsmall')));
        }
        
        $line[] = implode(' ', $buttons);

        $data[] = $line;
    }
    $table = new html_table();
    $table->head  = array(get_string('name', 'local_fuzzylogic'),  get_string('description', 'local_fuzzylogic'),
                          get_string('totalconcepts', 'local_fuzzylogic'), get_string('totalcriteria', 'local_fuzzylogic'), get_string('status'), get_string('edit'));
    $table->size  = array('10%', '10%', '20%', '20%',  '30%','10%');
    $table->align = array('left', 'left', 'center', 'center','left','center');
    $table->width = '80%';
    $table->data  = $data;
    
    
    echo html_writer::table($table);
    echo $OUTPUT->paging_bar($structures['totalstructures'], $page, 25, $baseurl);
        
    echo $OUTPUT->single_button(new moodle_url('/local/fuzzylogic/structure.php', array('cid'=>$context->id, 'action'=>FUZZYLOGIC_ACTION_ADD)), get_string('addstructure', 'local_fuzzylogic'));
    if (has_capability('local/fuzzylogic:importstructure', $context)) {
        echo html_writer::empty_tag('br');
        
        
        
        require_once(dirname(__FILE__).'/structure_form.php');
        $import_form = new local_fuzzylogic_fileimport_form(null, array(
                                                                  'cid'=>$context->id, 
                                                                ), 'post');
        $import_form->display();
        
        
        echo $OUTPUT->box_start();
        echo $OUTPUT->heading( get_string('importstructurefromcourse', 'local_fuzzylogic') );
        echo $OUTPUT->single_button(new moodle_url('/local/fuzzylogic/structure.php', array('cid'=>$context->id, 'action'=>FUZZYLOGIC_ACTION_DUPLICATE)), get_string('import'));
        echo $OUTPUT->box_end();
        
        
    }
    break;
  case FUZZYLOGIC_ACTION_ADD:
  case FUZZYLOGIC_ACTION_EDIT:
    require_once(dirname(__FILE__).'/structure_form.php');
    
    $structureid = optional_param('structureid', 0, PARAM_INT);

    $structure = (!empty($structureid)) ? local_fuzzylogic_get_structure ($structureid) : null;
    
    $structure->totalconcepts = local_fuzzylogic_count_conceptstructure($structureid);
    
    if ($structure->totalconcepts > 0) {
      $concepts = local_fuzzylogic_get_conceptstructure($structure->id);
      
      $concept->concept_id = array();
      $concept->concept_structureid = array();
      $concept->concept_conceptid = array();
  
      foreach ($concepts as $concept){
        $structure->concept_id[] = $concept->id;
        $structure->concept_structureid[] = $concept->structureid;
        $structure->concept_conceptid[] = $concept->conceptid;
      }
    }
    
    $editoroptions = array('maxfiles'=>0, 'context'=>$context);
    if ($structure->id) {
        // Edit existing.
        $structure = file_prepare_standard_editor($structure, 'description', $editoroptions, $context);
        $strheading = get_string('editstructure', 'local_fuzzylogic');
        $action = FUZZYLOGIC_ACTION_EDIT;

    } else {
        // Add new.
        $structure = file_prepare_standard_editor($structure, 'description', $editoroptions, $context);
        $strheading = get_string('addstructure', 'local_fuzzylogic');
        $action = FUZZYLOGIC_ACTION_ADD;
    }
    
    $PAGE->set_title($strheading);
    $PAGE->set_heading($COURSE->fullname);
    $PAGE->navbar->add($strheading);
    $mform = new local_fuzzylogic_structure_form(null, array(
                                                              'cid'=>$context->id, 
                                                              'courseid'=>$course->id, 
                                                              'data'=>$structure, 
                                                              'editoroptions'=>$editoroptions,
                                                              'action'=> $action
                                                            ), 'post');

    if ($mform->is_cancelled()) {
        redirect( new moodle_url('/local/fuzzylogic/structure.php', array('cid'=>$context->id, 'action'=>FUZZYLOGIC_ACTION_LIST)) );
    } else if ($mform->is_submitted() && $mform->is_validated() ) {
      $data = $mform->get_data();
      $data = file_postupdate_standard_editor($data, 'description', $editoroptions, $context);

      if ($data->id) {
          local_fuzzylogic_update_structure($data);
     
      } else {
          $structureid = local_fuzzylogic_add_structure($data);
      }
      
      //Save concepts
      foreach ($data->concept_id as $num=>$value){
        $concept = new stdClass();
        $concept->id = $data->concept_id[$num];
        $concept->structureid = $data->concept_structureid[$num];
        $concept->conceptid = $data->concept_conceptid[$num];

        
        //Borro la valoración en caso de que venga vacia
        if (  $concept->conceptid == 0 ) 
        {
            if ($concept->id != 0){ 
               local_fuzzylogic_delete_conceptstructure($concept->id);
            }
        } else {
          $concept->structureid = ($concept->structureid==0) ? $structureid :  $concept->structureid;
        
          if ( local_fuzzylogic_conceptstructure_exists($concept) ) continue;
          
          if ($concept->id == 0) {
            unset($concept->id);
            local_fuzzylogic_add_conceptstructure($concept);
          } else {
            local_fuzzylogic_update_conceptstructure($concept);
          }
        }
        
        
      }
      
      local_fuzzylogic_create_grade_structure ($structureid);
      
      redirect( new moodle_url('/local/fuzzylogic/structure.php', array('cid'=>$context->id, 'action'=>FUZZYLOGIC_ACTION_LIST)) );
    }
    
    echo $OUTPUT->header();
    echo $OUTPUT->heading($strheading);
    $mform->display();
    break;
  

  
  CASE FUZZYLOGIC_ACTION_DELETE:
    $structureid = optional_param('structureid', 0, PARAM_INT);
    $confirm = optional_param('confirm', 0, PARAM_BOOL);
  
    if ($confirm and confirm_sesskey()) {
        local_fuzzylogic_delete_structure($structureid);
        $returnurl = new moodle_url('/local/fuzzylogic/structure.php', array('cid' => $contextid, 'action' => FUZZYLOGIC_ACTION_LIST));
        local_fuzzylogic_create_grade_structure ($structureid);
        redirect($returnurl);
    }
    $strheading = get_string('delstructure', 'local_fuzzylogic');
    $PAGE->navbar->add($strheading);
    $PAGE->set_title($strheading);
    $PAGE->set_heading($COURSE->fullname);
    echo $OUTPUT->header();
    echo $OUTPUT->heading($strheading);
    $deleteurl = new moodle_url('/local/fuzzylogic/structure.php', array('cid' => $contextid, 'structureid'=>$structureid, 'action' => FUZZYLOGIC_ACTION_DELETE, 'confirm'=>1, 'sesskey'=>sesskey()));
    $returnurl = new moodle_url('/local/fuzzylogic/structure.php', array('cid' => $contextid, 'action' => FUZZYLOGIC_ACTION_LIST));
    echo $OUTPUT->confirm(get_string('confirmdeletestructure', 'local_fuzzylogic'), $deleteurl, $returnurl);
    break;
  
  CASE FUZZYLOGIC_ACTION_DUPLICATE:
    require_capability('local/fuzzylogic:importstructure', $context);
    $source = optional_param('source', 0, PARAM_INT );
    if ($source > 0) {
        $xml = local_fuzzylogic_get_xml_structure($source);        
        $data = new SimpleXMLElement($xml);
        $result = local_fuzzylogic_import_xml_structure ($course->id, $data);
        
        if ($result == true) {
            $returnurl = new moodle_url('/local/fuzzylogic/structure.php', array('cid' => $contextid, 'action' => FUZZYLOGIC_ACTION_LIST));
            redirect($returnurl);
        } else {
            $returnurl = new moodle_url('/local/fuzzylogic/structure.php', array('cid' => $contextid, 'action' => FUZZYLOGIC_ACTION_LIST));
            error(get_string('error_importing_structure', 'local_fuzzylogic'), $returnurl);
        }
    } else {
        
        $sources = array();
        $structures = local_fuzzylogic_get_structures(null, 0, 10000);
        
        foreach ($structures['structures'] as $structure){
            if ($structure->courseid == $course->id) continue;
            
            if ( !array_key_exists($structure->courseid, $sources) ) {
                $item = array();        
                $item['name'] = $DB->get_field('course', 'fullname', array( 'id'=>$structure->courseid ) );
                $item['structures'] = array();
            } else {
               $item =  $sources[$structure->courseid];
            }
            $item['structures'][$structure->id] = $structure->name;
            
            $sources[$structure->courseid] = $item;
        }
        
        if (count($sources) <= 0) {
            $returnurl = new moodle_url('/local/fuzzylogic/structure.php', array('cid' => $contextid, 'action' => FUZZYLOGIC_ACTION_LIST));
            redirect($returnurl, get_string('no_structure_available', 'local_fuzzylogic'), 1);
        }
        
        $strheading = get_string('importstructure', 'local_fuzzylogic');
        $PAGE->set_title($strheading);
        $PAGE->set_heading($course->fullname);
        $PAGE->navbar->add($strheading);
        require_once(dirname(__FILE__).'/structure_form.php');
        $mform = new local_fuzzylogic_import_structure_form(null, array(
                                                                  'cid'=>$context->id, 
                                                                  'data'=>$sources
                                                                ), 'post');
       

        echo $OUTPUT->header();
        echo $OUTPUT->heading($strheading);
        echo html_writer::tag('div', get_string('select_sctructure', 'local_fuzzylogic') );
        $mform->display();
     }
     break;
    
   CASE FUZZYLOGIC_ACTION_XML_EXPORT:
       require_capability('local/fuzzylogic:importstructure', $context);
       $structureid = optional_param('structureid', 0, PARAM_INT);
       $xml = local_fuzzylogic_get_xml_structure ($structureid);
       header("Content-disposition: attachment; filename=fuzzylogic_structure_" . time() . ".xml");
       header('Content-type: application/xml');
       echo $xml;
       die;     
       break;
   
   CASE FUZZYLOGIC_ACTION_XML_IMPORT:
       require_capability('local/fuzzylogic:importstructure', $context);
       require_once(dirname(__FILE__).'/structure_form.php');
       $import_form = new local_fuzzylogic_fileimport_form(null, array(
                                                                  'cid'=>$context->id, 
                                                                ), 'post');
        
       if ($form = $import_form->get_data()) {
            // work out if this is an uploaded file
            // or one from the filesarea.
            $realfilename = $import_form->get_new_filename('newfile');

            $importfile = "{$CFG->tempdir}/structureimport/{$realfilename}";
            make_temp_directory('structureimport');
            if (!$result = $import_form->save_file('newfile', $importfile, true)) {
                throw new moodle_exception('uploadproblem');
            }
            
            if (!is_readable($importfile)) {
                throw new moodle_exception('uploadproblem');
            }

            $file = fopen($importfile, 'r', 0);
            if (empty($file)) {
                throw new moodle_exception('uploadproblem');
            }
            $buffer = '';
            while ($line = fgets($file)) {
               $buffer .= $line;             
            }
            fclose($file);
            
            unlink ($importfile);
            
            $data = new SimpleXMLElement($buffer);
            
            $result = local_fuzzylogic_import_xml_structure ($course->id, $data);
            if ($result == true) {
                $returnurl = new moodle_url('/local/fuzzylogic/structure.php', array('cid' => $contextid, 'action' => FUZZYLOGIC_ACTION_LIST));
                redirect($returnurl);
            } else {
                $returnurl = new moodle_url('/local/fuzzylogic/structure.php', array('cid' => $contextid, 'action' => FUZZYLOGIC_ACTION_LIST));
                error(get_string('error_importing_structure', 'local_fuzzylogic'), $returnurl);
            }
            
       }
       //Si llego aquí es que el formulario no tenia datos
       $returnurl = new moodle_url('/local/fuzzylogic/structure.php', array('cid' => $contextid, 'action' => FUZZYLOGIC_ACTION_LIST));
       error(get_string('incorrectfile', 'local_fuzzylogic'), $returnurl);
       break;
}

echo $OUTPUT->footer();