<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Concepts editor Page
 *
 * @package    local_fuzzylogic
 * @copyright  2013 Oscar Ruesga <oscar@ruesga.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once (dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/lib.php');
require_once(dirname(__FILE__).'/locallib.php');

$contextid = required_param('cid', PARAM_INT);
$action  = optional_param('action', FUZZYLOGIC_ACTION_LIST, PARAM_ALPHA );



list($context, $course, $cm) = get_context_info_array($contextid);

require_login($course, true);
require_capability('local/fuzzylogic:manage', $context);


$PAGE->set_url(new moodle_url('/local/fuzzylogic/concepts.php', array('cid' => $contextid, 'action' => $action)));
$PAGE->set_title(get_string('concepts_definition', 'local_fuzzylogic'));
$PAGE->set_heading(get_string('concepts_definition', 'local_fuzzylogic'));
$PAGE->set_pagelayout('standard');




switch ($action) {
  case FUZZYLOGIC_ACTION_LIST:
    $page = optional_param('page', 0, PARAM_INT);
    $searchquery  = optional_param('search', '', PARAM_RAW);
    $concepts = local_fuzzylogic_get_concepts($course->id, $page, 25, $searchquery);
    
    echo $OUTPUT->header();
    echo $OUTPUT->heading(get_string('concepts_allowed', 'local_fuzzylogic'));

    // Add search form.
    $search  = html_writer::start_tag('form', array('id'=>'searchconceptquery', 'method'=>'post'));
    $search .= html_writer::start_tag('div');
    $search .= html_writer::label(get_string('search_concept', 'local_fuzzylogic'), 'concept_search_q'); // No : in form labels!
    $search .= html_writer::empty_tag('input', array('id'=>'concept_search_q', 'type'=>'text', 'name'=>'search', 'value'=>$searchquery));
    $search .= html_writer::empty_tag('input', array('id'=>'cid', 'type'=>'hidden', 'name'=>'id', 'value'=>$contextid));
    $search .= html_writer::empty_tag('input', array('type'=>'submit', 'value'=>get_string('search', 'local_fuzzylogic')));
    $search .= html_writer::end_tag('div');
    $search .= html_writer::end_tag('form');
    echo $search;
    
    // Output pagination bar.
    $params = array('page' => $page);
    if ($contextid) {
        $params['cid'] = $contextid;
    }
    if ($search) {
        $params['search'] = $searchquery;
    }

    $params['action'] = FUZZYLOGIC_ACTION_LIST;
    
    $baseurl = new moodle_url('/local/fuzzylogic/concepts.php', $params);
    echo $OUTPUT->paging_bar($concepts['totalconcepts'], $page, 25, $baseurl);
    
    $data = array();
    
    foreach($concepts['concepts'] as $concept) {
        $line = array();
        $line[] = format_string($concept->name);
        $line[] = s($concept->shortname); // All idnumbers are plain text.
        $line[] = format_text($concept->description, FORMAT_HTML);
        $line[] = $concept->totalassesments;

        $buttons = array();
        $buttons[] = html_writer::link(new moodle_url('/local/fuzzylogic/concepts.php', array('cid'=>$contextid, 'action'=>FUZZYLOGIC_ACTION_DELETE, 'conceptid'=>$concept->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/delete'), 'alt'=>get_string('delete'), 'class'=>'iconsmall')));
        $buttons[] =  html_writer::link(new moodle_url('/local/fuzzylogic/concepts.php', array('cid'=>$contextid, 'action'=>FUZZYLOGIC_ACTION_EDIT, 'conceptid'=>$concept->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/edit'), 'alt'=>get_string('edit'), 'class'=>'iconsmall')));
        $buttons[] =  html_writer::link(new moodle_url('/local/fuzzylogic/assesstgraphs.php', array('cid'=>$contextid, 'id'=>$concept->id, 'type'=>FUZZYLOGIC_TYPE_CONCEPT)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('i/outcomes'), 'alt'=>get_string('assessment_graphs', 'local_fuzzylogic'), 'class'=>'iconsmall')));
        if (has_capability('local/fuzzylogic:duplicateconcepts', $context)) {
            $buttons[] =  html_writer::link(new moodle_url('/local/fuzzylogic/concepts.php', array('cid'=>$contextid, 'action'=>FUZZYLOGIC_ACTION_DUPLICATE, 'conceptid'=>$concept->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('i/backup'), 'alt'=>get_string('duplicate'), 'class'=>'iconsmall')));
        }
        $line[] = implode(' ', $buttons);

        $data[] = $line;
    }
    $table = new html_table();
    $table->head  = array(get_string('name', 'local_fuzzylogic'), get_string('shortname', 'local_fuzzylogic'), get_string('description', 'local_fuzzylogic'),
                          get_string('assesmentscount', 'local_fuzzylogic'), get_string('edit'));
    $table->size  = array('20%', '10%', '40%', '20%',  '10%');
    $table->align = array('left', 'left', 'left', 'left','center', 'center');
    $table->width = '80%';
    $table->data  = $data;
    echo html_writer::table($table);
    echo $OUTPUT->paging_bar($concepts['totalconcepts'], $page, 25, $baseurl);
        
    echo $OUTPUT->single_button(new moodle_url('/local/fuzzylogic/concepts.php', array('cid'=>$context->id, 'action'=>FUZZYLOGIC_ACTION_ADD)), get_string('addconcept', 'local_fuzzylogic'));

    break;
  case FUZZYLOGIC_ACTION_ADD:
  case FUZZYLOGIC_ACTION_EDIT:
    require_once(dirname(__FILE__).'/concepts_form.php');
    
    $conceptid = optional_param('conceptid', 0, PARAM_INT);

    $concept = (!empty($conceptid)) ? local_fuzzylogic_get_concept ($conceptid) : null;
    
    $concept->totalassesments = local_fuzzylogic_count_assesstments($concept->id, 'concept');
    
    if ($concept->totalassesments > 0) {
      $assessments = local_fuzzylogic_get_assessments($concept->id, 'concept');
      
      $concept->assess_id = array();
      $concept->assess_instanceid = array();
      $concept->assess_assesmenttype = array();
      $concept->assess_linguistictag = array();
      $concept->assess_feedback = array();
      $concept->assess_param_a = array();
      $concept->assess_param_b = array();
      $concept->assess_param_c = array();
      $concept->assess_param_d = array();
      $concept->assess_param_e = array();
  
      foreach ($assessments as $assessment){
        $concept->assess_id[] = $assessment->id;
        $concept->assess_instanceid[] = $assessment->instanceid;
        $concept->assess_assesmenttype[] = $assessment->assesmenttype;
        $concept->assess_linguistictag[] = $assessment->linguistictag;
        $concept->assess_feedback[] = $assessment->feedback;
        $concept->assess_param_a[] = $assessment->param_a;
        $concept->assess_param_b[] = $assessment->param_b;
        $concept->assess_param_c[] = $assessment->param_c;
        $concept->assess_param_d[] = $assessment->param_d;
        $concept->assess_param_e[] = $assessment->param_e;
      }
    }
    
    $editoroptions = array('maxfiles'=>0, 'context'=>$context);
    if ($concept->id) {
        // Edit existing.
        $concept = file_prepare_standard_editor($concept, 'description', $editoroptions, $context);
        $strheading = get_string('editconcept', 'local_fuzzylogic');
        $action = FUZZYLOGIC_ACTION_EDIT;

    } else {
        // Add new.
        $concept = file_prepare_standard_editor($concept, 'description', $editoroptions, $context);
        $strheading = get_string('addconcept', 'local_fuzzylogic');
        $action = FUZZYLOGIC_ACTION_ADD;
    }

    $PAGE->set_title($strheading);
    $PAGE->set_heading($COURSE->fullname);
    $PAGE->navbar->add($strheading);
    $mform = new local_fuzzylogic_concepts_form(null, array(
                                                              'cid'=>$context->id, 
                                                              'courseid'=>$course->id, 
                                                              'data'=>$concept, 
                                                              'editoroptions'=>$editoroptions,
                                                              'action'=> $action
                                                            ), 'post');

    if ($mform->is_cancelled()) {
        redirect( new moodle_url('/local/fuzzylogic/concepts.php', array('cid'=>$context->id, 'action'=>FUZZYLOGIC_ACTION_LIST)) );
    } else if ($mform->is_submitted() && $mform->is_validated() ) {
      $data = $mform->get_data();
      $data = file_postupdate_standard_editor($data, 'description', $editoroptions, $context);

      if ($data->id) {
          local_fuzzylogic_update_concept($data);
     
      } else {
          $conceptid = local_fuzzylogic_add_concept($data);
      }
      
      //Save assessments
      foreach ($data->assess_id as $num=>$value){
        $assessment = new stdClass();
        $assessment->id = $data->assess_id[$num];
        $assessment->instanceid = $data->assess_instanceid[$num];
        $assessment->assesmenttype = $data->assess_assesmenttype[$num];
        $assessment->linguistictag = $data->assess_linguistictag[$num];
        $assessment->feedback = $data->assess_feedback[$num];
        $assessment->param_a = $data->assess_param_a[$num];
        $assessment->param_b = $data->assess_param_b[$num];
        $assessment->param_c = $data->assess_param_c[$num];
        $assessment->param_d = $data->assess_param_d[$num];
        $assessment->param_e = $data->assess_param_e[$num];
        
        //Borro la valoración en caso de que venga vacia
        if (  $assessment->linguistictag == '' || 
              $assessment->feedback == '' || 
              $assessment->param_a == '' ||
              $assessment->param_b == '' ||
              $assessment->param_c == '' ||
              $assessment->param_d == ''
            ) 
        {
            if ($assessment->id != 0){ 
               local_fuzzylogic_delete_assesment($assessment->id);
            }
        } else {
          $assessment->instanceid = ($assessment->instanceid==0) ? ( ($conceptid == 0) ? $data->assess_instanceid[0] : $conceptid ) :  $assessment->instanceid;
        
          if ($assessment->id == 0) {
            unset($assessment->id);
            local_fuzzylogic_add_assessment($assessment);
          } else {
            local_fuzzylogic_update_assessment($assessment);
          }
        }
        
        
      }
      
      redirect( new moodle_url('/local/fuzzylogic/concepts.php', array('cid'=>$context->id, 'action'=>FUZZYLOGIC_ACTION_LIST)) );
    }
    
    echo $OUTPUT->header();
    echo $OUTPUT->heading($strheading);
    $mform->display();
    break;
  

  
  CASE FUZZYLOGIC_ACTION_DELETE:
    $conceptid = optional_param('conceptid', 0, PARAM_INT);
    $confirm = optional_param('confirm', 0, PARAM_BOOL);
  
    if ($confirm and confirm_sesskey()) {
        local_fuzzylogic_delete_concept($conceptid);
        $returnurl = new moodle_url('/local/fuzzylogic/concepts.php', array('cid' => $contextid, 'action' => FUZZYLOGIC_ACTION_LIST));
        redirect($returnurl);
    }
    $strheading = get_string('delconcept', 'local_fuzzylogic');
    $PAGE->navbar->add($strheading);
    $PAGE->set_title($strheading);
    $PAGE->set_heading($COURSE->fullname);
    echo $OUTPUT->header();
    echo $OUTPUT->heading($strheading);
    $deleteurl = new moodle_url('/local/fuzzylogic/concepts.php', array('cid' => $contextid, 'conceptid'=>$conceptid, 'action' => FUZZYLOGIC_ACTION_DELETE, 'confirm'=>1, 'sesskey'=>sesskey()));
    $returnurl = new moodle_url('/local/fuzzylogic/concepts.php', array('cid' => $contextid, 'action' => FUZZYLOGIC_ACTION_LIST));
    echo $OUTPUT->confirm(get_string('confirmdeleteconcept', 'local_fuzzylogic'), $deleteurl, $returnurl);
    break;
    
  CASE FUZZYLOGIC_ACTION_DUPLICATE:
    require_capability('local/fuzzylogic:duplicateconcepts', $context);
    
     $conceptid = optional_param('conceptid', 0, PARAM_INT);
      
    $result = local_fuzzylogic_duplicate_concept($conceptid);
    $returnurl = new moodle_url('/local/fuzzylogic/concepts.php', array('cid' => $contextid, 'action' => FUZZYLOGIC_ACTION_LIST));
    if ($result == true) {        
        redirect($returnurl);
    } else {
        error(get_string('error_duplicating_concept', 'local_fuzzylogic'), $returnurl);
    }

    break;
}

echo $OUTPUT->footer();