<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Results Page
 *
 * @package    local_fuzzylogic
 * @copyright  2013 Oscar Ruesga <oscar@ruesga.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once (dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/lib.php');
require_once(dirname(__FILE__).'/javascript/geshi/geshi.php');

$contextid = required_param('cid', PARAM_INT);
$structureid  = required_param('sid', PARAM_INT );
$userid  = required_param('uid', PARAM_INT );
$attemptid  = optional_param('aid', 0, PARAM_INT );

list($context, $course, $cm) = get_context_info_array($contextid);
require_login($course, true);
require_capability('local/fuzzylogic:viewlog', $context);


$urlparams = array ();
$urlparams['cid'] = $contextid;
$urlparams['sid'] = $structureid;
$urlparams['uid'] = $userid;

$user = $DB->get_record('user', array('id'=>$userid) );
$structurename = $DB->get_field('fuzzylogic_structure', 'name', array('id'=>$structureid) );

$username = $user->firstname . ' ' . $user->lastname;

$PAGE->set_url(new moodle_url('/local/fuzzylogic/log.php', $urlparams ));
$PAGE->set_title($structurename);
$PAGE->set_heading($structurename);
$PAGE->set_pagelayout('standard');

$attempts = $DB->get_records('fuzzylogic_results_log', array('structureid'=>$structureid, 'userid'=>$userid));

if ($attemptid == 0) {
    foreach ($attempts as $attempt) {
        if ($attempt->attemptid > $attemptid ) {
            $attemptid = $attempt->attempid;
            $current_attempt = $attempt;
        }
    } 
}
 
echo $OUTPUT->header();
echo $OUTPUT->heading();

$geshi = new GeSHi(base64_decode($current_attempt->log));

$geshi->set_language( 'txt' );

$geshi->set_header_type(GESHI_HEADER_DIV);

$geshi->enable_line_numbers(GESHI_NORMAL_LINE_NUMBERS );
$geshi->set_line_style('text-align: left;font-size:11px; background: #CCCCCC;', 'font-size:11px; background: #BBBBBB;', true);
$geshi->set_tab_width(1);

$geshi->set_header_content( "$structurename ($username)" );
$geshi->set_header_content_style('font-family:Courier; font-size:14px; color: #ffffff; background: #000000; text-align: center;');
$geshi->set_footer_content( gmdate("d-m-Y H:i:s", $current_attempt->date) . " ($current_attempt->attemptid)" );
$geshi->set_footer_content_style('font-family:Courier; font-size:10px; color: #ffffff; background: #000000; text-align: right;');

$geshi->set_comments_style(1, 'font-style: italic;');
$geshi->set_comments_style('MULTI', 'font-style: italic;');
echo $geshi->parse_code();

echo '<br />';


    
 

echo $OUTPUT->footer();