<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Concepts editor Page
 *
 * @package    local_fuzzylogic
 * @copyright  2013 Oscar Ruesga <oscar@ruesga.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once (dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/lib.php');
require_once(dirname(__FILE__).'/locallib.php');
require_once(dirname(__FILE__).'/concept2question_form.php');


$contextid = required_param('cid', PARAM_INT);
$action  = optional_param('action', FUZZYLOGIC_ACTION_LIST, PARAM_ALPHA );



list($context, $course, $cm) = get_context_info_array($contextid);

require_login($course, true);
require_capability('local/fuzzylogic:manage', $context);


$PAGE->set_url(new moodle_url('/local/fuzzylogic/concept2question.php', array('cid' => $contextid, 'action' => $action)));
$PAGE->set_title(get_string('quiz_question_definition', 'local_fuzzylogic'));
$PAGE->set_heading(get_string('quiz_question_definition', 'local_fuzzylogic'));
$PAGE->set_pagelayout('standard');




switch ($action) {
  case FUZZYLOGIC_ACTION_LIST:

   
    
    echo $OUTPUT->header();

    $quiz2structure = local_fuzzylogic_get_quiz2structure($course->id);
     
     $structureoptions = array( 0 => get_string('selectstructure', 'local_fuzzylogic') );
     $quizoptions = array ( 0 => get_string('selectquiz', 'local_fuzzylogic') );
     
     $quizs = $DB->get_records ('quiz', array('course'=>$course->id) );
     
     foreach ($quizs as $quiz) {
       $quizoptions[$quiz->id] = $quiz->name;
     }
     
     $structures = $DB->get_records ('fuzzylogic_structure', array('courseid'=>$course->id) );
     
     foreach ($structures as $structure) {
       $structureoptions[$structure->id] = $structure->name;
     }
     
     
     $mform = new local_fuzzylogic_structurequiz_form (null, array(
                                                              'cid'=>$context->id, 
                                                              'structureoptions'=>$structureoptions, 
                                                              'quizoptions'=>$quizoptions, 
                                                              'action'=>FUZZYLOGIC_ACTION_EDIT, 
                                                            ), 'post');
     
     if ($mform->is_submitted() && $mform->is_validated() ) {
        $data = $mform->get_data();
        $params = array();

        $params['cid'] = $contextid;
        $params['action'] = FUZZYLOGIC_ACTION_ADD;
        $params['quizid'] = $data->quizid;
        $params['structureid'] = $data->structureid;

        $url = new moodle_url('/local/fuzzylogic/concept2question.php', $params);
        redirect($url);
     }
     $mform->display();
     
    
    
   $data = array();
    
    foreach($quiz2structure as $qs) {
        $line = array();
        $line[] = format_string($qs->quizname);
        $line[] = format_string($qs->structurename);


        $buttons = array();
        $buttons[] = html_writer::link(new moodle_url('/local/fuzzylogic/concept2question.php', array('cid'=>$contextid, 'structureid'=>$qs->structureid,'quizid'=>$qs->quizid, 'action'=>FUZZYLOGIC_ACTION_DELETE)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/delete'), 'alt'=>get_string('delete'), 'class'=>'iconsmall')));
        $buttons[] =  html_writer::link(new moodle_url('/local/fuzzylogic/concept2question.php', array('cid'=>$contextid, 'structureid'=>$qs->structureid,'quizid'=>$qs->quizid,'action'=>FUZZYLOGIC_ACTION_EDIT)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/edit'), 'alt'=>get_string('edit'), 'class'=>'iconsmall')));
        
        $line[] = implode(' ', $buttons);

        $data[] = $line;
    }
    $table = new html_table();
    $table->head  = array(get_string('quiz', 'local_fuzzylogic'), get_string('structure', 'local_fuzzylogic'), get_string('edit'));
    $table->size  = array('45%', '45%', '10%');
    $table->align = array('left', 'left', 'center');
    $table->width = '80%';
    $table->data  = $data;
    echo html_writer::table($table);

    
    break;
    
  case FUZZYLOGIC_ACTION_EDIT:
   
    
    $quizid = optional_param('quizid', 0, PARAM_INT);
    $structureid = optional_param('structureid', 0, PARAM_INT);
    
    $questionconcepts = array();
    $qcrecords = $DB->get_records('fuzzylogic_questionconcepts', array('quizid'=>$quizid, 'structureid'=>$structureid) );
    foreach($qcrecords as $qcrecord) {
      $questionconcepts[$qcrecord->questioninstanceid][] = $qcrecord;
    }

    $questions=array();
    $questioninstances = $DB->get_records('quiz_question_instances', array('quiz'=>$quizid));
    foreach ($questioninstances as $questioninstance){
      $q = $DB->get_record('question', array('id'=>$questioninstance->question));
      $answers  = $DB->get_records('question_answers', array('question'=>$q->id), '','answer');
      $a = array();
      foreach ($answers as $answer){
        $a[] = format_string($answer->answer);
      }
      $question = new stdClass();
      $question->id = $q->id;
      $question->title = format_string($q->questiontext);
      $question->answers = $a;
      $question->questioninstanceid = $questioninstance->id;
      $questions[] = $question;
      
    }

    $conceptids = $DB->get_records('fuzzylogic_structureconcepts', array('structureid'=>$structureid), '', 'conceptid');   
    $conceptoptions = array ( 0 => get_string('conceptselection', 'local_fuzzylogic'));
    foreach ($conceptids as $conceptid){
      $concept = $DB->get_record('fuzzylogic_concepts', array('id'=>$conceptid->conceptid));
      $conceptoptions[$concept->id] = '('.$concept->shortname.') '. $concept->name;
    }

    $strheading = get_string('quiz_question_definition', 'local_fuzzylogic');

    $PAGE->set_title($strheading);
    $PAGE->set_heading($COURSE->fullname);
    $PAGE->navbar->add($strheading);
    $mform = new local_fuzzylogic_concept2question_form (null, array(
                                                              'cid'=>$context->id, 
                                                              'conceptoptions'=>$conceptoptions, 
                                                              'questions'=>$questions, 
                                                              'action'=> $action,
                                                              'quizid'=>$quizid,
                                                              'structureid'=>$structureid,
                                                              'questionconcepts'=>$questionconcepts
                                                            ), 'post');

    if ($mform->is_cancelled()) {
        $params['cid'] = $contextid;
        $params['action'] = FUZZYLOGIC_ACTION_LIST;
        $url = new moodle_url('/local/fuzzylogic/concept2question.php', $params);
        redirect( $url );
    } else if ($mform->is_submitted() && $mform->is_validated() ) {
      $data = $mform->get_data();
      
      $questionconcept = new stdClass();
      $questionconcept->quizid = $data->quizid;
      $questionconcept->structureid = $data->structureid;
      foreach($data->question_instanceid as $num=>$value){
        $questionconcept->questioninstanceid = $data->question_instanceid[$num];
        
        $question_conceptid = $data->question_conceptid[$data->question_instanceid[$num]];
        foreach ($question_conceptid as $i=>$value){
          if ($value == 0) {
            $qcid = $data->question_id[$data->question_instanceid[$num]][$i];
            if (!empty($qcid))
               $DB->delete_records('fuzzylogic_questionconcepts', array('id'=>$qcid));
          } else {
            $questionconcept->conceptid = $value; 
            $questionconcept->id =  $data->question_id[$questionconcept->questioninstanceid][$i];
            if( $DB->record_exists('fuzzylogic_questionconcepts', array('quizid'=>$questionconcept->quizid,'questioninstanceid'=>$questionconcept->questioninstanceid,'structureid'=>$questionconcept->structureid,'conceptid'=>$questionconcept->conceptid) ) )
               continue;
            if (!empty($questionconcept->id)) {
              $DB->update_record('fuzzylogic_questionconcepts', $questionconcept);
            } else {
              $DB->insert_record('fuzzylogic_questionconcepts', $questionconcept);
            }
          }
        }
        
        
      }
     
      local_fuzzylogic_create_grade_structure ($structureid);
      
      $params['cid'] = $contextid;
      $params['action'] = FUZZYLOGIC_ACTION_LIST;
      $url = new moodle_url('/local/fuzzylogic/concept2question.php', $params);
      redirect( $url );
    }
    
    echo $OUTPUT->header();
    echo $OUTPUT->heading($strheading);
    $mform->display();
    break;
  

  
  CASE FUZZYLOGIC_ACTION_DELETE:

    $structureid = optional_param('structureid', 0, PARAM_INT);
    $quizid = optional_param('quizid', 0, PARAM_INT);
    $confirm = optional_param('confirm', 0, PARAM_BOOL);
  
    if ($confirm and confirm_sesskey()) {

        $DB->delete_records('fuzzylogic_questionconcepts', array('quizid'=>$quizid,'structureid'=>$structureid));
        
        $returnurl = new moodle_url('/local/fuzzylogic/concept2question.php', array('cid' => $contextid, 'action' => FUZZYLOGIC_ACTION_LIST));
        redirect($returnurl);
    }
    $strheading = get_string('disassociate_question', 'local_fuzzylogic');
    $PAGE->navbar->add($strheading);
    $PAGE->set_title($strheading);
    $PAGE->set_heading($COURSE->fullname);
    echo $OUTPUT->header();
    echo $OUTPUT->heading($strheading);
    $deleteurl = new moodle_url('/local/fuzzylogic/concept2question.php', array('cid' => $contextid, 'structureid'=>$structureid,'quizid'=>$quizid, 'conceptid'=>$conceptid, 'action' => FUZZYLOGIC_ACTION_DELETE, 'confirm'=>1, 'sesskey'=>sesskey()));
    $returnurl = new moodle_url('/local/fuzzylogic/concept2question.php', array('cid' => $contextid, 'action' => FUZZYLOGIC_ACTION_LIST));
    echo $OUTPUT->confirm(get_string('confirm_disassociate_question', 'local_fuzzylogic'), $deleteurl, $returnurl);
    break;
}

echo $OUTPUT->footer();
