<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'local_fuzylogic', language 'es', branch 'MOODLE_24_STABLE'
 *
 * @package    local_fuzzylogic
 * @copyright  2013 Oscar Ruesga
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


$string['pluginname'] = 'Fuzzy Logic';
$string['concepts_definition'] = 'Elementary  Concepts definitions';
$string['structure_definition'] = 'Structure definition';
$string['quiz_question_definition'] = 'Quiz question definition';
$string['results'] = 'Results';

$string['concepts_allowed'] = 'Allowed elementary concepts';
$string['search_concept'] = 'Elementary Concept search';
$string['search'] = 'Search';
$string['addconcept'] = 'Add elementary concept';
$string['editconcept'] = 'Edit elementary concept';
$string['delconcept'] = 'Delete elementary concept';
$string['name'] = 'Name';
$string['shortname'] = 'Identifier';
$string['description'] = 'Description';
$string['assesmentscount'] = 'Assesments Number';
$string['confirmdeleteconcept'] = 'It will proceed to eliminate the elementary concept and associated assessments.';
$string['saveconcept'] = 'Save Elementary Concept';
$string['duplicateshortname'] = 'Duplicate shortname';
$string['assessments'] = 'Assesments';
$string['assessment'] = 'Assesment';
$string['linguistictag'] = 'Linguistic tag';
$string['feedback'] = 'Feddback';
$string['parama'] = '"A" parameter of membership function';
$string['paramb'] = '"B" parameter of membership function';
$string['paramc'] = '"C" parameter of membership function';
$string['paramd'] = '"D" parameter of membership function';
$string['parame'] = 'Centroide alternative';
$string['addassessment'] = 'Add assessment';
$string['invalidrange'] = 'Invalid score range. Introduce values between 0 and 1';
$string['minorvalue'] = 'The value can not be smaller than the previous';
$string['fuzzylogic_structures'] = 'Fuzzy logic structures';
$string['search_structure'] = 'Search structure';
$string['totalconcepts'] = 'Total elementary concepts associated to a structure';
$string['totalcriteria'] = 'Total complex concept';
$string['addstructure'] = 'Add structure';
$string['editstructure'] = 'Edit structure';
$string['delstructure'] = 'Delete structure';
$string['associateconncepts'] = 'Associate elementary concepts';
$string['conceptselection'] = 'Select a elementary concept...';
$string['concept'] = 'Elementary Concept';
$string['confirmdeletestructure'] = 'It will proceed to eliminate the structure and elementary concept associated.';
$string['criteria_definition'] = 'Define complex concepts';
$string['search_criteria'] = 'Search complex concepts';
$string['entriescount'] = 'Complex concept entries number';
$string['addcriteria'] = 'Add complex concept';
$string['editcriteria'] = 'Edit complex concept';
$string['addglobalcriteria'] = 'Add global concept';
$string['editglobalcriteria'] = 'Edit global concept';
$string['delcriteria'] = 'Delete complex concept';
$string['confirmdeletecriteria'] = 'It will proceed to remove the complex concept and all its associated data.';
$string['returnstructure'] = '<< Return to structure list';
$string['globalcriteria'] = 'Global omplex concepts';
$string['defineglobalcriteria'] = 'Define global concepts';
$string['specificcriterias'] = 'Specific complex concepts';
$string['errorglobalcriteriaexist'] = 'Global concepts defined structure. There can only be a global concepts for structure';
$string['criteria'] = 'Criteria';
$string['entry_global_criteria_selection'] = 'Select a complex concepts to associate the Global concepts...';
$string['entry_criteria_selection'] = 'Select a elementary concept to associate the complex concepts...';
$string['addentry'] = 'Add entry';
$string['entries'] = 'Entries';
$string['noconceptsentries'] = 'Elementary Concepts are not associated with the structure..';
$string['nocriteriasentries'] = 'Complex concepts are not associated with the structure.';
$string['criteriarule_definition'] = 'Definir reglas del criterio';
$string['tnorma'] = 't-norm';
$string['tconorma'] = 't-conorm';
$string['lukasiewicz'] = 'Lukasiewicz';
$string['tnorma_producto'] = 'Product';
$string['tnorma_minimo'] = 'Minimum';
$string['tconorma_sumaproducto'] = 'Sum-product';
$string['tconorma_maximo'] = 'Maximum';
$string['selecttnorma'] = 'Select a t-norm for aggregation of antecedents ...';
$string['selecttconorma'] = 'Select a t-conorm for aggregation of the resulting ...';
$string['rule'] = 'Rule {$a}';
$string['antecedent'] = 'Antecedent';
$string['consequent'] = 'Consequent';
$string['selectconsequent'] = 'Select a consequent...';
$string['invalidselect'] = 'Select a valid item';
$string['associate_test_structure'] = 'Associate structure assessment to questionnaire';
$string['structure'] = 'Structure';
$string['quiz'] = 'Quiz';
$string['selectstructure'] = 'Select the structure to associate...';
$string['selectquiz'] = 'Select the quiz to associate...';
$string['asociate_questions'] = 'Associate questions';
$string['question'] = 'Question {$a}';
$string['disassociate_question'] = 'Disassociate question';
$string['confirm_disassociate_question'] = 'It will proceed to detach the quiz questions of this structure.';
$string['IF'] = 'IF';
$string['AND'] = 'AND';
$string['EQUAL'] = 'IS EQUAL TO';
$string['user_reports'] = 'User Report';
$string['concepts'] = 'Elementary Concepts';
$string['criterias'] = 'Complex concepts';
$string['score'] = 'Score';
$string['viewlog'] = 'View log';
$string['validation_error_concept_count'] = 'At least there must be a elementary concept';
$string['validation_error_concept_assesst_count'] = 'At least there must be a valuation associated with the elementary concept <strong>{$a}</strong>';
$string['validation_error_criteria_count'] = 'At a minimum there must be a complex concepts and a global concepts';
$string['validation_error_criteria_assest_count'] = 'At least there has to be an assessment complex concepts associated to <strong>{$a}</strong>';
$string['validation_error_criteria_entry_count'] = 'At least there should be an entry to the complex concepts <strong>{$a}</strong>';
$string['validation_error_criteria_rules'] = 'You need to define the rules of the complex concepts <strong>{$a}</strong>';
$string['validation_error_consequents'] = 'The number of consequents must be equal to the number of rules for the complex concepts <strong>{$a}</strong>';
$string['validation_error_global_criteria_count'] = 'There must be one and only one global concepts';
$string['validation_error_question_concepts_count'] = 'At a minimum there must be a question related to a elementary concept in the structure';
$string['review_elements'] ='Tiene que revisar los siguientes elementos:';
$string['quiz_access'] = 'Access to Quiz...';
$string['quiz_not_available'] = 'The quiz is not available for evaluation by fuzzy logic.';
$string['quiz_not_evaluate'] = 'There is no assessment of the quiz associated with this fuzzy logic structure.';
$string['assessment_graphs'] = 'Assessment graph for {$a}';
$string['select_entry'] = 'Select the entry you want to keep its value constant and introduces the value:';
$string['generategraph'] = 'Generate Graph';
$string['js_minentries'] = 'At a minimum there must be two entrances to the criteria.';
$string['js_maxentries'] = 'At a maximum there must be two entrances to the criteria.';
$string['js_noconstantselect'] = 'You need to select an entry from the list and indicate its constant value (between 0 and 1).';
$string['js_constantrangeincorrect'] = 'The constant value must be within the range between the values ​​0 and 1.';
$string['entries_graph'] = 'Criteria entries graph';
$string['graphs'] = 'Graphs Representation';
$string['error_importing_structure'] = 'There is an error importing the structure';
$string['no_structure_available'] = 'There is no structures availables to import to this course';
$string['select_sctructure'] = 'Select the structure to import from one of the next available courses:';
$string['error_duplicating_concept'] = 'There is an error duplicating elemental concept.';
$string['error_duplicating_criteria'] = 'There is an error duplicating complex concept.';
$string['exportxmlstructure'] = 'Export structure to XML file';
$string['importstructurefromfile'] = 'Import structure from a file';
$string['importstructurefromcourse'] = 'Import structure from course';
$string['importwrongfiletype'] = 'Incorrect file type to import';
$string['importstructure'] = 'Import structure';
$string['incorrectfile'] = 'The selected file to import is malformed or empty';
