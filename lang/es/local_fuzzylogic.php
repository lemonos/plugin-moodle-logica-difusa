<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'local_fuzylogic', language 'en', branch 'MOODLE_24_STABLE'
 *
 * @package    local_fuzzylogic
 * @copyright  2013 Oscar Ruesga
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


$string['pluginname'] = 'Lógica Difusa';
$string['concepts_definition'] = 'Definir conceptos elementales para evaluación del aprendizaje';
$string['structure_definition'] = 'Definir estructura para la valoración';
$string['quiz_question_definition'] = 'Asociar conceptos elementales a preguntas';
$string['results'] = 'Resultados';

$string['concepts_allowed'] = 'Conceptos elementales disponibles';
$string['search_concept'] = 'Buscar Concepto elemental';
$string['search'] = 'Buscar';
$string['addconcept'] = 'Agregar concepto elemental';
$string['editconcept'] = 'Editar concepto elemental';
$string['delconcept'] = 'Borrar concepto elemental';
$string['name'] = 'Nombre';
$string['shortname'] = 'Identificador';
$string['description'] = 'Descripcion';
$string['assesmentscount'] = 'Número de Valoraciones';
$string['confirmdeleteconcept'] = 'Se va a proceder a eliminar el concepto elemental y las valoraciones asociadas.';
$string['saveconcept'] = 'Guardar Concepto elemental';
$string['duplicateshortname'] = 'Identificador duplicado';
$string['assessments'] = 'Valoraciones';
$string['assessment'] = 'Valoración';
$string['linguistictag'] = 'Etiqueta lingüística';
$string['feedback'] = 'Retroalimentación';
$string['parama'] = 'Parámetro "A" de la función de pertenencia';
$string['paramb'] = 'Parámetro "B" de la función de pertenencia';
$string['paramc'] = 'Parámetro "C" de la función de pertenencia';
$string['paramd'] = 'Parámetro "D" de la función de pertenencia';
$string['parame'] = 'Alternativa al centroide';
$string['addassessment'] = 'Agregar valoración';
$string['invalidrange'] = 'Rango de puntuación no valido. Introduce valores entre 0 y 1';
$string['minorvalue'] = 'El valor no puede ser menor que el anterior';
$string['fuzzylogic_structures'] = 'Estructuras de lógica difusa';
$string['search_structure'] = 'Buscar estructuras';
$string['totalconcepts'] = 'Total conceptos elementales asociados a la estructura';
$string['totalcriteria'] = 'Conceptos complejos totales';
$string['addstructure'] = 'Agregar estructura';
$string['editstructure'] = 'Editar estructura';
$string['delstructure'] = 'Borrar estructura';
$string['associateconncepts'] = 'Conceptos elementales asociados';
$string['conceptselection'] = 'Selecciona un concepto elemental...';
$string['concept'] = 'Concepto elemental';
$string['confirmdeletestructure'] = 'Se va a proceder a eliminar la estructura y los conceptos elemental asociados.';
$string['criteria_definition'] = 'Definir conceptos complejos';
$string['search_criteria'] = 'Buscar conceptos complejos';
$string['entriescount'] = 'Número de entradas al citerio';
$string['addcriteria'] = 'Agregar concepto complejo';
$string['editcriteria'] = 'Editar concepto complejo';
$string['addglobalcriteria'] = 'Agregar concepto global';
$string['editglobalcriteria'] = 'Editar concepto global';
$string['delcriteria'] = 'Borrar concepto complejo';
$string['confirmdeletecriteria'] = 'Se va a proceder a eliminar el concepto complejo y todos sus datos asociados.';
$string['returnstructure'] = '<< Volver al listado de estructuras';
$string['globalcriteria'] = 'Concepto Global';
$string['defineglobalcriteria'] = 'Definir el concepto global';
$string['specificcriterias'] = 'Conceptos complejos Especificos';
$string['errorglobalcriteriaexist'] = 'Estructura con concepto global definido. Solamente puede existir un concepto complejo global por estructura';
$string['criteria'] = 'Concepto complejo';
$string['entry_global_criteria_selection'] = 'Selecciona un concepto complejo para asociar al concepto global...';
$string['entry_criteria_selection'] = 'Selecciona un concepto elemental para asociar al concepto complejo...';
$string['addentry'] = 'Añadir entrada';
$string['entries'] = 'Entradas';
$string['noconceptsentries'] = 'No existen conceptos elementales asociados a la estructura.';
$string['nocriteriasentries'] = 'No existen conceptos complejos asociados a la estructura.';
$string['criteriarule_definition'] = 'Definir reglas del concepto complejo';
$string['tnorma'] = 't-norma';
$string['tconorma'] = 't-conorma';
$string['lukasiewicz'] = 'Lukasiewicz';
$string['tnorma_producto'] = 'Producto';
$string['tnorma_minimo'] = 'Mínimo';
$string['tconorma_sumaproducto'] = 'Suma-producto';
$string['tconorma_maximo'] = 'Máximo';
$string['selecttnorma'] = 'Selecciona una t-norma para la agregación de los antecedentes';
$string['selecttconorma'] = 'Selecciona una t-conorma para la agregación de los consecuentes';
$string['rule'] = 'Regla {$a}';
$string['antecedent'] = 'Antecedente';
$string['consequent'] = 'Consecuente';
$string['selectconsequent'] = 'Selecciona un consecuente...';
$string['invalidselect'] = 'Selecciona un elemento valido.';
$string['associate_test_structure'] = 'Asociar cuestionario a estructura de valoración';
$string['structure'] = 'Estructura';
$string['quiz'] = 'Cuestionario';
$string['selectstructure'] = 'Selecciona la estructura a asociar...';
$string['selectquiz'] = 'Selecciona el cuestionario a asociar...';
$string['asociate_questions'] = 'Asociar preguntas';
$string['question'] = 'Pregunta {$a}';
$string['disassociate_question'] = 'Desasociar pregunta';
$string['confirm_disassociate_question'] = 'Se va a proceder a desasociar las preguntas del cuestionario de esta estructura.';
$string['IF'] = 'SI';
$string['AND'] = 'Y';
$string['EQUAL'] = 'ES IGUAL A';
$string['user_report'] = 'Reporte de usuario';
$string['concepts'] = 'Conceptos elementales';
$string['criterias'] = 'Conceptos complejos';
$string['score'] = 'Puntuación';
$string['viewlog'] = 'Ver log';
$string['validation_error_concept_count'] = 'Al menos tiene que haber un concepto elemental';
$string['validation_error_concept_assesst_count'] = 'Al menos tiene que haber una valoración asociada al concepto elemental <strong>{$a}</strong>';
$string['validation_error_criteria_count'] = 'Cómo mínimo tiene que haber un concepto complejo y el concepto global';
$string['validation_error_criteria_assest_count'] = 'Al menos tiene que haber una valoración asociada al concepto complejo <strong>{$a}</strong>';
$string['validation_error_criteria_entry_count'] = 'Por lo menos debe de existir una entrada al concepto complejo <strong>{$a}</strong>';
$string['validation_error_criteria_rules'] = 'Es necesario definir las reglas del concepto complejo <strong>{$a}</strong>';
$string['validation_error_consequents'] = 'El numero de consecuentes establecido tiene que ser igual al número de reglas para el concepto complejo <strong>{$a}</strong>';
$string['validation_error_global_criteria_count'] = 'Debe existir 1 y solo 1 concepto global';
$string['validation_error_question_concepts_count'] = 'Como minimo tiene que existir una pregunta asociada a un concepto elemental en la estructura';
$string['review_elements'] ='Tiene que revisar los siguientes elementos:';
$string['quiz_access'] ='Acceder al cuestionario...';
$string['quiz_not_available'] = 'El questionario no está disponible para ser evaluado mediante lógica difusa.';
$string['quiz_not_evaluate'] = 'No hay ninguna evaluación del cuestionario asociada a esta estructura de lógica difusa.';
$string['assessment_graphs'] = 'Gráfico de valoraciones de {$a}';
$string['select_entry'] = 'Selecciona la entrada que quieres mantener constante e introduce su valor:';
$string['generategraph'] = 'Generar Gráfico';
$string['js_minentries'] = 'Cómo mínimo tienen que existir dos entradas al criterio.';
$string['js_maxentries'] = 'Cómo máximo pueden existir tres entradas para el criterio.';
$string['js_noconstantselect'] = 'Es necesario seleccionar una entrada de la lista e indicar su valor constante (entre 0 y 1)';
$string['js_constantrangeincorrect'] = 'El valor constante tiene que estar comprendido en el rango comprentido entre los valores 0 y 1';
$string['entries_graph'] = 'Gráfico de entradas del criterio';
$string['graphs'] = 'Representaciones Gráficas';
$string['error_importing_structure'] = 'Se ha producido un error importando la estructura';
$string['no_structure_available'] = 'No existen estructuras disponibles para importar en este curso';
$string['select_sctructure'] = 'Selecciona la estructura a importar de uno de los siguientes cursos disponibles:';
$string['error_duplicating_concept'] = 'Se ha producido un error duplicando el concepto elemental.';
$string['error_duplicating_criteria'] = 'Se ha producido un error duplicando el concepto complejo.';
$string['exportxmlstructure'] = 'Exportar estructura a archivo XML';
$string['importstructurefromfile'] = 'Importar estructura de un archivo';
$string['importstructurefromcourse'] = 'Importar estructura de otro curso';
$string['importwrongfiletype'] = 'El tipo de fichero a importar es incorrecto';
$string['importstructure'] = 'Importar estructura';
$string['incorrectfile'] = 'El fichero seleccionado para importar tiene formato incorrecto o está vacio';
