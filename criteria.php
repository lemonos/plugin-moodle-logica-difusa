<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Criteria editor Page
 *
 * @package    local_fuzzylogic
 * @copyright  2013 Oscar Ruesga <oscar@ruesga.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once (dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/lib.php');
require_once(dirname(__FILE__).'/locallib.php');

$contextid = required_param('cid', PARAM_INT);
$structureid = required_param('structureid', PARAM_INT);
$action  = optional_param('action', FUZZYLOGIC_ACTION_LIST, PARAM_ALPHA );



list($context, $course, $cm) = get_context_info_array($contextid);

require_login($course, true);
require_capability('local/fuzzylogic:manage', $context);


$PAGE->set_url(new moodle_url('/local/fuzzylogic/criteria.php', array('cid' => $contextid, 'structureid'=>$structureid, 'action' => $action)));
$PAGE->set_title(get_string('criteria_definition', 'local_fuzzylogic'));
$PAGE->set_heading(get_string('criteria_definition', 'local_fuzzylogic'));
$PAGE->set_pagelayout('standard');

if ($action == FUZZYLOGIC_ACTION_LIST) {
  $PAGE->navbar->add(get_string('criteria_definition', 'local_fuzzylogic') );
} else {
  $PAGE->navbar->add(get_string('criteria_definition', 'local_fuzzylogic'), new moodle_url('/local/fuzzylogic/criteria.php', array('cid'=>$context->id, 'structureid'=>$structureid)) );
}

switch ($action) {
  case FUZZYLOGIC_ACTION_LIST:
    
    $page = optional_param('page', 0, PARAM_INT);
    $searchquery  = optional_param('search', '', PARAM_RAW);
    $criterias = local_fuzzylogic_get_criterias($structureid, false, $page, 25, $searchquery);
    
    echo $OUTPUT->header();
    echo $OUTPUT->heading(get_string('criteria_definition', 'local_fuzzylogic'));

    // Add search form.
    $search  = html_writer::start_tag('form', array('id'=>'searchconceptquery', 'method'=>'post'));
    $search .= html_writer::start_tag('div');
    $search .= html_writer::label(get_string('search_criteria', 'local_fuzzylogic'), 'concept_search_q'); // No : in form labels!
    $search .= html_writer::empty_tag('input', array('id'=>'concept_search_q', 'type'=>'text', 'name'=>'search', 'value'=>$searchquery));
    $search .= html_writer::empty_tag('input', array('id'=>'cid', 'type'=>'hidden', 'name'=>'id', 'value'=>$contextid));
    $search .= html_writer::empty_tag('input', array('type'=>'submit', 'value'=>get_string('search', 'local_fuzzylogic')));
    $search .= html_writer::end_tag('div');
    $search .= html_writer::end_tag('form');
    echo $search;
    
    // Output pagination bar.
    $params = array('page' => $page);
    if ($contextid) {
        $params['cid'] = $contextid;
    }
    if ($search) {
        $params['search'] = $searchquery;
    }
    if ($structureid) {
        $params['structureid'] = $structureid;
    }
    $params['action'] = FUZZYLOGIC_ACTION_LIST;
    
    
    
    
    
    
    
    //CRITERIA TABLE
    echo $OUTPUT->heading(get_string('specificcriterias', 'local_fuzzylogic'), 3);
    
    if ( count($criterias['criterias']) > 0 ) {
      $baseurl = new moodle_url('/local/fuzzylogic/criteria.php', $params);
      echo $OUTPUT->paging_bar($criterias['totalcriterias'], $page, 25, $baseurl);
      $data = array();

      foreach($criterias['criterias'] as $criteria) {
          $line = array();
          $line[] = format_string($criteria->name);
          $line[] = s($criteria->shortname); 
          $line[] = format_text($criteria->description, FORMAT_HTML);
          $line[] = $criteria->totalentries;
          $line[] = $criteria->totalassesments;

          $buttons = array();
          $buttons[] = html_writer::link(new moodle_url('/local/fuzzylogic/criteria.php', array('cid'=>$contextid, 'structureid'=>$structureid, 'action'=>FUZZYLOGIC_ACTION_DELETE, 'id'=>$criteria->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/delete'), 'alt'=>get_string('delete'), 'class'=>'iconsmall')));
          $buttons[] =  html_writer::link(new moodle_url('/local/fuzzylogic/criteria.php', array('cid'=>$contextid, 'structureid'=>$structureid, 'action'=>FUZZYLOGIC_ACTION_EDIT, 'id'=>$criteria->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/edit'), 'alt'=>get_string('edit'), 'class'=>'iconsmall')));
          $buttons[] =  html_writer::link(new moodle_url('/local/fuzzylogic/criteriarule.php', array('cid'=>$contextid, 'criteriaid'=>$criteria->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('i/course'), 'alt'=>get_string('edit'), 'class'=>'iconsmall')));
          $buttons[] =  html_writer::link(new moodle_url('/local/fuzzylogic/assesstgraphs.php', array('cid'=>$contextid, 'id'=>$criteria->id, 'type'=>FUZZYLOGIC_TYPE_CRITERIA)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('i/outcomes'), 'alt'=>get_string('assessment_graphs', 'local_fuzzylogic'), 'class'=>'iconsmall')));
          if (has_capability('local/fuzzylogic:duplicateconcepts', $context)) {
            $buttons[] = html_writer::link(new moodle_url('/local/fuzzylogic/criteria.php', array('cid'=>$contextid, 'structureid'=>$structureid, 'action'=>FUZZYLOGIC_ACTION_DUPLICATE, 'id'=>$criteria->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('i/backup'), 'alt'=>get_string('duplicate'), 'class'=>'iconsmall')));
          }
          $line[] = implode(' ', $buttons);

          $data[] = $line;
      }
      $table = new html_table();
      $table->head  = array(get_string('name', 'local_fuzzylogic'), get_string('shortname', 'local_fuzzylogic'), get_string('description', 'local_fuzzylogic'), get_string('entriescount', 'local_fuzzylogic'),
                            get_string('assesmentscount', 'local_fuzzylogic'), get_string('edit'));
      $table->size  = array('20%', '10%', '40%', '10%', '5%',  '15%');
      $table->align = array('left', 'left', 'left', 'left','center','center', 'center');
      $table->width = '80%';
      $table->data  = $data;
      echo html_writer::table($table);
      echo $OUTPUT->paging_bar($criterias['totalcriterias'], $page, 25, $baseurl);
    }    
    echo $OUTPUT->single_button(new moodle_url('/local/fuzzylogic/criteria.php', array('cid'=>$context->id, 'structureid'=>$structureid, 'action'=>FUZZYLOGIC_ACTION_ADD)), get_string('addcriteria', 'local_fuzzylogic'));
    
    //GLOBAL CRITERIA
    echo $OUTPUT->heading(get_string('globalcriteria', 'local_fuzzylogic'), 3);
    if (!empty ($criterias['globalcriteria'])) {

      $line = array();
      $line[] = format_string($criterias['globalcriteria']->name);
      $line[] = s($criterias['globalcriteria']->shortname); 
      $line[] = format_text($criterias['globalcriteria']->description, FORMAT_HTML);
      $line[] = $criterias['globalcriteria']->totalentries;
      $line[] = $criterias['globalcriteria']->totalassesments;

      $buttons = array();
      $buttons[] = html_writer::link(new moodle_url('/local/fuzzylogic/criteria.php', array('cid'=>$contextid, 'structureid'=>$structureid, 'action'=>FUZZYLOGIC_ACTION_DELETE, 'id'=>$criterias['globalcriteria']->id, 'g'=>1)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/delete'), 'alt'=>get_string('delete'), 'class'=>'iconsmall')));
      $buttons[] =  html_writer::link(new moodle_url('/local/fuzzylogic/criteria.php', array('cid'=>$contextid, 'structureid'=>$structureid, 'action'=>FUZZYLOGIC_ACTION_EDIT, 'id'=>$criterias['globalcriteria']->id, 'g'=>1)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/edit'), 'alt'=>get_string('edit'), 'class'=>'iconsmall')));
      $buttons[] =  html_writer::link(new moodle_url('/local/fuzzylogic/criteriarule.php', array('cid'=>$contextid, 'criteriaid'=>$criterias['globalcriteria']->id, 'g'=>1)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('i/course'), 'alt'=>get_string('edit'), 'class'=>'iconsmall')));
      $buttons[] =  html_writer::link(new moodle_url('/local/fuzzylogic/assesstgraphs.php', array('cid'=>$contextid, 'id'=>$criterias['globalcriteria']->id, 'type'=>FUZZYLOGIC_TYPE_CRITERIA, 'global'=>1)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('i/outcomes'), 'alt'=>get_string('assessment_graphs', 'local_fuzzylogic'), 'class'=>'iconsmall')));
      $line[] = implode(' ', $buttons);

      
      $table = new html_table();
      $table->head  = array(get_string('name', 'local_fuzzylogic'), get_string('shortname', 'local_fuzzylogic'), get_string('description', 'local_fuzzylogic'), get_string('entriescount', 'local_fuzzylogic'),
                            get_string('assesmentscount', 'local_fuzzylogic'), get_string('edit'));
      $table->size  = array('20%', '10%', '40%', '10%', '5%',  '15%');
      $table->align = array('left', 'left', 'left', 'left','center','center', 'center');
      $table->width = '80%';
      $table->data  = array($line);
      echo html_writer::table($table);
      
    } else {
        echo $OUTPUT->box_start();
        echo html_writer::start_tag('center');
        echo $OUTPUT->single_button(new moodle_url('/local/fuzzylogic/criteria.php', array('cid'=>$context->id, 'structureid'=>$structureid, 'action'=>FUZZYLOGIC_ACTION_ADD, 'g'=>1,)), get_string('defineglobalcriteria', 'local_fuzzylogic'));
        echo html_writer::end_tag('center');
        echo $OUTPUT->box_end();
        
    }

    echo $OUTPUT->single_button(new moodle_url('/local/fuzzylogic/structure.php', array('cid'=>$context->id, 'action'=>FUZZYLOGIC_ACTION_LIST)), get_string('returnstructure', 'local_fuzzylogic'));

    
    break;
  case FUZZYLOGIC_ACTION_ADD:
  case FUZZYLOGIC_ACTION_EDIT:
    require_once(dirname(__FILE__).'/criteria_form.php');
    
    $criteriaid = optional_param('id', 0, PARAM_INT);

    $globalcriteria = optional_param('g', 0, PARAM_INT);

    $criteria = (!empty($criteriaid)) ? local_fuzzylogic_get_criteria ($criteriaid) : null;
    
    $criteria->totalassesments = local_fuzzylogic_count_assesstments($criteria->id, 'criteria');
    
    if ($criteria->totalassesments > 0) {
      $assessments = local_fuzzylogic_get_assessments($criteria->id, 'criteria');
      
      $criteria->assess_id = array();
      $criteria->assess_instanceid = array();
      $criteria->assess_assesmenttype = array();
      $criteria->assess_linguistictag = array();
      $criteria->assess_feedback = array();
      $criteria->assess_param_a = array();
      $criteria->assess_param_b = array();
      $criteria->assess_param_c = array();
      $criteria->assess_param_d = array();
      $criteria->assess_param_e = array();
  
      foreach ($assessments as $assessment){
        $criteria->assess_id[] = $assessment->id;
        $criteria->assess_instanceid[] = $assessment->instanceid;
        $criteria->assess_assesmenttype[] = $assessment->assesmenttype;
        $criteria->assess_linguistictag[] = $assessment->linguistictag;
        $criteria->assess_feedback[] = $assessment->feedback;
        $criteria->assess_param_a[] = $assessment->param_a;
        $criteria->assess_param_b[] = $assessment->param_b;
        $criteria->assess_param_c[] = $assessment->param_c;
        $criteria->assess_param_d[] = $assessment->param_d;
        $criteria->assess_param_e[] = $assessment->param_e;
      }
    }
    
    $entries = local_fuzzylogic_get_criteriaentries($criteria->id, ($globalcriteria==1) ? 'criteria' : 'concept');
    if ($entries['totalentries'] > 0) {
      $criteria->entry_id = array();
      $criteria->entry_criteriaid = array();
      $criteria->entry_entryid = array();
      $criteria->entry_entrytype = array();
  
      foreach ($entries['entries'] as $entry){
        $criteria->entry_id[] = $entry->id;
        $criteria->entry_criteriaid[] = $entry->criteriaid;
        $criteria->entry_entryid[] = $entry->entryid;
        $criteria->entry_entrytype[] = $entry->entrytype;
      }
    }
    
    $editoroptions = array('maxfiles'=>0, 'context'=>$context);
    if ($criteria->id) {
        // Edit existing.
        $criteria = file_prepare_standard_editor($criteria, 'description', $editoroptions, $context);
        $strheading = ($globalcriteria==false) ? get_string('editcriteria', 'local_fuzzylogic') : get_string('editglobalcriteria', 'local_fuzzylogic');
        $action = FUZZYLOGIC_ACTION_EDIT;

    } else {
        // Add new.
        $criteria = file_prepare_standard_editor($criteria, 'description', $editoroptions, $context);
        $strheading = ($globalcriteria==false) ?  get_string('addcriteria', 'local_fuzzylogic') : get_string('addglobalcriteria', 'local_fuzzylogic');
        $action = FUZZYLOGIC_ACTION_ADD;
    }

    $PAGE->set_title($strheading);
    $PAGE->set_heading($COURSE->fullname);
    $PAGE->navbar->add($strheading);
    $mform = new local_fuzzylogic_criteria_form(null, array(
                                                              'cid'=>$context->id, 
                                                              'structureid'=>$structureid, 
                                                              'data'=>$criteria, 
                                                              'editoroptions'=>$editoroptions,
                                                              'action'=> $action,
                                                              'g'=> $globalcriteria
                                                            ), 'post');

    if ($mform->is_cancelled()) {
        redirect( new moodle_url('/local/fuzzylogic/criteria.php', array('cid'=>$context->id, 'structureid'=>$structureid, 'action'=>FUZZYLOGIC_ACTION_LIST)) );
    } else if ($mform->is_submitted() && $mform->is_validated() ) {
      $data = $mform->get_data();
      $data = file_postupdate_standard_editor($data, 'description', $editoroptions, $context);

      if ($data->id) {
          local_fuzzylogic_update_criteria($data);
     
      } else {
          $criteriaid = local_fuzzylogic_add_criteria($data);
      }
      
      //Save assessments
      foreach ($data->assess_id as $num=>$value){
        $assessment = new stdClass();
        $assessment->id = $data->assess_id[$num];
        $assessment->instanceid = $data->assess_instanceid[$num];
        $assessment->assesmenttype = $data->assess_assesmenttype[$num];
        $assessment->linguistictag = $data->assess_linguistictag[$num];
        $assessment->feedback = $data->assess_feedback[$num];
        $assessment->param_a = $data->assess_param_a[$num];
        $assessment->param_b = $data->assess_param_b[$num];
        $assessment->param_c = $data->assess_param_c[$num];
        $assessment->param_d = $data->assess_param_d[$num];
        $assessment->param_e = $data->assess_param_e[$num];
        
        //Borro la valoración en caso de que venga vacia
        if (  $assessment->linguistictag == '' || 
              $assessment->feedback == '' || 
              $assessment->param_a == '' ||
              $assessment->param_b == '' ||
              $assessment->param_c == '' ||
              $assessment->param_d == ''
            ) 
        {
            if ($assessment->id != 0){ 
               local_fuzzylogic_delete_assesment($assessment->id);
            }
        } else {
          $assessment->instanceid = ($assessment->instanceid==0) ? $criteriaid :  $assessment->instanceid;
        
          if ($assessment->id == 0) {
            unset($assessment->id);
            local_fuzzylogic_add_assessment($assessment);
          } else {
            local_fuzzylogic_update_assessment($assessment);
          }
        }
        
        
      }
      
      //Save entries
       foreach ($data->entry_id as $num=>$value){
        $entry = new stdClass();
        $entry->id = $data->entry_id[$num];
        $entry->criteriaid = $data->entry_criteriaid[$num];
        $entry->entryid = $data->entry_entryid[$num];
        $entry->entrytype = $data->entry_entrytype[$num];

        
        //Borro la entrada en caso de que venga vacia
        if (  $entry->entryid == 0 ) 
        {
            if ($entry->id != 0){ 
               local_fuzzylogic_delete_criteriaentry($entry->id);
            }
        } else {
          $entry->criteriaid = ($entry->criteriaid==0) ? $criteriaid :  $entry->criteriaid;
        
          if ( local_fuzzylogic_logic_criteriaentry_exists($entry) ) continue;
          
          if ($entry->id == 0) {
            unset($entry->id);
            local_fuzzylogic_add_criteriaentry($entry);
          } else {
            local_fuzzylogic_update_criteriaentry($entry);
          }
        }
        
        
      }
      
      local_fuzzylogic_create_grade_structure ($structureid);
      
      redirect( new moodle_url('/local/fuzzylogic/criteria.php', array('cid'=>$context->id, 'structureid'=>$structureid, 'action'=>FUZZYLOGIC_ACTION_LIST)) );
    }
    
    echo $OUTPUT->header();
    echo $OUTPUT->heading($strheading);
    $mform->display();
    break;
  CASE FUZZYLOGIC_ACTION_DELETE:
    $criteriaid = optional_param('id', 0, PARAM_INT);
    $confirm = optional_param('confirm', 0, PARAM_BOOL);
  
    if ($confirm and confirm_sesskey()) {
        local_fuzzylogic_delete_criteria($criteriaid);
        $returnurl = new moodle_url('/local/fuzzylogic/criteria.php', array('cid' => $contextid, 'structureid'=>$structureid, 'action' => FUZZYLOGIC_ACTION_LIST));
        local_fuzzylogic_create_grade_structure ($structureid);
        redirect($returnurl);
    }
    $strheading = get_string('delcriteria', 'local_fuzzylogic');
    $PAGE->navbar->add($strheading);
    $PAGE->set_title($strheading);
    $PAGE->set_heading($COURSE->fullname);
    echo $OUTPUT->header();
    echo $OUTPUT->heading($strheading);
    $deleteurl = new moodle_url('/local/fuzzylogic/criteria.php', array('cid' => $contextid, 'structureid'=>$structureid, 'id'=>$criteriaid, 'action' => FUZZYLOGIC_ACTION_DELETE, 'confirm'=>1, 'sesskey'=>sesskey()));
    $returnurl = new moodle_url('/local/fuzzylogic/criteria.php', array('cid' => $contextid, 'structureid'=>$structureid, 'action' => FUZZYLOGIC_ACTION_LIST));
    echo $OUTPUT->confirm(get_string('confirmdeletecriteria', 'local_fuzzylogic'), $deleteurl, $returnurl);
    break;
    
  CASE FUZZYLOGIC_ACTION_DUPLICATE:
    require_capability('local/fuzzylogic:duplicateconcepts', $context);
    
    $criteriaid = optional_param('id', 0, PARAM_INT);
      
    $result = local_fuzzylogic_duplicate_criteria($criteriaid);
    
    $returnurl = new moodle_url('/local/fuzzylogic/criteria.php', array('cid' => $contextid, 'structureid'=>$structureid, 'action' => FUZZYLOGIC_ACTION_LIST));
    if ($result == true) {
        redirect($returnurl);
    } else {
        error(get_string('error_duplicating_criteria', 'local_fuzzylogic'), $returnurl);
    }

    break;
}

echo $OUTPUT->footer();
