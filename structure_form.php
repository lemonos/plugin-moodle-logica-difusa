<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The form used at the guide editor page is defined here
 *
 * @package    local_fuzzylogic
 * @copyright  2013 Oscar Ruesga <oscar@ruesga.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/lib/formslib.php');


/**
 * Defines the fuzzy logic structure edit form
 *
 * @package    local_fuzzylogic
 * @copyright  2013 Oscar Ruesga <oscar@ruesga.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class local_fuzzylogic_structure_form extends moodleform {

    /**
     * Form element definition
     */
    public function definition() {
        $form = $this->_form;
        
        $editoroptions = $this->_customdata['editoroptions'];
        $structure = $this->_customdata['data'];       
      

        $form->addElement('hidden', 'cid', $this->_customdata['cid']);
        $form->setType('cid', PARAM_INT);
        
        $form->addElement('hidden', 'structureid', $structure->id);
        $form->setType('structureid', PARAM_INT);
        
        $form->addElement('hidden', 'action', $this->_customdata['action']);
        $form->setType('action', PARAM_ALPHA);
        
        $form->addElement('hidden', 'courseid', $this->_customdata['courseid']);
        $form->setType('courseid', PARAM_INT);
        
        $form->addElement('hidden', 'id');
        $form->setType('id', PARAM_INT);

         // Name.
        $form->addElement('text', 'name', get_string('name', 'local_fuzzylogic'), array('size'=>52));
        $form->addRule('name', get_string('required'), 'required');
        $form->setType('name', PARAM_TEXT);

        // Description.
        $form->addElement('editor', 'description_editor', get_string('description', 'local_fuzzylogic'), null, $editoroptions);
        $form->setType('description_editor', PARAM_RAW);
        
        // Concepts
        $conceptsoption = array(0=>get_string('conceptselection', 'local_fuzzylogic'));
        $allconcepts = local_fuzzylogic_get_concepts($this->_customdata['courseid'], 0, 10000);
        foreach ($allconcepts['concepts'] as $allconcept){
          $conceptsoption[$allconcept->id] = '(' . $allconcept->shortname . ') ' . $allconcept->name;
        }
        
        $form->addElement('header', 'concepthdr',  get_string('associateconncepts', 'local_fuzzylogic'));
        $repeated = array();
        $repeatedoptions = array();
        $repeated[] = $form->createElement('select', 'concept_conceptid', get_string('concept','local_fuzzylogic') . ' {no}', $conceptsoption);
        $repeatedoptions['concept_conceptid']['type'] = PARAM_INT;        
        $repeated[] = $form->createElement('hidden', 'concept_id', $structure->concept_id);
        $repeatedoptions['concept_id']['type'] = PARAM_INT;
        $repeated[] = $form->createElement('hidden', 'concept_structureid', $structure->id);
        $repeatedoptions['concept_structureid']['type'] = PARAM_INT;
 
        // Get full version (including condition info) of section object 
        $count = local_fuzzylogic_count_conceptstructure($structure->id)+1;

        // Grade conditions
        $this->repeat_elements($repeated, $count, $repeatedoptions, 'numconcept',
                'addconcept', 1, get_string('addconcept', 'local_fuzzylogic'), true);
       
        $this->add_action_buttons();
        
        $this->set_data($structure);
    }

    /**
     * Setup the form depending on current values. This method is called after definition(),
     * data submission and set_data().
     * All form setup that is dependent on form values should go in here.
     *
     * We remove the element status if there is no current status (i.e. guide is only being created)
     * so the users do not get confused
     */
    public function definition_after_data() {
       
    }

    /**
     * Form vlidation.
     * If there are errors return array of errors ("fieldname"=>"error message"),
     * otherwise true if ok.
     *
     * @param array $data array of ("fieldname"=>value) of submitted data
     * @param array $files array of uploaded files "element_name"=>tmp_file_path
     * @return array of "element_name"=>"error_description" if there are errors,
     *               or an empty array if everything is OK (true allowed for backwards compatibility too).
     */
    public function validation($data, $files) {
      global $DB;  
      
      $err = parent::validation($data, $files);
      $err = array();
      $form = $this->_form;
 
     
        
      return $err;
    }

    /**
     * Return submitted data if properly submitted or returns NULL if validation fails or
     * if there is no submitted data.
     *
     * @return object submitted data; NULL if not valid or not submitted or cancelled
     */
    public function get_data() {
        $data = parent::get_data();

        return $data;
    }

}


/**
 * Defines the fuzzy logic structure edit form
 *
 * @package    local_fuzzylogic
 * @copyright  2013 Oscar Ruesga <oscar@ruesga.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class local_fuzzylogic_import_structure_form extends moodleform {

    /**
     * Form element definition
     */
    public function definition() {
        $form = $this->_form;
        $sources = $this->_customdata['data'];       

        $form->addElement('hidden', 'cid', $this->_customdata['cid']);
        $form->setType('cid', PARAM_INT);
        
        $form->addElement('hidden', 'action', FUZZYLOGIC_ACTION_DUPLICATE);
        
        
        foreach ($sources as $id=>$value){
            $coursetitle = get_string('course') . ': ' . $value['name'];
            $form->addElement('header', 'course_'.$id, $coursetitle);
            foreach( $value['structures'] as $key=>$title){
                $form->addElement('radio', 'source', null, $title, $key);
            }
        }

         
        $this->add_action_buttons(false, get_string('importstructure', 'local_fuzzylogic'));
        
    }

    /**
     * Setup the form depending on current values. This method is called after definition(),
     * data submission and set_data().
     * All form setup that is dependent on form values should go in here.
     *
     * We remove the element status if there is no current status (i.e. guide is only being created)
     * so the users do not get confused
     */
    public function definition_after_data() {
       
    }

    /**
     * Form vlidation.
     * If there are errors return array of errors ("fieldname"=>"error message"),
     * otherwise true if ok.
     *
     * @param array $data array of ("fieldname"=>value) of submitted data
     * @param array $files array of uploaded files "element_name"=>tmp_file_path
     * @return array of "element_name"=>"error_description" if there are errors,
     *               or an empty array if everything is OK (true allowed for backwards compatibility too).
     */
    public function validation($data, $files) {
      global $DB;  
      
      $err = parent::validation($data, $files);
      $err = array();
      $form = $this->_form;
 
     
        
      return $err;
    }

    /**
     * Return submitted data if properly submitted or returns NULL if validation fails or
     * if there is no submitted data.
     *
     * @return object submitted data; NULL if not valid or not submitted or cancelled
     */
    public function get_data() {
        $data = parent::get_data();

        return $data;
    }

}
/**
 * Defines the fuzzy logic file picker structure edit form
 *
 * @package    local_fuzzylogic
 * @copyright  2013 Oscar Ruesga <oscar@ruesga.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class local_fuzzylogic_fileimport_form extends moodleform {

    /**
     * Form element definition
     */
    public function definition() {
        $form = $this->_form;
        
        $form->addElement('hidden', 'cid', $this->_customdata['cid']);
        $form->setType('cid', PARAM_INT);
        
        $form->addElement('hidden', 'action', FUZZYLOGIC_ACTION_XML_IMPORT);
        
        // The file to import
        $form->addElement('header', 'importfileupload', get_string('importstructurefromfile', 'local_fuzzylogic'));

        $form->addElement('filepicker', 'newfile', get_string('import'));
        $form->addRule('newfile', null, 'required', null, 'client');

        // Submit button.
        $form->addElement('submit', 'submitbutton', get_string('import'));
        
    }

    /**
     * Checks that a file has been uploaded, and that it is of a plausible type.
     * @param array $data the submitted data.
     * @param array $errors the errors so far.
     * @return array the updated errors.
     */
    protected function validate_uploaded_file($data, $errors) {
       
        if (empty($data['newfile'])) {
            $errors['newfile'] = get_string('required');
            return $errors;
        }

        $files = $this->get_draft_files('newfile');
        if (count($files) < 1) {
            $errors['newfile'] = get_string('required');
            return $errors;
        }
        
        $file = reset($files);

        if ( $file->get_mimetype() != 'application/xml'){
            $errors['newfile'] = get_string('importwrongfiletype', 'local_fuzzylogic');
        }

        return $errors;
    }
    
    /*function definition_after_data() {
        
        $form = $this->_form;
        
        $files = $this->get_draft_files('newfile');
        if (count($files) < 1) {
            $d =& $form->getElement('action');
            $d->setValue(FUZZYLOGIC_ACTION_LIST);
        }
        
        $file = reset($files);

        if ( $file->get_mimetype() != 'application/xml'){
            $d =& $form->getElement('action');
            $d->setValue(FUZZYLOGIC_ACTION_LIST);
        }
    }*/

    /**
     * Form vlidation.
     * If there are errors return array of errors ("fieldname"=>"error message"),
     * otherwise true if ok.
     *
     * @param array $data array of ("fieldname"=>value) of submitted data
     * @param array $files array of uploaded files "element_name"=>tmp_file_path
     * @return array of "element_name"=>"error_description" if there are errors,
     *               or an empty array if everything is OK (true allowed for backwards compatibility too).
     */
    public function validation($data, $files) {
        $errors = parent::validation($data, $files);
        $errors = $this->validate_uploaded_file($data, $errors);
        
        if ($errors == 0) {
            $form = $this->_form;
            $d =& $form->getElement('action');
            $d->setValue(FUZZYLOGIC_ACTION_LIST);
        }
        return $errors;
    }

    /**
     * Return submitted data if properly submitted or returns NULL if validation fails or
     * if there is no submitted data.
     *
     * @return object submitted data; NULL if not valid or not submitted or cancelled
     */
    public function get_data() {
        $data = parent::get_data();

        return $data;
    }

}
