<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The form used at the concept2question selector editor page is defined here
 *
 * @package    local_fuzzylogic
 * @copyright  2013 Oscar Ruesga <oscar@ruesga.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/lib/formslib.php');


/**
 * Associate Questions & Concepts edit form
 *
 * @package    local_fuzzylogic
 * @copyright  2013 Oscar Ruesga <oscar@ruesga.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class local_fuzzylogic_concept2question_form extends moodleform {

    /**
     * Form element definition
     */
    public function definition() {
      global $DB;
        $form = $this->_form;
        
        $conceptoptions = $this->_customdata['conceptoptions'];       
        $questions = $this->_customdata['questions'];
        $questionconcepts = $this->_customdata['questionconcepts'];
        $quizid = $this->_customdata['quizid'];
        $structureid = $this->_customdata['structureid'];

        $form->addElement('hidden', 'cid', $this->_customdata['cid']);
        $form->setType('cid', PARAM_INT);

        $form->addElement('hidden', 'action', $this->_customdata['action']);
        $form->setType('action', PARAM_ALPHA);
        
        $form->addElement('hidden', 'quizid', $quizid);
        $form->setType('quizid', PARAM_INT);
        
        $form->addElement('hidden', 'structureid', $structureid);
        $form->setType('structureid', PARAM_INT);
        
        $counter=0;
        foreach ($questions as $question) {
          $form->addElement('header', 'question_'.$counter.'hdr', get_string('question', 'local_fuzzylogic', ($counter+1) ) );
           
          
          $form->addElement('hidden', 'question_instanceid['.$counter .']', $question->questioninstanceid);
          $form->setType('question_instanceid['.$counter .']', PARAM_INT);
          
          $question_text = '<pre>' . $question->title;       
          $question_text .= '<ul>';
          foreach($question->answers as $answer){
            $question_text .= '<li>' . $answer . '</li>';
          }
          $question_text .= '</ul><pre>';
          
          $form->addElement('static', 'question_'.$counter .'_tag', get_string('question', 'quiz'), $question_text);

          $repeated = array();
          $repeatedoptions = array();
          
          $repeated[] = $form->createElement('hidden', 'question_id['. $question->questioninstanceid  .']');
          $repeatedoptions['question_conceptid['.$question->questioninstanceid .']']['type'] = PARAM_INT;
          
          $repeatedoptions['assess_feedback']['type'] = PARAM_RAW;
          $repeated[] = $form->createElement('select', 'question_conceptid['. $question->questioninstanceid .']', get_string('concept', 'local_fuzzylogic'), $conceptoptions);
          
         $repeatedoptions['question_conceptid['. $question->questioninstanceid .']']['default'] = $questionconcepts[$question->questioninstanceid]->conceptid;
          
          $count = $DB->count_records('fuzzylogic_questionconcepts', array('quizid'=>$quizid, 'structureid'=>$structureid, 'questioninstanceid'=>$question->questioninstanceid) );
          
          
          $this->repeat_elements($repeated, $count, $repeatedoptions, 'numconcepts_'.$counter,
                'addconcept_'.$counter, 1, get_string('addconcept', 'local_fuzzylogic'), true);
          
          $counter++;
        }
        
        
        
        $this->add_action_buttons();

    }

    /**
     * Setup the form depending on current values. This method is called after definition(),
     * data submission and set_data().
     * All form setup that is dependent on form values should go in here.
     *
     * We remove the element status if there is no current status (i.e. guide is only being created)
     * so the users do not get confused
     */
    public function definition_after_data() {
      $form   = $this->_form;
  
      $questions = $this->_customdata['questions'];
      $questionconcepts = $this->_customdata['questionconcepts'];
      $quizid = $this->_customdata['quizid'];
      $structureid = $this->_customdata['structureid'];
      
      foreach ($questions as $question) {
        $q = $questionconcepts[$question->questioninstanceid];
        foreach ($q as $i=>$value){
          $form->setDefault('question_conceptid['. $question->questioninstanceid .'][' .$i .']', $value->conceptid);
          $form->setDefault('question_id['. $question->questioninstanceid .'][' .$i .']', $value->id);
        }
      }
    }

    /**
     * Form validation.
     * If there are errors return array of errors ("fieldname"=>"error message"),
     * otherwise true if ok.
     *
     * @param array $data array of ("fieldname"=>value) of submitted data
     * @param array $files array of uploaded files "element_name"=>tmp_file_path
     * @return array of "element_name"=>"error_description" if there are errors,
     *               or an empty array if everything is OK (true allowed for backwards compatibility too).
     */
    public function validation($data, $files) {
      global $DB;  
      
      $err = parent::validation($data, $files);
      $err = array();
     
      return $err;
    }

    /**
     * Return submitted data if properly submitted or returns NULL if validation fails or
     * if there is no submitted data.
     *
     * @return object submitted data; NULL if not valid or not submitted or cancelled
     */
    public function get_data() {
        $data = parent::get_data();

        return $data;
    }

}


class local_fuzzylogic_structurequiz_form extends moodleform {

    /**
     * Form element definition
     */
    public function definition() {
      $form = $this->_form;
      
      $form->addElement('header', 'stquizhdr', get_string('associate_test_structure', 'local_fuzzylogic'));
      $form->addElement('select', 'structureid',  get_string('structure', 'local_fuzzylogic'), $this->_customdata['structureoptions']);
      $form->setType('cid', PARAM_INT);
      $form->addElement('select', 'quizid',  get_string('quiz', 'local_fuzzylogic'), $this->_customdata['quizoptions']);
      $form->setType('cid', PARAM_INT);
      $form->addElement('hidden', 'cid', $this->_customdata['cid']);
      $form->setType('cid', PARAM_INT);
      $form->addElement('hidden', 'action', $this->_customdata['action']);
      $form->setType('action', PARAM_ALPHA);
      
      
      
      $this->add_action_buttons(false, get_string('asociate_questions', 'local_fuzzylogic'));
      
    }
}
