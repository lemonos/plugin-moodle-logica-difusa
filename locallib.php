<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * fuzzylogic functions
 *
 * @package     local_fuzzylogic
 * @copyright  2013 Oscar Ruesga (oscar@ruesga.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once(dirname(__FILE__).'/lib.php');

/**
 * Antecedent generator for Rules of Criterias
 */
class local_fuzzylogic_criteriarule_generator  {
    
    protected $criteriaid;
    private $matrix;
   
    private $entriesnames;
    private $valuesnames;

    public function __construct($criteriaid) {
        global $DB;
        $this->criteriaid = $criteriaid;
        $entries = local_fuzzylogic_get_criteriaentries($this->criteriaid );
        $this->matrix = array();
        $this->entriesnames = array();
        $this->valuesnames = array();
        
        $this->auxmatrix=array();
        
        
        foreach ($entries['entries'] as $entry){
          $instanceid = $entry->entryid;
          //En caso de que el tipo sea un concepto obtengo el concepto a partir de la estructura
          $arr = array();
          if ($entry->entrytype == 'concept'){
            $instanceid= $DB->get_field('fuzzylogic_structureconcepts', 'conceptid', array('id'=>$entry->entryid) );
            $this->entriesnames[$entry->id] = $DB->get_field('fuzzylogic_concepts', 'name', array('id'=>$instanceid) );
          } else {
            $this->entriesnames[$entry->id] = $DB->get_field('fuzzylogic_criteria', 'name', array('id'=>$instanceid) );
          }
          
          $assessments = local_fuzzylogic_get_assessments($instanceid, $entry->entrytype);
          $criteria_assessment = array();
          $c=1;
          foreach ($assessments as $assessment){
            $this->valuesnames[$assessment->id] = $assessment->linguistictag;
            $criteria_assessment[$assessment->id] = $assessment->id;
            $arr = array(
                                  'count'=> 0,
                                  'max'=>$c++
                      );
          }
          $this->matrix[$entry->id] = $criteria_assessment;
          $this->auxmatrix[] = $arr;
        }
       
    }
    
    
    public function get_rule_list($humanphrase=false){
      global $DB;
      
      //Obtenemos las reglas definidas previamente para el criterio
      $currentrules = $DB->get_records('fuzzylogic_criteriarule', array('criteriaid'=>$this->criteriaid));
      $rulelist = $this->map_rules();

      foreach ($rulelist as $rule){
        if ( !$this->in_array_field($rule->hash, 'hash', $currentrules) ){
          $updaterule = new stdClass();
          $updaterule->criteriaid= $this->criteriaid;
          $updaterule->hash = $rule->hash;
          $updaterule->antecedents= $rule->name;
          $rule->id = $DB->insert_record('fuzzylogic_criteriarule', $updaterule);
        }
      }

      foreach ($currentrules as $rule){
         if ( !$this->in_array_field($rule->hash, 'hash', $rulelist) ){
            $DB->delete_records('fuzzylogic_criteriarule', array('id'=>$rule->id));
        }
      }

      //Obtengo de nuevo las reglas actualizadas
      $rules = $DB->get_records('fuzzylogic_criteriarule', array('criteriaid'=> $this->criteriaid));
      if ($humanphrase == true) {
        foreach ($rules as $rule) {
          $rule->humanphrase = $this->get_human_phrase($rule->antecedents);
        }
      }
      return $rules;
    }
    
    public function get_human_phrase($logic_chain) {
      
      $predicates = explode('&', $logic_chain);
      foreach ($predicates as &$predicate) {
        list($antecedent, $consecuent) = explode ('=',$predicate);
        $antecedent = '<strong>' . $this->entriesnames[$antecedent] . '</strong>';
        $consecuent = '<strong>' . $this->valuesnames[$consecuent] . '</strong>';
        $predicate = $antecedent . ' ' . get_string ('EQUAL', 'local_fuzzylogic') . ' ' . $consecuent;
      }
      
      $phrase = get_string ('IF', 'local_fuzzylogic');
      $phrase .= ' ';
      $phrase .= implode(' '. get_string ('AND', 'local_fuzzylogic') . ' ', $predicates);
      
      return $phrase;
      
    }
    
    protected function in_array_field($needle, $needle_field, $haystack, $strict = false) { 
        if ($strict) { 
            foreach ($haystack as $item) 
                if (isset($item->$needle_field) && $item->$needle_field === $needle) 
                    return true; 
        } 
        else { 
            foreach ($haystack as $item) 
                if (isset($item->$needle_field) && $item->$needle_field == $needle) 
                    return true; 
        } 
        return false; 
    } 
    
    
    protected function generatematrix ($concepts_assessments) { 
      $concepts_assessments = array_reverse($concepts_assessments, true);
      reset($concepts_assessments);
      $line = array();
      $line = $this->buildfirstline($concepts_assessments);
      while (list($currentkey, $currentconceptvalue) = each($concepts_assessments) ){
        $pattern = $line;
        $auxline = array();
        next($currentconceptvalue);
        while ( list($k, $asset) = each($currentconceptvalue) ){
          $a= array();
          foreach ($pattern as $l) {
            $l[ $currentkey ] = $asset;
            $a[] = $l;
          }
          $auxline = array_merge($auxline, $a);
        }
        $line = array_merge($line, $auxline);
      }
      
      foreach ($line as &$l){
        $l= array_reverse($l, true);
      }
      return $line;
    }


    protected function buildfirstline($concepts_assessments) {
      $line = array();
      $line_elements = array(); 
      foreach($concepts_assessments as $concept=>$assesment){
        $line_elements[$concept] = current($assesment);
      }
      $line[] = $line_elements;
      return $line;
    }

    
    protected function map_rules() {
        $map = $this->generatematrix($this->matrix);

        $rules = array();
        
        foreach ( $map as $rule ) {
          $predicates = '';
          foreach ($rule as $concept=>$asset){  
           $predicates .= $concept . '=' . $asset . '&';
          }
          $predicates = trim($predicates, '&');
          $rule = new stdClass();
          $rule->name = $predicates;
          $rule->hash = sha1($predicates);
          $rules[] = $rule;
        }
        return $rules;
      
    }
    

}


/**
 * Algorithm of Fuzzylogic to Quiz
 */
class local_fuzzylogic_algorithm  {
  
  const  precision = 15; //Establezco la precision para redondear al nÃºmero mÃ¡ximo soportado por PHP  
  
  const range_inc = 0.05;
  
  private $userid;
  private $structureid;
  private $questions;
  
  private $concepts;
  private $criterias;
  private $global_criteria;
  
  private $logmsg;

  private $attemptid;   
  
  
  public function __construct($userid, $structureid, $questions, $attemptid) {
    $this->userid = $userid;
    $this->structureid = $structureid;
    $this->questions = $questions;
    
    $this->concepts = array();
    $this->criterias = array();
    $this->global_criteria = array();

    $this->attemptid = $attemptid;
    $this->logmsg = '';
    
    
    

  }
  
  public static function get_trapezoidal_membership_fuzzification ($x, $a, $b, $c, $d) {
      $fa = new local_fuzzylogic_algorithm();
      return $fa->trapezoidal_membership_fuzzification ($x, $a, $b, $c, $d);
  }
  
  public static function get_G_criteria_values ($criteriaid, $values, $globalcriteria) {
        $result_array = array();
        
        $fa = new local_fuzzylogic_algorithm();
        
        $w = array();

        foreach ($values as $value){
            $w[$value[0]] = ($value[1] == 'range') ? $value[1]  : (float) $value[1];
        }

        $countranges = array_count_values($w);
       
        switch ($countranges['range']){
            case 0:
                $defuzzificate = $fa->get_criteria_defuzzification_value($criteriaid, $w, null, $globalcriteria);
                return $defuzzificate->G;
                break;
            
            case 1:
                $result_array = array();
                foreach (range(0,1,self::range_inc) as $x) {
                    $a = array();
                    $keyrange = array_search('range', $w);
                    $w[$keyrange] = $x;
                    $defuzzificate = $fa->get_criteria_defuzzification_value($criteriaid, $w, null, $globalcriteria );
                    $a[] = $x;
                    $a[] = $defuzzificate->G;
                    $result_array[] = $a;
                    $w[$keyrange] = 'range';
                }
                
                break;
            
            case 2:
                $result_array = array();
                $i=0;
                foreach (range(0,1,self::range_inc) as $x) {
                    $j=0;
                    $result_array[$i] = array();
                    foreach (range(0,1,self::range_inc) as $y) {
                        $keyrange = array_keys ($w, 'range');
                        $ranges = array($x, $y);
                        foreach ($keyrange as $key) {
                            $pointer = each($ranges);
                            $w[$key] = $pointer['value'];
                        }
                        
                        $defuzzificate = $fa->get_criteria_defuzzification_value($criteriaid, $w, null, $globalcriteria);
                        $result_array[$i][$j++] = $defuzzificate->G;
                        foreach ($keyrange as $key) {
                            $w[$key] = 'range';
                        }
                    }
                    $i++;
                }
                break;
            default:
                return null;
        }
        return $result_array;
  }

  private function get_criteria_defuzzification_value ($criteriaid, $w=null, $instances=null, $isglobalcriteria=false) {
    global $DB;
    $prodWA = 0; 
    $sumW = 0;
    $bestassest_obj = null;

    //Obtengo instancia de criterio
    $criteria = local_fuzzylogic_get_criteria($criteriaid);

    $this->log ("CRITERIO: [$criteria->id]-{$criteria->name}",2);

    //Obtener criteria assesments
    $assessments = local_fuzzylogic_get_assessments($criteria->id, 'criteria');

    //Obtengo las reglas del criterio
    $rules = local_fuzzylogic_get_criteriarule ($criteria->id);
    
    $entrytype = ( $isglobalcriteria == true ) ? 'criteria' : 'concept';
    $idcol = ( $isglobalcriteria == true ) ? 'id' : 'conceptid';
    $table = ( $isglobalcriteria == true ) ? 'fuzzylogic_criteria' : 'fuzzylogic_structureconcepts';

    $criteria->W =array();   
    $sql = "
        SELECT  instance.{$idcol} as instanceid, criteriaentry.id as entryid
        FROM {{$table}} as instance
        INNER JOIN {fuzzylogic_criteriaentry} as criteriaentry ON instance.id=criteriaentry.entryid
        WHERE criteriaentry.criteriaid=? AND criteriaentry.entrytype=?";
    $criteria_entriesids = $DB->get_records_sql($sql, array($criteria->id, $entrytype));
    
    if ( $instances != null ) {
        $entriesinstances = array_intersect_key($instances, $criteria_entriesids);          
    } else {
        $entriesinstances = array();   
        $entriesinstances = ($isglobalcriteria) ? $this->get_criteria_entries ($w) : $this->get_concept_entries ($w);
    }
    
    foreach($entriesinstances as $key=>$values) {
        $this->log ("ENTRADA [{$values->id}] {$values->name}: G={$values->G} | TAG={$values->linguistictag}", 4);
        $entriesinstances[$key]->entryid = $criteria_entriesids[$key]->entryid; 
    }

    $rulegenerator = new local_fuzzylogic_criteriarule_generator($criteria->id);
    
    foreach ($assessments as &$assesstment){
        $this->log(" ;VALORACIÃƒâ€œN [$assesstment->id] {$assesstment->linguistictag}: a={$assesstment->param_a} | b={$assesstment->param_b} | c={$assesstment->param_c} | d={$assesstment->param_d}",4);
        $rules_in_assessment = array();
        $this->log(" ;REGLAS CON CONSECUENTE IGUAL A VALORACIÃƒâ€œN [$assesstment->id] {$assesstment->linguistictag}:",4);
        foreach($rules as $rule) {
          if ($rule->consequent == $assesstment->id) {

            $antecedent = $rulegenerator->get_human_phrase($rule->antecedents);
            $antecedent = str_replace(array('<strong>', '</strong>'),'',$antecedent);
            $this->log('----------------------------------------',8);

            $this->log("ANTECEDENTE: [{$rule->antecedents}] {$antecedent}",8);
            $this->log("CONCESUENTE: [{$rule->consequent}] {$assesstment->linguistictag}",8);
            $rules_in_assessment[] = $rule;
          }
        }

        $assesstment->rules = $rules_in_assessment;

      }

      //Aplico t-norma a instancia de valores
      foreach ($assessments as &$assesstment){  
        $this->log(" ;CALCULO DE LA T-NORMA PARA VALORACIÃƒâ€œN [$assesstment->id] {$assesstment->linguistictag}:",4);
        foreach ($assesstment->rules as &$rule) {
          $entryscores =$this->get_fuzzy_antecedent_scores ($rule->antecedents, $entriesinstances);
            
          $tnorm = null;
          foreach ($entryscores as $entryscore) {
            if ($tnorm === NULL) {
              $tnorm = $entryscore;
              continue;
            }
            $tnorm = $this->tnorm($tnorm, $entryscore, $criteria->t_norma);
          }
          $rule->t_norm =  $tnorm;
        }

       }

    //Aplico t-conorma a las t-normas obtenidas de las reglas
    $Gelements = new stdClass();
    $Gelements->membershipvalue = array();
    $Gelements->centroid = array();

    foreach ($assessments as &$assesstment){  
        $this->log(" ;CALCULO DE LA T-CONORMA PARA VALORACIÃƒâ€œN [$assesstment->id] {$assesstment->linguistictag}:",4);
        $tconorm = null;
        foreach ($assesstment->rules as &$rule) {
          $this->log(" ;T-NORMA DE LA REGLA [{$rule->antecedents} = {$rule->consequent}] = {$rule->t_norm}",8);
          if ($tconorm === NULL) {
            $tconorm = $rule->t_norm;
            continue;
          }
          $tconorm = $this->tconorm($tconorm, $rule->t_norm, $criteria->t_conorma);
        }

        $assest_obj = new stdClass();
        $assest_obj->membershipvalue = $tconorm;
        $assest_obj->centroid = ($assesstment->param_e == '' || $assesstment->param_e == null) ? $this->get_trapecioid_centroide($assesstment->param_a, $assesstment->param_b, $assesstment->param_c, $assesstment->param_d): $assesstment->param_e;
        $assest_obj->id = $assesstment->id;
        $assest_obj->linguistictag = $assesstment->linguistictag;
        $assest_obj->feedback = $assesstment->feedback;  

        $criteria->W[$assesstment->id] = $assest_obj;

        $this->log(" ;CRITERIO W[{$assesstment->id}]: membershipvalue={$assest_obj->membershipvalue}  | centroid={$assest_obj->centroid}",4);

        $Gelements->membershipvalue[] = $assest_obj->membershipvalue;
        $Gelements->centroid[] = $assest_obj->centroid;

        $prodWA +=  ($assest_obj->membershipvalue*$assest_obj->centroid);
        $sumW += $assest_obj->membershipvalue;
        $bestassest_obj = ( empty($bestassest_obj) ) ? $assest_obj : ( ($assest_obj->membershipvalue >= $bestassest_obj->membershipvalue) ? $assest_obj : $bestassest_obj );

    }

    $this->log(" ;G = (" . implode('+', $Gelements->membershipvalue) . ") * (" . implode('+', $Gelements->centroid) . ") / (" . implode('+', $Gelements->membershipvalue) . ")",4 );
    
    $G = $prodWA/$sumW;
    $G = (empty($G)) ? 0 : $G;

    $this->log(" ;CRITERIO G={$G}",4);
    
    $objresult = new stdClass();
    $objresult->G = $G;
    $objresult->bestassest_obj = $bestassest_obj;
    $objresult->W = $criteria->W;
    
    return $objresult;
}
  
            
  public function get_concept_entries ($w) {
      $returninstances = array();
      foreach ($w as $entryid=>$score){
            $concept = new stdClass();
            $assests = local_fuzzylogic_get_assessments($entryid, 'concept');
            $concept->W = array();
            foreach($assests as $assesst) {
                $obj = new stdClass();
                $obj->membershipvalue = ($assesst->param_e == '' || $assesst->param_e == null) ? $this->trapezoidal_membership_fuzzification($score,$assesst->param_a, $assesst->param_b, $assesst->param_c, $assesst->param_d) : $assesst->param_e;
                $obj->id = $assesst->id;
                $concept->W[$assesst->id] = $obj;
            }
            $returninstances[$entryid] = $concept;
        }
      return $returninstances;
  }
  
  public function get_criteria_entries ($w){
     $returninstances = array();

     foreach ($w as $entryid=>$score) {
        $criteria = local_fuzzylogic_get_criteria($entryid);
        $rules = local_fuzzylogic_get_criteriarule ($entryid);
        $assests = local_fuzzylogic_get_assessments($entryid, 'criteria');
        $criteria->W = array();

        foreach ($assests as &$assesst){
          $rules_in_assessment = array();
          foreach($rules as $rule) {
            if ($rule->consequent == $assesst->id) {
              $rules_in_assessment[] = $rule;
            }
          }

          $assesst->rules = $rules_in_assessment;

        }

        //Aplico t-norma a instancia de valores
        foreach ($assests as &$assesst){  
          foreach ($assesst->rules as &$rule) {
            $entryscores =$score;

            $tnorm = null;
            foreach ($entryscores as $entryscore) {
              if ($tnorm === NULL) {
                $tnorm = $entryscore;
                continue;
              }
              $tnorm = $this->tnorm($tnorm, $entryscore, $criteria->t_norma);
            }
            $rule->t_norm =  $score;
          }

         }


        foreach ($assests as &$assesst){  
            $tconorm = null;
            foreach ($assesst->rules as &$rule) {
              if ($tconorm === NULL) {
                $tconorm = $rule->t_norm;
                continue;
              }
              $tconorm = $this->tconorm($tconorm, $rule->t_norm, $criteria->t_conorma);
            }

            $assest_obj = new stdClass();
            $assest_obj->membershipvalue = $tconorm;
            $assest_obj->id = $assesstment->id;

            $criteria->W[$assesst->id] = $assest_obj;
        }
      
        $returninstances[$entryid] = $criteria;
          
      }
      return $returninstances;
  }
  
  public function concepts_valoration () {
    return $this->concepts;
  }
  
  public function criterias_valoration() {
    return $this->criterias;
  }
  
  public function global_criteria_valoration () {
    return $this->global_criteria;
  }
  
  public function execute () {
    $this->concepts = $this->concept_valoration();
    $this->criterias = $this->criteria_valoration($this->concepts);
    $this->global_criteria = $this->criteria_valoration($this->criterias, true);
    
    $this->save_log();
  }
  
  private function concept_valoration () { 
    global $DB;
    
    $score = array();
    $concepts = array();
   
    $this->log(" ;--- CUESTIONES");
    foreach ($this->questions as $question) {
      
      $objscore = new stdClass();
      
      
      $objscore->maxscore = $question->max_score;
      
      $objscore->score = $question->score;

      $this->log(" ;conceptid: {$question->conceptid} / questionid: {$question->instanceid}  / score: {$objscore->score}  / max_score:  {$objscore->maxscore}");
      $concepts[$question->conceptid]->score = (empty($concepts[$question->conceptid]->score) ) ? 0 : $concepts[$question->conceptid]->score;
      $concepts[$question->conceptid]->score += $objscore->score;
      
      $concepts[$question->conceptid]->maxscore = (empty($concepts[$question->conceptid]->maxscore) ) ? 0 : $concepts[$question->conceptid]->maxscore;
      $concepts[$question->conceptid]->maxscore += $objscore->maxscore;
      
      $concepts[$question->conceptid]->id = $question->conceptid;
    }
    $this->log(" ;------------------------------------------------");
    $this->log(" ;--- VALORACIÃƒâ€œN DE CONCEPTOS");
    foreach ($concepts as &$concept) {
      $conceptname = $DB->get_field( 'fuzzylogic_concepts', 'name', array('id'=>$concept->id) );  
      $this->log(" ;CONCEPTO: [{$concept->id}] {$conceptname}");
      $concept->G = $concept->score / $concept->maxscore;
      $concept->G = ($concept->G<0) ? 0 : $concept->G;
      
      $this->log(" ;CALCULO EL VALOR G DEL CONCEPTO:  G = SUM(score) / SUM(maxscore) => G= {$concept->score} / {$concept->maxscore} = {$concept->G}", 2);
      
      $assesments = local_fuzzylogic_get_assessments($concept->id, 'concept');
      $concept->W = array();
      $bestassest_obj = null;
      foreach($assesments as $assesment) {
        
        $assest_obj = new stdClass();
        $assest_obj->membershipvalue = ($assesment->param_e == '' || $assesment->param_e == null) ? $this->trapezoidal_membership_fuzzification($concept->G,$assesment->param_a, $assesment->param_b, $assesment->param_c, $assesment->param_d) : $assesment->param_e;
        $assest_obj->linguistictag = $assesment->linguistictag;
        $assest_obj->feedback = $assesment->feedback;  
        $assest_obj->id = $assesment->id;
        
        if ($assesment->param_e == '' || $assesment->param_e == null) {
            $this->log(" ;VALORACIÃƒâ€œN {$assest_obj->linguistictag}: a={$assesment->param_a} / b={$assesment->param_b} / c={$assesment->param_c} / d='{$assesment->param_d} / valor de pertenencia={$assest_obj->membershipvalue}",4);
        } else {
            $this->log(" ;VALORACIÃƒâ€œN {$assest_obj->linguistictag}: Alternativa manual al centroide={$assesment->param_e}");
        }
        $bestassest_obj = ( empty($bestassest_obj) ) ? $assest_obj : ( ($assest_obj->membershipvalue >= $bestassest_obj->membershipvalue) ? $assest_obj : $bestassest_obj );       
        
        $concept->W[$assesment->id] = $assest_obj;
      }
      
      $concept->best_assessment = $bestassest_obj;
      $concept->name = $conceptname;
      $concept->linguistictag = $bestassest_obj->linguistictag;
      $concept->feedback = $bestassest_obj->feedback;
      $this->log(" ;VALORACIÃƒâ€œN DEL CONCEPTO {$conceptname} -> NUMERICA: {$bestassest_obj->membershipvalue} / LINGUISTICA: {$bestassest_obj->linguistictag} / FEEDBACK: {$bestassest_obj->feedback}", 2);
    }
    $this->log(" ;------------------------------------------------");
    return $concepts; 
  }
  
  
  
  private function criteria_valoration ($instances, $isglobalcriteria=false) {
    global $DB;
    
    $params =array();
    $params['structureid'] = $this->structureid;
    $params['global'] = ( $isglobalcriteria == true ) ? 1 : 0;
    
    $this->log( ($isglobalcriteria) ? "--- VALORACIÃ“N DE CRITERIO GLOBAL" : "--- VALORACIÃ“N DE CRITERIOS");
    
    $criterias = $DB->get_records('fuzzylogic_criteria', $params );
    
    foreach ($criterias as &$criteria) {
   
      $defuzzificate = $this->get_criteria_defuzzification_value($criteria->id, null, $instances, $isglobalcriteria);
      
      $criteria->G = $defuzzificate->G;
      $criteria->best_assessment = $defuzzificate->bestassest_obj;
      $criteria->linguistictag   = $defuzzificate->bestassest_obj->linguistictag;
      $criteria->feedback   = $defuzzificate->bestassest_obj->feedback;
      $criteria->W = $defuzzificate->W;
      //$this->log(" ;CRITERIO G={$criteria->G}",4);
      $this->log(" ;CRITERIO linguistictag={$criteria->linguistictag}",4);
      $this->log(" ;CRITERIO feedback={$criteria->feedback}",4);
    }
    
    return $criterias;
  }
    

  private function get_fuzzy_antecedent_scores($antecedent, $entryinstances) {
    $scores = array ();
    $elements = explode('&', $antecedent);
    $this->log("VALORES DE PERTENENCIA DE LAS ENTRADAS",8);
    foreach($elements as $element){
      list($entryid, $assesmentid) = explode ('=', $element);
       
      foreach ($entryinstances as $instance) {
        if ($instance->entryid == $entryid) {
          $this->log(" [{$instance->id}]{$instance->name} = {$instance->W[$assesmentid]->membershipvalue}",12);  
          $scores[] = $instance->W[$assesmentid]->membershipvalue;
          break;
        }
      }
    }
    return $scores;
  } 

  private function tnorm($a, $b, $operation) {
    $a = round($a, self::precision);
    $b = round($b, self::precision);
    switch ($operation) {
        case FUZZYLOGIC_TNORMA_LUKASIEWICZ:
          $result = max(0, ($a+$b+1));
          $this->log(" ;tnorm (a:$a,b:$b,LUKASIEWICZ) => max(0, (a:$a+b:$b+1))=$result",12);
          return $result;
          break;
         
        case FUZZYLOGIC_TNORMA_MINIMO:
          $result = min($a,$b);
          $this->log(" ;tnorm (a:$a,b:$b,MINIMO) => min(a:$a,b:$b)=$result",12);
          return $result;
          break;
         
         
        case FUZZYLOGIC_TNORMA_PRODUCTO:
            $result = $a*$b;
            $this->log(" ;tnorm (a:$a,b:$b,PRODUCTO) => a:$a*b:$b=$result",12);
            return $result;
            break;
    }
    return null;
  }
  
  private function tconorm ($a, $b, $operation) {
    $a = round($a, self::precision);
    $b = round($b, self::precision);  
    switch ($operation) {
        case FUZZYLOGIC_TCONORMA_LUKASIEWICZ:
           $result = min( ($a+$b), 1);
           $this->log(" ;tconorm (a:$a,b:$b,LUKASIEWICZ) => min( (a:$a+b:$b), 1)=$result",8);
           return $result;
           break;
         
        case FUZZYLOGIC_TCONORMA_SUMAPRODUCTO:
           $result = ($a+$b)-($a*$b);
           $this->log(" ;tconorm (a:$a,b:$b,SUMAPRODUCTO) => (a:$a+b:$b)-(a:$a*b:$b)=$result",8);
           return $result;
           break;
         
        case FUZZYLOGIC_TCONORMA_MAXIMO:
           $result = max($a, $b);
           $this->log(" ;tconorm (a:$a,b:$b,MAXIMO) => max(a:$a, b:$b)=$result",8);
           return $result;
           break;
    }
    return null;
  }

  private function trapezoidal_membership_fuzzification ($x, $a, $b, $c, $d) {
    $x = round($x, self::precision);
    $a = round($a, self::precision);
    $b = round($b, self::precision);
    $c = round($c, self::precision);
    $d = round($d, self::precision);
    if ($d == 0) {
       $result = ($x==0) ? 1 : 0;
       $this->log(" ;trapezoidal_membership_fuzzification (x:$x,a:$a,b:$b,c:$c,d:$d) => $result",4);
       return $result;
    } else if ($a == 1) {
       $result = ($x==1) ? 1 : 0;
       $this->log(" ;trapezoidal_membership_fuzzification (x:$x,a:$a,b:$b,c:$c,d:$d) => $result",4);
       return $result;
    } else if ($b == 0 && $c == 1) {
       $result = ($x == 0) ? 0 : 1;
       $this->log(" ;trapezoidal_membership_fuzzification (x:$x,a:$a,b:$b,c:$c,d:$d) => $result",4);
       return $result;
    } else if ($a == $b && $b == 0 ) {
       if ($x>=0 && $x<$c) {
         $result = 1;
         $this->log(" ;trapezoidal_membership_fuzzification (x:$x,a:$a,b:$b,c:$c,d:$d) => $result",4);
         return $result;
       }
       if ($x>=$c && $x<$d) {
         $result = ($x-$d)/($c-$d);
         $this->log(" ;trapezoidal_membership_fuzzification (x:$x,a:$a,b:$b,c:$c,d:$d) => $result",4);
         return $result;
       } 
       if($x>=$d && $x<=1) {
         $result = 0;
         $this->log(" ;trapezoidal_membership_fuzzification (x:$x,a:$a,b:$b,c:$c,d:$d) => $result",4);
         return $result;
       }
       $result = 0;
       $this->log(" ;trapezoidal_membership_fuzzification (x:$x,a:$a,b:$b,c:$c,d:$d) => $result",4);
       return $result;
     } else if ($c == 1) {
       if ($x>=0 && $x<$a) {
         $result = 0;
         $this->log(" ;trapezoidal_membership_fuzzification (x:$x,a:$a,b:$b,c:$c,d:$d) => $result",4);
         return $result;
       }
       if ($x>=$a && $x<$b) {
         $result = ($x-$a)/($b-$a);
         $this->log(" ;trapezoidal_membership_fuzzification (x:$x,a:$a,b:$b,c:$c,d:$d) => $result",4);
         return $result;
       } 
       if ($x>=$b && $x<=1) {
         $result = 1;
         $this->log(" ;trapezoidal_membership_fuzzification (x:$x,a:$a,b:$b,c:$c,d:$d) => $result",4);
         return $result;
       }
       $result = 0;
       $this->log(" ;trapezoidal_membership_fuzzification (x:$x,a:$a,b:$b,c:$c,d:$d) => $result",4);
       return $result;
     } else {
       if(($x>=$a) && ($x<$b)) {
         $result = ($x-$a)/($b-$a);
         $this->log(" ;trapezoidal_membership_fuzzification (x:$x,a:$a,b:$b,c:$c,d:$d) => $result",4);
         return $result;
       }
       if (($x>=$b) && ($x<$c)){
         $result = 1;
         $this->log(" ;trapezoidal_membership_fuzzification (x:$x,a:$a,b:$b,c:$c,d:$d) => $result",4);
         return $result;
       } 
       if(($x>=$c) && ($x<$d)) {
         $result = ($x-$d)/($c-$d);
         $this->log(" ;trapezoidal_membership_fuzzification (x:$x,a:$a,b:$b,c:$c,d:$d) => $result",4);
         return $result;
       }
       $result = 0;
       $this->log(" ;trapezoidal_membership_fuzzification (x:$x,a:$a,b:$b,c:$c,d:$d) => $result",4);
       return $result;
     }
  }

  private function get_trapecioid_centroide ($a, $b, $c, $d) {
    $a = round($a, self::precision);
    $b = round($b, self::precision);
    $c = round($c, self::precision);
    $d = round($d, self::precision);
    if ($a == $d) {  
      $result = $a;
      $this->log(" ;get_trapecioid_centroide (a:$a,b:$b,c:$c,d:$d) => a:$result",4);
    } else {
      $result =  1/3 * ( ( pow($a,2) + ($b*$a) - pow($d,2) - ($d*$c) - pow($c,2) + pow($b,2) ) / ($a-$d+$b-$c));
      $this->log(" ;get_trapecioid_centroide (a:$a,b:$b,c:$c,d:$d) => 1/3 * ( ( pow(a:$a,2) + (b:$b*a:$a) - pow(d:$d,2) - (d:$d*c:$c) - pow(c:$c,2) + pow(b:$b,2) ) / (a:$a-d:$d+b:$b-c:$c))=$result",4);
    }
    return $result;
  }

  private function log($message, $indent=0){
      for ($i=0; $i<$indent; $i++) {
          $this->logmsg .= "\t";
      }
      $this->logmsg .= "$message\n";
  }

  private function save_log($debug_type=DEBUG_NORMAL){
      global $DB;
      $result_log = array();
      $result_log['userid'] =  $this->userid;
      $result_log['structureid'] = $this->structureid;
      $result_log['attemptid'] = $this->attemptid;
      $result_log['log'] = base64_encode($this->logmsg);
      $result_log['date'] = time();
      
      $DB->insert_record('fuzzylogic_results_log', $result_log);
    
  }
  
}
