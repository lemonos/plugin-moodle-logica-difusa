<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Concepts editor Page
 *
 * @package    local_fuzzylogic
 * @copyright  2013 Oscar Ruesga <oscar@ruesga.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once (dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/lib.php');
require_once(dirname(__FILE__).'/locallib.php');

$contextid = required_param('cid', PARAM_INT);
$assessment_instance_id  = required_param('id', PARAM_INT );
$assessment_instance_type  = required_param('type', PARAM_INT );
$globalcriteria  = optional_param('global', false, PARAM_BOOL );

$assessment_instance_type = ($assessment_instance_type == FUZZYLOGIC_TYPE_CONCEPT) ? 'concept' : 'criteria';

list($context, $course, $cm) = get_context_info_array($contextid);

require_login($course, true);
require_capability('local/fuzzylogic:manage', $context);

$title = get_string('graphs', 'local_fuzzylogic', $instancename);
$PAGE->set_url(new moodle_url('/local/fuzzylogic/assesstgraphs.php', array('cid' => $contextid, 'id' => $assessment_instance_id, 'type' => $assessment_instance_type)));
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->set_pagelayout('standard');




//Incluyo jquey y librerias de graficos
$PAGE->requires->js('/local/fuzzylogic/javascript/flot/excanvas.min.js', true);
$PAGE->requires->js('/local/fuzzylogic/javascript/flot/jquery.min.js', true);
$PAGE->requires->js('/local/fuzzylogic/javascript/flot/jquery.flot.js', true);

//Incluyo libreria de graficos 3d y librerias de google para criterios
if ($assessment_instance_type == 'criteria' && $globalcriteria == false) {
    
    
    $criteria = local_fuzzylogic_get_criteria($assessment_instance_id);
    $criteriaurl = new moodle_url('/local/fuzzylogic/criteria.php', array('cid'=>$context->id,'structureid'=>$criteria->structureid));
    $PAGE->navbar->add(get_string('criteria_definition', 'local_fuzzylogic'), $criteriaurl );
    
    $etype = ($globalcriteria == true) ? 'criteria' : 'concept';
    $entries = local_fuzzylogic_get_criteriaentries($criteria->id, $etype);
    
    foreach ($entries['entries'] as &$entry) {
        if ($entry->entrytype == 'concept') {
            $structureconcept = $DB->get_record('fuzzylogic_structureconcepts', array('id'=>$entry->entryid) );
            $entry->name = $DB->get_field('fuzzylogic_concepts', 'name', array('id'=>$structureconcept->conceptid));
            $entry->entryid = $structureconcept->conceptid;
        } else {
            $criteria = local_fuzzylogic_get_criteria($entry->entryid);
            $entry->name = $criteria->name;
        }
    }
    
    $ruleGraphType = null;
    if ( $entries ['totalentries'] == 3 ) {
        $ruleGraphType = '3D';
    } else if ($entries ['totalentries'] == 2 ) {
        $ruleGraphType = '2D';
    }     

    if ( $ruleGraphType == '3D' ) {
       $PAGE->requires->js('/local/fuzzylogic/javascript/JSSurfacePlot/SurfacePlot.js', true);
       $PAGE->requires->js('/local/fuzzylogic/javascript/JSSurfacePlot/ColourGradient.js', true);
       $PAGE->requires->js('/local/fuzzylogic/javascript/JSSurfacePlot/glMatrix-0.9.5.min.js', true);
       $PAGE->requires->js('/local/fuzzylogic/javascript/JSSurfacePlot/webgl-utils.js', true); 
    }
    
} 

$assessments = $DB->get_records('fuzzylogic_assessment', array('instanceid'=>$assessment_instance_id, 'assesmenttype'=>$assessment_instance_type) );

echo $OUTPUT->header();

$inc = 0.01;

foreach ($assessments as $assessment) {
    $linguistictag = $assessment->linguistictag;
    $datasets->$linguistictag = new stdClass();
    $datasets->$linguistictag->label = $linguistictag; 
    $arrdata = array();
    for ($i=0; $i<=1; $i+=$inc) {
        $a = array();
        $a[] = $i;
        $a[] = local_fuzzylogic_algorithm::get_trapezoidal_membership_fuzzification( (float) $i, $assessment->param_a, (float)$assessment->param_b, (float)$assessment->param_c, (float)$assessment->param_d);
        $arrdata[] = $a;
    }
    $datasets->$linguistictag->data = $arrdata;
}

$PAGE->requires->js_init_call('M.local_fuzzylogic.drawAssessmentGrapth', array( $datasets), true );

if ($assessment_instance_type == 'concept') {
    $instancename = $DB->get_field('fuzzylogic_concepts', 'name', array('id'=>$assessment_instance_id) );
} else {
    $instancename = $DB->get_field('fuzzylogic_criteria', 'name', array('id'=>$assessment_instance_id) );
}

echo $OUTPUT->heading(get_string('assessment_graphs', 'local_fuzzylogic', $instancename)); 
echo html_writer::start_tag('div', array('class'=>'graph-container'));
   echo html_writer::tag('div', '', array('id'=>'choices'));
    echo html_writer::tag('div', '', array('class'=>'graph-placeholder', 'id'=>'placeholder'));
    
echo html_writer::end_tag('div');

if ($assessment_instance_type == 'criteria' && $globalcriteria == false) {
    echo $OUTPUT->heading(get_string('entries_graph', 'local_fuzzylogic'));
    
    if ($ruleGraphType == '3D'){
        echo get_webGL_shaders();
    }
     
    echo html_writer::start_tag('div', array('class'=>'entriesgraph-container', 'id'=>'entriesgraph-container'));
    echo html_writer::tag('label', get_string('select_entry','local_fuzzylogic'));
    echo html_writer::empty_tag('br');
    echo html_writer::start_tag('form', array('id'=>'form'));
        foreach ($entries['entries'] as $e ) {
            echo html_writer::empty_tag('input', array('type'=>'checkbox', 'name'=>'entries', 'id'=>'entry_' . $e->entryid, 'value'=>$e->entryid));
            echo html_writer::tag('label', $e->name, array('for'=>'entry_' . $e->entryid));
            echo html_writer::empty_tag('input', array('type'=>'text', 'size'=>2, 'id'=>'input_entry_'.$e->entryid, 'name'=>'input_entry'.$e->entryid) );
            echo html_writer::empty_tag('br');
       }
       echo html_writer::empty_tag('input', array('type'=>'hidden', 'id'=>'criteriaid', 'value'=>$criteria->id ));
       echo html_writer::empty_tag('input', array('type'=>'button', 'name'=>'btnsend', 'value'=>get_string('generategraph', 'local_fuzzylogic') ));
    echo html_writer::end_tag('form');   
    echo html_writer::tag('div', '', array('class'=>'entriesgraph-placeholder', 'id'=>'entriesplaceholder'));
    echo html_writer::end_tag('div');

    $PAGE->requires->js_init_call('M.local_fuzzylogic.initRulesGraph', array($globalcriteria), true );

}

echo $OUTPUT->footer();


function get_webGL_shaders () {
    return '
        <script id="shader-fs" type="x-shader/x-fragment">
            #ifdef GL_ES
            precision highp float;
            #endif
            
            varying vec4 vColor;
            varying vec3 vLightWeighting;
            
            void main(void)
            {
            	gl_FragColor = vec4(vColor.rgb * vLightWeighting, vColor.a);
            }
        </script>
        
        <script id="shader-vs" type="x-shader/x-vertex">
            attribute vec3 aVertexPosition;
            attribute vec3 aVertexNormal;
            attribute vec4 aVertexColor;
            
            uniform mat4 uMVMatrix;
            uniform mat4 uPMatrix;
            uniform mat3 uNMatrix;
            varying vec4 vColor;
            
            uniform vec3 uAmbientColor;
            uniform vec3 uLightingDirection;
            uniform vec3 uDirectionalColor;
            varying vec3 vLightWeighting;
            
            void main(void)
            {
                gl_Position = uPMatrix * uMVMatrix * vec4(aVertexPosition, 1.0);
                
                vec3 transformedNormal = uNMatrix * aVertexNormal;
                float directionalLightWeighting = max(dot(transformedNormal, uLightingDirection), 0.0);
                vLightWeighting = uAmbientColor + uDirectionalColor * directionalLightWeighting; 

                vColor = aVertexColor;
            }
        </script>
        
        <script id="axes-shader-fs" type="x-shader/x-fragment">
            precision mediump float;
			varying vec4 vColor;
			
			void main(void)
			{
				gl_FragColor = vColor;
			}
        </script>
        
        <script id="axes-shader-vs" type="x-shader/x-vertex">
            attribute vec3 aVertexPosition;
			attribute vec4 aVertexColor;
			uniform mat4 uMVMatrix;
			uniform mat4 uPMatrix;
			varying vec4 vColor;
			uniform vec3 uAxesColour;
			
			void main(void)
			{
				gl_Position = uPMatrix * uMVMatrix * vec4(aVertexPosition, 1.0);
				vColor =  vec4(uAxesColour, 1.0);
			} 
        </script>
        
		<script id="texture-shader-fs" type="x-shader/x-fragment">
            #ifdef GL_ES
            precision highp float;
            #endif
            
            varying vec2 vTextureCoord;
            
            uniform sampler2D uSampler;
            
            void main(void)
            {
                gl_FragColor = texture2D(uSampler, vTextureCoord);
            }
        </script>
        
        <script id="texture-shader-vs" type="x-shader/x-vertex">
            attribute vec3 aVertexPosition;
            
            attribute vec2 aTextureCoord;
            varying vec2 vTextureCoord;
            
            uniform mat4 uMVMatrix;
            uniform mat4 uPMatrix;
            
            void main(void)
            {
                gl_Position = uPMatrix * uMVMatrix * vec4(aVertexPosition, 1.0);
                vTextureCoord = aTextureCoord; 
            }
        </script>
    ';
}