<?php

define('FUZZYLOGIC_ACTION_LIST',           'l');
define('FUZZYLOGIC_ACTION_EDIT',           'e');
define('FUZZYLOGIC_ACTION_ADD',            'a');
define('FUZZYLOGIC_ACTION_DELETE',         'd');
define('FUZZYLOGIC_ACTION_DUPLICATE',      'i');
define('FUZZYLOGIC_ACTION_XML_EXPORT',     'x');
define('FUZZYLOGIC_ACTION_XML_IMPORT',     'y');


define('FUZZYLOGIC_TNORMA_LUKASIEWICZ',     1);
define('FUZZYLOGIC_TNORMA_MINIMO',          2);
define('FUZZYLOGIC_TNORMA_PRODUCTO',        3);

define('FUZZYLOGIC_TCONORMA_LUKASIEWICZ',   1);
define('FUZZYLOGIC_TCONORMA_SUMAPRODUCTO',  2);
define('FUZZYLOGIC_TCONORMA_MAXIMO',        3);


define('FUZZYLOGIC_TYPE_CONCEPT',           1);
define('FUZZYLOGIC_TYPE_CRITERIA',          2);
define('FUZZYLOGIC_TYPE_GLOBALCRITERIA',    3);

function local_fuzzylogic_extends_settings_navigation($settingsnav, $context) {
    global $CFG, $PAGE, $DB, $USER;
 
    // Only add this settings item on non-site course pages.
    if (!$PAGE->course or $PAGE->course->id == 1) {
        return;
    }
 
    // Only let users with the appropriate capability see this settings item.
    
         
    if ($settingnode = $settingsnav->find('courseadmin', navigation_node::TYPE_COURSE)) {
        $str = get_string('pluginname', 'local_fuzzylogic');
        $fuzzynode = navigation_node::create(
            $str,
            null,
            navigation_node::NODETYPE_BRANCH,
            'fuzzylogic',
            'fuzzylogic',
            null
        );

        if (has_capability('local/fuzzylogic:manage', context_course::instance($PAGE->course->id))) {
         //Concepts Definition
          $str = get_string('concepts_definition', 'local_fuzzylogic');
          $url = new moodle_url('/local/fuzzylogic/concepts.php', array('cid' => $PAGE->context->id));
          $courseconceptnode = navigation_node::create(
            $str,
            $url,
            navigation_node::TYPE_CUSTOM,
            'course_concept',
            'course_concept',
            null
          );
          $urlcriteriagraph = new moodle_url('/local/fuzzylogic/assesstgraphs.php');

          
          if ($PAGE->url->compare($url, URL_MATCH_BASE) ||  
             ($PAGE->url->compare($urlcriteriagraph, URL_MATCH_BASE) && strpos( $PAGE->url->get_query_string(false), 'type=concept') !== FALSE )) 
          {
              $courseconceptnode->make_active();
          } 
          
          $fuzzynode->add_node($courseconceptnode);
        
        
          //Structure Definition
          $str = get_string('structure_definition', 'local_fuzzylogic');
          $url = new moodle_url('/local/fuzzylogic/structure.php', array('cid' => $PAGE->context->id));
          $structurenode = navigation_node::create(
            $str,
            $url,
            navigation_node::TYPE_CUSTOM,
            'sctructure_definition',
            'sctructure_definition',
            null
          );

          $urlcriteria = new moodle_url('/local/fuzzylogic/criteria.php');
          $urlrulecriteria = new moodle_url('/local/fuzzylogic/criteriarule.php');
          $urlcriteriagraph = new moodle_url('/local/fuzzylogic/assesstgraphs.php');
          if ($PAGE->url->compare($url, URL_MATCH_BASE) || 
              $PAGE->url->compare($urlcriteria, URL_MATCH_BASE) || 
              $PAGE->url->compare($urlrulecriteria, URL_MATCH_BASE) || 
              ($PAGE->url->compare($urlcriteriagraph, URL_MATCH_BASE) && strpos( $PAGE->url->get_query_string(false), 'type=criteria') !== FALSE ) ) {
                $structurenode->make_active();
          }
          $fuzzynode->add_node($structurenode);

          //Quiz Question Definitiion
          $str = get_string('quiz_question_definition', 'local_fuzzylogic');
          $url = new moodle_url('/local/fuzzylogic/concept2question.php', array('cid' => $PAGE->context->id));
          $quizquestionnode = navigation_node::create(
            $str,
            $url,
            navigation_node::TYPE_CUSTOM,
            'quiz_question_definition',
            'quiz_question_definition',
            null
          );

          if ($PAGE->url->compare($url, URL_MATCH_BASE)) {
              $quizquestionnode->make_active();
          }
          $fuzzynode->add_node($quizquestionnode);
        }
        
        $structures = $DB->get_records ('fuzzylogic_structure', array('courseid'=>$PAGE->course->id) );
        
        if (has_capability('local/fuzzylogic:viewreport', context_course::instance($PAGE->course->id)) ) {
          //Reports
          if (count($structures) > 0){
            $str = get_string('results', 'local_fuzzylogic');
            $resultsnode = navigation_node::create(
              $str,
              null,
              navigation_node::NODETYPE_BRANCH,
              'results',
              'results',
              null
            );   
            

             foreach($structures as $structure) {
               if ($DB->record_exists('fuzzylogic_results', array('structureid'=>$structure->id) ) == true ){
                 //Result structure node
                $str = $structure->name;
                $url = new moodle_url('/local/fuzzylogic/report.php', array('cid' => $PAGE->context->id, 'sid'=>$structure->id));
                $resultstructurenode = navigation_node::create(
                  $str,
                  $url,
                  navigation_node::TYPE_CUSTOM,
                  'result_structure_'.$structure->id,
                  'result_structure_'.$structure->id,
                  null
                );

               
                if ($PAGE->url->compare($url, URL_MATCH_EXACT) ) {
                    $structurenode->make_active();
                }
                
                $resultsnode->add_node($resultstructurenode);
               }
             }
             if ( $resultsnode->has_children() ) $fuzzynode->add_node($resultsnode);
          }
          
        } else if (has_capability('local/fuzzylogic:viewownreport', context_course::instance($PAGE->course->id)) ) {
           foreach($structures as $structure) {
                 //Result structure node
                $str = $structure->name;
                $url = new moodle_url('/local/fuzzylogic/report.php', array('cid' => $PAGE->context->id, 'sid'=>$structure->id, 'uid'=>$USER->id));
                $resultstructurenode = navigation_node::create(
                  $str,
                  $url,
                  navigation_node::TYPE_CUSTOM,
                  'result_structure_'.$structure->id,
                  'result_structure_'.$structure->id,
                  null
                );
                
                if ($PAGE->url->compare($url, URL_MATCH_EXACT)) {
                    $resultstructurenode->make_active();
                }
                $fuzzynode->add_node($resultstructurenode);
               }
        }
 
        if ( $fuzzynode->has_children() ) $settingnode->add_node($fuzzynode);
          
       }  
}

function local_fuzzylogic_get_concepts ($courseid=null, $page = 0, $perpage = 25, $search = ''){
  global $DB;

  // Add some additional sensible conditions
  $tests = array();
  $params = array();
  
  if ( !empty($courseid) ) {
    $tests[] = 'courseid = ?';
    $params[] = $courseid;
  } 

  if (!empty($search)) {
      $conditions = array('name', 'shortname', 'description');
      $searchparam = '%' . $DB->sql_like_escape($search) . '%';
      foreach ($conditions as $key=>$condition) {
          $conditions[$key] = $DB->sql_like($condition, "?", false);
          $params[] = $searchparam;
      }
      $tests[] = '(' . implode(' OR ', $conditions) . ')';
  }
  $wherecondition = implode(' AND ', $tests);

  $fields = "SELECT *";
  $countfields = "SELECT COUNT(1)";
  $sql = " FROM {fuzzylogic_concepts}
           WHERE $wherecondition";
  $order = " ORDER BY name ASC, shortname ASC";
  $totalconcepts = $DB->count_records_sql($countfields . $sql, $params);
  $concepts = $DB->get_records_sql($fields . $sql . $order, $params, $page*$perpage, $perpage);
  
  //Calculate total assesment
  
  foreach ($concepts as $concept) {
      $concept->totalassesments = local_fuzzylogic_count_assesstments ($concept->id, 'concept');
      
      
      //Desactivo la recuperaciÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â³n de assessment en el objeto concept para optimizar.
      /*if ($concept->totalassesments > 0) {
        $concept->assesments = local_fuzzylogic_get_assessments($concept->id, 'concept');
      } else {
        $concept->assesments =null;
      }*/
  }
 

  return array( 'totalconcepts' => $totalconcepts, 'concepts' => $concepts );
}

/**
 * Get a concept.
 * @param  int $concept
 * @return stdClass $concept
 */
function local_fuzzylogic_get_concept ($conceptid) {
  global $DB;
  
  return $DB->get_record('fuzzylogic_concepts', array('id'=> $conceptid) );
  
}

/**
 * Update a concept.
 * @param array $concept
 * @return Boolean 
 */
function local_fuzzylogic_update_concept ($concept) {
    global $DB;
    if (empty($concept->id)) return false;
    
    return $DB->update_record('fuzzylogic_concepts', $concept);

}


/**
 * Add a concept.
 * @param array $concept
 * @return int $id 
 */
function local_fuzzylogic_add_concept ($concept) {
    global $DB;
    
    return $DB->insert_record('fuzzylogic_concepts', $concept);

}

/**
 * Delete concept.
 * @param int $conceptid
 * @param Boolean $cascade Borra en cascada las instacias especificas del concepto asociadas a una estructura
 * @return void
 */
function local_fuzzylogic_delete_concept ($conceptid, $cascade=false) {
    global $DB;
    if (!$conceptid) return false;
    
    if ($cascade) {
      $structureconceptid = $DB->get_field('fuzzylogic_questionconcepts', 'id', array('conceptid'=>$conceptid) );
      $DB->delete_records('fuzzylogic_questionconcepts', array('structureconceptid'=>$structureconceptid));
      $DB->delete_records('fuzzylogic_structureconcepts', array('conceptid'=>$concept->id));
    }
    
    $DB->delete_records('fuzzylogic_assessment', array('instanceid'=>$conceptid, 'assesmenttype'=>'concept'));
    $DB->delete_records('fuzzylogic_concepts', array('id'=>$conceptid));

}

/**
 * Check if concept exists.
 * @param array $concept
 * @return Boolean 
 */
function local_fuzzylogic_concept_exists ($concept) {
    global $DB;
  
    $where = 'courseid=? AND shortname=?';
    $params = array ($concept['courseid'], $concept['shortname'] );
    if (!empty($concept['id'])) {
      $where .= ' AND id<>?';
      $params[]= $concept['id'];
    }
    
    return $DB->record_exists_select('fuzzylogic_concepts', $where, $params);
}

/**
 * Count assessments
 * @param  int $conceptid
 * @return stdClass $concept
 */
function local_fuzzylogic_count_assesstments ($instanceid=0, $assesmenttype=null) {
  global $DB;
  if (!$instanceid || !$assesmenttype) return 0;
  return $DB->count_records('fuzzylogic_assessment', array('assesmenttype'=>$assesmenttype, 'instanceid'=>$instanceid) );
}

/**
 * Count assessments
 * @param  array $conceptsearchparams
 * @return stdClass $concept
 */
function local_fuzzylogic_get_assessments ($instanceid=0, $assesmenttype=null) {
  global $DB;
  if (!$instanceid || !$assesmenttype) return null;
  return $DB->get_records('fuzzylogic_assessment', array('assesmenttype'=>$assesmenttype, 'instanceid'=>$instanceid) );
  
}

/**
 * Get an assessment.
 * @param  int $assessmentid
 * @return stdClass $concept
 */
function local_fuzzylogic_get_assessment ($instanceid=0, $assesmenttype=null) {
  global $DB;
  if (!$instanceid || !$assesmenttype) return null;
  return $DB->get_record('fuzzylogic_assessment', array('assesmenttype'=>$assesmenttype, 'instanceid'=>$instanceid) );
  
}

/**
 * Update an assessment.
 * @param array $assessment
 * @return Boolean 
 */
function local_fuzzylogic_update_assessment ($assessment) {
    global $DB;
    if (empty($assessment->id)) return false;
    
    if ($assessment->param_e == ''){
        $assessment->param_e = null;
    }
    
    return $DB->update_record('fuzzylogic_assessment', $assessment);

}


/**
 * Add an assessment.
 * @param array $assesment
 * @return int $id 
 */
function local_fuzzylogic_add_assessment ($assessment) {
    global $DB;
    
    if ($assessment->param_e == ''){
        $assessment->param_e = null;
    }
    
    return $DB->insert_record('fuzzylogic_assessment', $assessment);

}

/**
 * Delete an assessment.
 * @param int $assesmentid
 * @param int $assesmenttype
 * @return void
 */
function local_fuzzylogic_delete_assesment ($assesmentid) {
    global $DB;
    if (!$assesmentid) return false;

    $DB->delete_records('fuzzylogic_assessment', array('id'=>$assesmentid));
}


function local_fuzzylogic_get_structures ($courseid=null, $page = 0, $perpage = 25, $search = ''){
  global $DB;

  // Add some additional sensible conditions
  $tests = array();
  $params = array();
  
  if ( !empty($courseid) ) {
    $tests[] = 'courseid = ?';
    $params[] = $courseid;
  } 

  if (!empty($search)) {
      $conditions = array('name', 'description');
      $searchparam = '%' . $DB->sql_like_escape($search) . '%';
      foreach ($conditions as $key=>$condition) {
          $conditions[$key] = $DB->sql_like($condition, "?", false);
          $params[] = $searchparam;
      }
      $tests[] = '(' . implode(' OR ', $conditions) . ')';
  }
  $wherecondition = implode(' AND ', $tests);

  $fields = "SELECT *";
  $countfields = "SELECT COUNT(1)";
  $sql = " FROM {fuzzylogic_structure}";
  $sql .= ( empty ($wherecondition) ) ? "" : " WHERE $wherecondition";
  $order = " ORDER BY name ASC";
  $totalstructures = $DB->count_records_sql($countfields . $sql, $params);
  $structures = $DB->get_records_sql($fields . $sql . $order, $params, $page*$perpage, $perpage);
  
  //Calculate total assesment
  
  foreach ($structures as $structure) {
      $structure->totalcriteria = local_fuzzylogic_count_criteria ($structure->id);
      $structure->totalconcepts = local_fuzzylogic_count_conceptstructure ($structure->id);

  }
 

  return array( 'totalstructures' => $totalstructures, 'structures' => $structures );
}

/**
 * Get a structure.
 * @param  int $structureid
 * @return stdClass $structure
 */
function local_fuzzylogic_get_structure ($structureid) {
  global $DB;
  
  return $DB->get_record('fuzzylogic_structure', array('id'=> $structureid) );
  
}

/**
 * Update an structure.
 * @param array $structure
 * @return Boolean 
 */
function local_fuzzylogic_update_structure ($structure) {
    global $DB;
    if (empty($structure->id)) return false;
    
    return $DB->update_record('fuzzylogic_structure', $structure);

}


/**
 * Add a structure.
 * @param array $structure
 * @return int $id 
 */
function local_fuzzylogic_add_structure ($structure) {
    global $DB;
    
    return $DB->insert_record('fuzzylogic_structure', $structure);

}

/**
 * Delete structure.
 * @param int $structureid
 * @param Boolean $cascade Borra en cascada las instacias especificas de la estructura
 * @return void
 */
function local_fuzzylogic_delete_structure ($structureid, $cascade=false) {
    global $DB;
    if (!$structureid) return false;
    
    /*if ($cascade) {
      $structureconceptid = $DB->get_field('fuzzylogic_questionconcepts', 'id', array('conceptid'=>$conceptid) );
      $DB->delete_records('fuzzylogic_questionconcepts', array('structureconceptid'=>$structureconceptid));
      $DB->delete_records('fuzzylogic_structureconcepts', array('conceptid'=>$concept->id));
    }*/
    
    $DB->delete_records('fuzzylogic_structureconcepts', array('structureid'=>$structureid));
    $DB->delete_records('fuzzylogic_structure', array('id'=>$structureid));

}

/**
 * Check if a structure exists.
 * @param array $concept
 * @return Boolean 
 */
function local_fuzzylogic_structure_exists ($structure) {
    global $DB;
  
    $where = 'courseid=?';
    $params = array ($structure['courseid']);
    if (!empty($structure['id'])) {
      $where .= ' AND id<>?';
      $params[]= $concept['id'];
    }
    
    return $DB->record_exists_select('fuzzylogic_concepts', $where, $params);
}


/**
 * Get concepts associates to structure
 * @param  int $structureid
 * @return stcClass $concepts 
 */
function local_fuzzylogic_get_conceptstructure ($structureid=0, $includeconceptname=false) {
  global $DB;
  if (!$structureid ) return 0;

  $structureconcepts = $DB->get_records('fuzzylogic_structureconcepts', array('structureid'=>$structureid) );
  
  $concepts = array();
  
  if ($includeconceptname === false) {
    $concepts = $structureconcepts;
  } else {
    foreach ($structureconcepts as $structureconcept) {

      $concept = $DB->get_record('fuzzylogic_concepts', array('id'=>$structureconcept->conceptid), 'id, shortname, name' );
      if ($concept->id > 0) {
        $concept->structureid = $structureid;
        $concept->conceptid = $concept->id;
        $concept->id = $structureconcept->id;
        $concepts[] = $concept;
      }
    }
  }
  
  return $concepts;
  
}

/**
 * Update a concept associates to structure.
 * @param array $conceptstructure
 * @return Boolean 
 */
function local_fuzzylogic_update_conceptstructure ($conceptstructure) {
    global $DB;
    if (empty($conceptstructure->id)) return false;
    
    return $DB->update_record('fuzzylogic_structureconcepts', $conceptstructure);

}


/**
 * Add a concept associates to structure.
 * @param array $conceptstructure
 * @return int $id 
 */
function local_fuzzylogic_add_conceptstructure ($conceptstructure) {
    global $DB;
    
    return $DB->insert_record('fuzzylogic_structureconcepts', $conceptstructure);

}

/**
 * Delete a concept associates to structure.
 * @param int $conceptstructureid
 * @return void
 */
function local_fuzzylogic_delete_conceptstructure ($conceptstructureid) {
    global $DB;
    if (!$conceptstructureid) return false;

    $DB->delete_records('fuzzylogic_structureconcepts', array('id'=>$conceptstructureid));
}

/**
 * Count concepts associates to structure
 * @param  int $structureid
 * @return int 
 */
function local_fuzzylogic_count_conceptstructure ($structureid=0) {
  global $DB;
  if (!$structureid ) return 0;
  return $DB->count_records('fuzzylogic_structureconcepts', array('structureid'=>$structureid) );
}









/**
 * Count criteria
 * @param  int $structureid
 * @return int 
 */
function local_fuzzylogic_count_criteria ($structureid=0) {
  global $DB;
  if (!$structureid ) return 0;
  return $DB->count_records('fuzzylogic_criteria', array('structureid'=>$structureid) );
}

/**
 * Check if conceptstructure exists.
 * @param stdClass $conceptstructure
 * @return Boolean 
 */
function local_fuzzylogic_conceptstructure_exists ($conceptstructure) {
    global $DB;
  
    $where = 'structureid=? AND conceptid=?';
    $params = array ( $conceptstructure->structureid, $conceptstructure->conceptid );
   
    return $DB->record_exists_select('fuzzylogic_structureconcepts', $where, $params);
}


function local_fuzzylogic_get_criterias ($structureid=null, $global=null, $page = 0, $perpage = 25, $search = ''){
  global $DB;

  // Add some additional sensible conditions
  $tests = array();
  $params = array();
  
  if ( !empty($structureid) ) {
    $tests[] = 'structureid = ?';
    $params[] = $structureid;
  } 

  if (!empty($search)) {
      $conditions = array('name', 'shortname', 'description');
      $searchparam = '%' . $DB->sql_like_escape($search) . '%';
      foreach ($conditions as $key=>$condition) {
          $conditions[$key] = $DB->sql_like($condition, "?", false);
          $params[] = $searchparam;
      }
      $tests[] = '(' . implode(' OR ', $conditions) . ')';
  }
  $wherecondition = implode(' AND ', $tests);

  $fields = "SELECT *";
  $countfields = "SELECT COUNT(1)";
  $sql = " FROM {fuzzylogic_criteria}
           WHERE $wherecondition";
  $order = " ORDER BY name ASC, shortname ASC";
  $totalcriterias = $DB->count_records_sql($countfields . $sql, $params);
  $criterias = $DB->get_records_sql($fields . $sql . $order, $params, $page*$perpage, $perpage);
  
  //Calculate total entries and assesment
  $globalcriteria = null;
  foreach ($criterias as $index=>$criteria) {
      $criteria->totalassesments = local_fuzzylogic_count_assesstments ($criteria->id, 'criteria');
      $criteria->totalentries = local_fuzzylogic_count_criteriaentries ($criteria->id, $global);

      if ($criteria->global == 1) {
        $globalcriteria  = $criteria;
        unset($criterias[$index]); 
      } 
  }
 

  return array( 'totalcriterias' => $totalcriterias, 'criterias' => $criterias, 'globalcriteria'=> $globalcriteria);
}

/**
 * Get a criteria.
 * @param  int $criteriaid
 * @return stdClass $criteria
 */
function local_fuzzylogic_get_criteria ($criteriaid) {
  global $DB;
  
  return $DB->get_record('fuzzylogic_criteria', array('id'=> $criteriaid) );
  
}

/**
 * Update a criteria.
 * @param array $criteria
 * @return Boolean 
 */
function local_fuzzylogic_update_criteria ($criteria) {
    global $DB;
    if (empty($criteria->id)) return false;
    
    return $DB->update_record('fuzzylogic_criteria', $criteria);

}


/**
 * Add a criteria.
 * @param array $criteria
 * @return int $id 
 */
function local_fuzzylogic_add_criteria ($criteria) {
    global $DB;
    
    return $DB->insert_record('fuzzylogic_criteria', $criteria);

}


/**
 * Delete criteria.
 * @param int $criteriaid
 * @param Boolean $cascade Borra en cascada las instacias especificas del criterio asociadas a una estructura
 * @return void
 */
function local_fuzzylogic_delete_criteria ($criteriaid) {
    global $DB;
    if (!$criteriaid) return false;
    $DB->delete_records('fuzzylogic_criteriaentry', array('id'=>$criteriaid));
    $DB->delete_records('fuzzylogic_criteriarule', array('criteriaid'=>$criteriaid));
    $DB->delete_records('fuzzylogic_assessment', array('instanceid'=>$criteriaid, 'assesmenttype'=>'criteria'));
    $DB->delete_records('fuzzylogic_criteria', array('id'=>$criteriaid));

}

/**
 * Check if criteria exists.
 * @param array $criteria
 * @return Boolean 
 */
function local_fuzzylogic_criteria_exists ($criteria) {
    global $DB;
  
    $where = 'structureid=? AND shortname=?';
    $params = array ($criteria['structureid'], $criteria['shortname'] );
    if (!empty($criteria['id'])) {
      $where .= ' AND id<>?';
      $params[]= $criteria['id'];
    }
    
    return $DB->record_exists_select('fuzzylogic_criteria', $where, $params);
}



function local_fuzzylogic_get_criteriaentries ($criteriaid=null, $entrytype){
  global $DB;

  if (empty($criteriaid)) return null;
  // Add some additional sensible conditions
  $tests = array();
  $params = array();
  
  if ( !empty($criteriaid) ) {
    $tests[] = 'criteriaid = ?';
    $params[] = $criteriaid;
  } 
  
  if ( !empty($entrytype) ) {
    $tests[] = 'entrytype = ?';
    $params[] = $entrytype;
  } 

  $wherecondition = implode(' AND ', $tests);

  $fields = "SELECT *";
  $countfields = "SELECT COUNT(1)";
  $sql = " FROM {fuzzylogic_criteriaentry}";
  $sql .= (empty($wherecondition)) ? "" : " WHERE $wherecondition";
  $totalentries = $DB->count_records_sql($countfields . $sql, $params);
  $entries = $DB->get_records_sql($fields . $sql, $params);
  
  return array( 'totalentries' => $totalentries, 'entries' => $entries);
}


/**
 * Get entries associates to a criteria
 * @param  int $criteriaid
 * @param  boolean $includecriterianame
 * @return stcClass $concepts 
 */
function local_fuzzylogic_get_criteriaentry ($criteriaid=0, $includecriterianame=false) {
  global $DB;
  if (!$criteriaid ) return 0;

  $criteriaentries = $DB->get_records('fuzzylogic_criteriaentry', array('$criteriaid'=>$criteriaid) );

  if ($includecriterianame === false) 
    return $criteriaentries;

  foreach ($criteriaentries as $criteriaentry) {

    $criteria = $DB->get_record('fuzzylogic_criteria', array('id'=>$criteriaid), 'id, shortname, name' );
    if ($criteria->id > 0) {
      $criteriaentry->name = $criteria->name;
      $criteriaentry->shortname = $criteria->shortname;
    }
  }
  
  
  return $criteriaentries;
  
}

/**
 * Update a entry associates to a criteria.
 * @param array $entry
 * @return Boolean 
 */
function local_fuzzylogic_update_criteriaentry ($entry) {
    global $DB;
    if (empty($entry->id)) return false;
    
    return $DB->update_record('fuzzylogic_criteriaentry', $entry);

}


/**
 * Add  a entry associates to a criteria.
 * @param array $entry
 * @return int $id 
 */
function local_fuzzylogic_add_criteriaentry ($entry) {
    global $DB;
    
    return $DB->insert_record('fuzzylogic_criteriaentry', $entry);

}

/**
 * Delete a entry associates to a criteria.
 * @param int $entryid
 * @return void
 */
function local_fuzzylogic_delete_criteriaentry ($entryid) {
    global $DB;
    if (!$entryid) return false;

    $DB->delete_records('fuzzylogic_criteriaentry', array('id'=>$entryid));
}



/**
 * Count criteria entries
 * @param  int $criteriaid
 * @param  string $entrytype
 * @return int 
 */
function local_fuzzylogic_count_criteriaentries ($criteriaid, $entrytype) {
  global $DB;
  if (!$criteriaid ) return 0;
  $params = array ();
  $params['criteriaid'] = $criteriaid;
  if ($entrytype != null)
    $params['entrytype'] = $entrytype;
  
  return $DB->count_records('fuzzylogic_criteriaentry', $params );
}

/**
 * Check if criteria entry exists.
 * @param stdClass $entry
 * @return Boolean 
 */
function local_fuzzylogic_logic_criteriaentry_exists ($entry) {
    global $DB;
    
    // Add some additional sensible conditions
    $tests = array();
    $params = array();
    
    if ( !empty($entry->id) ) {
      $tests[] = 'id <> ?';
      $params[] = $entry->id;
    } 
    

    $tests[] = 'criteriaid = ?';
    $params[] = $entry->criteriaid;

    $tests[] = 'entryid = ?';
    $params[] = $entry->entryid;


    $wherecondition = implode(' AND ', $tests);

    return $DB->record_exists_select('fuzzylogic_criteriaentry', $wherecondition, $params);
}

/**
 * Check if global criteria exists.
 * @param int $structureid
 * @return Boolean 
 */
function local_fuzzylogic_logic_globalcriteria_exists ($criteria) {
    global $DB;
    
    $where = 'global=? AND structureid=?';
    $params = array();
    $params[] = array( 1 );
    $params[] = array( $criteria->structureid );
    if ( !empty($criteria->id) ){
      $where .= ' AND id<>?';
      $params[] = $criteria->id;
    }

    return $DB->record_exists_select('fuzzylogic_criteria', $where, $params);
}


function local_fuzzylogic_get_criteriarules ($criteriaid=null){
  global $DB;

  if (empty($criteriaid)) return null;
  // Add some additional sensible conditions
  $tests = array();
  $params = array();
  
  if ( !empty($criteriaid) ) {
    $tests[] = 'criteriaid = ?';
    $params[] = $criteriaid;
  } 
  

  $wherecondition = implode(' AND ', $tests);

  $fields = "SELECT *";
  $countfields = "SELECT COUNT(1)";
  $sql = " FROM {fuzzylogic_criteriarule}
           WHERE $wherecondition";
  $totalrules = $DB->count_records_sql($countfields . $sql, $params);
  $rules = $DB->get_records_sql($fields . $sql, $params);
  
  return array( 'totalrules' => $totalrules, 'rules' => $rules);
}


/**
 * Get rules associates to a criteria
 * @param  int $criteriaid
 * @param  boolean $includecriterianame
 * @return stcClass $concepts 
 */
function local_fuzzylogic_get_criteriarule ($criteriaid=0, $includecriterianame=false) {
  global $DB;
  if (!$criteriaid ) return 0;

  $criteriarules = $DB->get_records('fuzzylogic_criteriarule', array('criteriaid'=>$criteriaid) );

  if ($includecriterianame === false) 
    return $criteriarules;

  foreach ($criteriarules as $criteriarule) {

    $criteria = $DB->get_record('fuzzylogic_criteria', array('id'=>$criteriaid), 'id, shortname, name' );
    if ($criteria->id > 0) {
      $criteriarule->name = $criteria->name;
      $criteriarule->shortname = $criteria->shortname;
    }
  }
  return $criteriarules;
  
}

/**
 * Update a rule associates to a criteria.
 * @param array $rule
 * @return Boolean 
 */
function local_fuzzylogic_update_criteriarule ($rule) {
    global $DB;
    if (empty($rule->id)) return false;
    
    return $DB->update_record('fuzzylogic_criteriarule', $rule);

}


/**
 * Add  a rule associates to a criteria.
 * @param array $rule
 * @return int $id 
 */
function local_fuzzylogic_add_criteriarule ($rule) {
    global $DB;
    return $DB->insert_record('fuzzylogic_criteriarule', $rule);

}

/**
 * Delete a rule associates to a criteria.
 * @param int $ruleid
 * @return void
 */
function local_fuzzylogic_delete_criteriarule ($ruleid) {
    global $DB;
    if (!$ruleid) return false;

    $DB->delete_records('fuzzylogic_criteriarule', array('id'=>$ruleid));
}


require_once($CFG->dirroot . '/cohort/lib.php');

/**
 * Count criteria rules
 * @param  int $criteriaid
 * @param  string $entrytype
 * @return int 
 */
function local_fuzzylogic_count_criteriarules ($criteriaid) {
  global $DB;
  if (!$criteriaid ) return 0;
  $params = array ();
  $params['criteriaid'] = $criteriaid;
  return $DB->count_records('fuzzylogic_criteriarule', $params );
}

function local_fuzzylogic_get_quiz2structure ($courseid) {
  global $DB;
  if (!$courseid ) return null;
  
  $structures = $DB->get_records('fuzzylogic_structure', array('courseid'=>$courseid) );
  
  $structure_concepts = array();
  foreach ($structures as $structure) {

    $qc = $DB->get_records_select( 'fuzzylogic_questionconcepts', 'structureid=? GROUP BY quizid,structureid', array($structure->id) );
    $structure_concepts = array_merge($structure_concepts, $qc);
  }
  
 
  foreach ($structure_concepts as $structure_concept) {
    $structure_concept->quizname = $DB->get_field('quiz', 'name', array('id'=>$structure_concept->quizid) );
    $structure_concept->structurename = $DB->get_field('fuzzylogic_structure', 'name', array('id'=>$structure_concept->structureid) );
  }
  return $structure_concepts;
}



function local_fuzzylogic_quiz_attempt_submitted_handler($eventdata) {
  global $DB;
  local_fuzzylogic_generate_results ($eventdata->userid, $eventdata->quizid, $eventdata->attemptid);
  return true;
}

function local_fuzzylogic_validatestructure($structureid, $returnboolean=true, $checkquestions=true ) {
  global $DB;
  
  $return = array();
  
  $concepts = $DB->get_records('fuzzylogic_structureconcepts', array('structureid'=>$structureid),'', 'conceptid');  
  
  if ( count($concepts) < 1) {
      if ($returnboolean) 
         return false; //Al menos tiene que haber 1 concepto
      else
         $return[] = get_string('validation_error_concept_count', 'local_fuzzylogic');
  }
  
  foreach($concepts as $concept){
      $assessmentscount = $DB->count_records('fuzzylogic_assessment', array('instanceid'=>$concept->conceptid, 'assesmenttype' => 'concept' ) );
      if($assessmentscount <= 0) {
          if ($returnboolean) 
            return false; //Al menos tiene que haber una valoraciÃƒÂ³n asociada al concepto
         else{
             $name = $DB->get_field('fuzzylogic_concepts', 'name', array('id'=>$concept->conceptid));
             $return[] = get_string('validation_error_concept_assesst_count', 'local_fuzzylogic', $name);
         }
            
      }
          
  }
  
  $criterias = $DB->get_records('fuzzylogic_criteria', array('structureid'=>$structureid),'', 'id, name, global');  
  
  if ( count($criterias) < 2) {
      if ($returnboolean) 
         return false; //CÃƒÂ³mo mÃƒÂ­nimo tiene que haber un criterio y el criterio global
      else
         $return[] = get_string('validation_error_criteria_count', 'local_fuzzylogic');
  }
  
  $global = 0;
  foreach($criterias as $criteria){
      $assessmentscount = $DB->count_records('fuzzylogic_assessment', array('instanceid'=>$criteria->id, 'assesmenttype' => 'criteria' ) );
      if($assessmentscount <= 0) {
         if ($returnboolean) 
            return false; //Al menos tiene que haber una valoraciÃƒÂ³n asociada al criterio
         else
            $return[] = get_string('validation_error_criteria_assest_count', 'local_fuzzylogic', $criteria->name);
      }
      if ($criteria->global == 1) $global++;
      
      $entriescount = $DB->count_records('fuzzylogic_criteriaentry', array('criteriaid'=>$criteria->id));
      if ($entriescount < 1) {
          if ($returnboolean) 
            return false; //Por lo menos debe de existir una entrada de criterio.
         else
            $return[] = get_string('validation_error_criteria_entry_count', 'local_fuzzylogic', $criteria->name);
      }
      
      $criteriarules = $DB->get_records('fuzzylogic_criteriarule', array('criteriaid'=>$criteria->id));
      
      $criteriarules_count = count ($criteriarules);
      if ( $criteriarules_count < 1 ) {
          if ($returnboolean) 
            return false; //Es necesario definir las reglas del criterio
         else
            $return[] = get_string('validation_error_criteria_rules', 'local_fuzzylogic', $criteria->name);
      }
      $consequents_count = 0;
      foreach($criteriarules as $criteriarule) {
          if ($criteriarule->consequent > 0) $consequents_count++;
      }
      if ($consequents_count != $criteriarules_count) {
          if ($returnboolean) 
            return false; //El numero de consecuentes establecido tiene que ser igual al nÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Âºmero de reglas.
         else
            $return[] = get_string('validation_error_consequents', 'local_fuzzylogic', $criteria->name);
      }
      
      
  }
  
  
  if ($global != 1) {
      if ($returnboolean) 
            return false; //Debe existir 1 y solo 1 criterio global
         else
            $return[] = get_string('validation_error_global_criteria_count', 'local_fuzzylogic');
  }
  
  if ( $checkquestions == true) {
    $question_concepts_count = $DB->count_records('fuzzylogic_questionconcepts', array('structureid'=>$structureid));
      
    if ($question_concepts_count < 1) {
        if ($returnboolean) 
          return false; //Como minimo tiene que existir una pregunta asociada a un concepto en la estructura
       else
          $return[] = get_string('validation_error_question_concepts_count', 'local_fuzzylogic');
    }
  }
  
    
  if ($returnboolean) 
    return true;
 else
    return $return;
}


function local_fuzzylogic_generate_results ($userid, $quizid, $attemptid=null){
  global $CFG, $DB, $COURSE, $USER;
  
  require_once(dirname(__FILE__).'/locallib.php');
  require_once($CFG->dirroot.'/mod/quiz/lib.php');
  require_once($CFG->dirroot.'/mod/quiz/attemptlib.php');
  require_once($CFG->dirroot.'/mod/quiz/accessmanager.php');
  


  if ( empty($attemptid) ) {  
    $quizattempt = quiz_attempt::create($attemptid);
    $attempts = quiz_get_user_attempts($quizid, $userid, 'all', true);
    end($attempts);
    $attemptid = key($attempts);
  }
  
  $quizattempt = quiz_attempt::create($attemptid);
  
  $slots = $quizattempt->get_slots();
  $courseid = $quizattempt->get_courseid();
  $questions = array();  
  foreach ($slots as $slot) {
      $q = new stdClass();
      $qa = $quizattempt->get_question_attempt($slot);
      $q->id = $qa->get_question()->id;
      $q->instanceid = $DB->get_field('quiz_question_instances', 'id', array('quiz'=>$quizid, 'question'=>$q->id ) );    
      $q->score = $qa->get_fraction();
      $q->score = ($q->score == NULL) ? 0 : $q->score;
      $q->max_score = $qa->get_max_mark(); 
      $questions[] = $q;
  }
 
  $structuresids = array_keys ($DB->get_records('fuzzylogic_structure', array('courseid'=>$courseid), '', 'id' ));
  
  //Retorno si no tiene asignado estructuras el curso
  if ( count($structuresids) == 0 ) return true;
  
  $questionconcepts = $DB->get_records_select('fuzzylogic_questionconcepts', 'structureid IN (' . implode(', ', $structuresids) . ') AND quizid=?', array($quizid) );

  $structures = array();
  foreach ($questionconcepts as $questionconcept) {
    if ( $structures[$questionconcept->structureid] == null) $structures[$questionconcept->structureid] = array();
    //
    foreach ($questions as $questionkey => $question) {
      if ($question->instanceid == $questionconcept->questioninstanceid ) {
        
        $structures[$questionconcept->structureid][] = clone $question;
        $lastqs = end ($structures[$questionconcept->structureid]);
   
        $lastqs->conceptid = $questionconcept->conceptid;
        break;
      }
    }
  }
  
  $context=get_context_instance(CONTEXT_COURSE, $COURSE->id);
  
  foreach ($structures as $structureid => $questions) {
    if (local_fuzzylogic_validatestructure($structureid) == false ) return;
    
    
    
    $algorithm = new local_fuzzylogic_algorithm($userid, $structureid, $questions, $attemptid); 
    $algorithm->execute();
    
    $valorations = array();
    $time = time();
    
    
    $concepts = $algorithm->concepts_valoration();
    foreach ($concepts as $concept){
      $valoration['instanceid'] = $concept->id;
      $valoration['type'] = FUZZYLOGIC_TYPE_CONCEPT;
      $valoration['bestassessmentid'] = $concept->best_assessment->id;
      $valoration['score'] = round($concept->G,2);
      
      $valorations[] = $valoration;
    }
    
    $totalconcepts = local_fuzzylogic_get_conceptstructure($structureid);
    
    $criterias = $algorithm->criterias_valoration();
    foreach ($criterias as $criteria){
      $valoration['instanceid'] = $criteria->id;
      $valoration['type'] = FUZZYLOGIC_TYPE_CRITERIA;
      $valoration['bestassessmentid'] = $criteria->best_assessment->id;
      $valoration['score'] = round($criteria->G,2);
      
      $valorations[] = $valoration;
    }
    
    $global_criteria = $algorithm->global_criteria_valoration();
    $criteria = reset($global_criteria);
    
    $valoration['instanceid'] = $criteria->id;
    $valoration['type'] = FUZZYLOGIC_TYPE_GLOBALCRITERIA;
    $valoration['bestassessmentid'] = $criteria->best_assessment->id;
    $valoration['score'] = round ($criteria->G, 2);
    
    
    $valorations[] = $valoration;
    
    foreach ($valorations as &$item) {
      $item['userid'] = $userid;
      $item['structureid'] = $structureid; 
      $item['attemptid'] = $attemptid;
      
      $resultobj = array();
      $resultobj['userid'] = $userid;
      $resultobj['structureid'] = $structureid; 
      $resultobj['attemptid'] = $attemptid;
      $resultobj['instanceid'] = $item['instanceid'];
      $resultobj['type'] = $item['type'];
      $id = $DB->get_field('fuzzylogic_results', 'id', $resultobj );
      
      $item['date'] = $time;
      
      if ( $item['score'] == NULL || $item['bestassessmentid'] == NULL ) continue;
          
      if ( !empty($id) ) {
        $item['id'] = $id;
        $DB->update_record('fuzzylogic_results', $item);
      } else {
        $DB->insert_record('fuzzylogic_results', $item);
      } 
      
      require_once("$CFG->libdir/gradelib.php");
      require_once("$CFG->libdir/grade/grade_item.php");
      
      $itemnumber = ($item['type'] == 1) ? 1 : 2;
      if (!$grade_item = grade_item::fetch(array('itemnumber'=>$itemnumber, 'courseid'=>$courseid, 'iteminstance'=>$item['instanceid']))) {
        continue;
      }
      
     $bestassessment = $DB->get_record('fuzzylogic_assessment', array('id'=>$item['bestassessmentid'])); 
     $feedback = "{$bestassessment->feedback}";
      $grade_item->update_raw_grade($item['userid'], $item['score'], null, $feedback, FORMAT_MOODLE, null, $time, $time);     
     
    
    }
    
    //Eliminamos todos las entradas del intento que no se hayan actualizado.
    $maxresultdate = $DB->get_field('fuzzylogic_results', 'MAX(date)', array('structureid'=>$structureid, 'attemptid'=>$attemptid) );
    $DB->delete_records_select('fuzzylogic_results', 'structureid =? AND attemptid=? AND date<?', array($structureid,$attemptid,$maxresultdate));
    
    
  }

} 


function local_fuzzylogic_delete_grade_structure ($structureid) {
    global $CFG, $DB;
 
    //TODO: Borrar categorias
    
    $structure = local_fuzzylogic_get_structure($structureid);
    
    if ( empty ($structure) ) return;
    
    require_once("$CFG->libdir/grade/grade_category.php");
    
    $current_grade_category = grade_category::fetch(array('categoryid'=>$grade_category->id,'itemtype'=>'local/fuzzylogic', 'courseid'=>$structure->courseid));
}

function local_fuzzylogic_create_grade_structure ($structureid) {
    global $CFG, $DB;
    if (!local_fuzzylogic_validatestructure($structureid)) return;
    
    $structure = local_fuzzylogic_get_structure($structureid);
    
    if ( empty ($structure) ) return;
    
    require_once("$CFG->libdir/grade/grade_category.php");
    require_once("$CFG->libdir/grade/grade_item.php");
    require_once("$CFG->libdir/gradelib.php");
    
    $grade_category = $DB->get_record_select('grade_categories', "fullname LIKE ? AND courseid=? AND depth=2", array("[{$structure->id}]%", $structure->courseid ));
    

    
    if (empty($grade_category)) {
        $grade_category = new grade_category();
        $grade_category->courseid = $structure->courseid;
        $grade_category->fullname = "[{$structure->id}] {$structure->name}";
        $grade_category->insert();
    } else {
        $gc = new grade_category();
        $gc->id = $grade_category->id;
        $gc->courseid = $structure->courseid;
        $gc->fullname = "[{$structure->id}] {$structure->name}";
        $gc->timecreated = $grade_category->timecreated;
        $gc->timemodified = time();
        $gc->parent = 3;
        $gc->update();
    }
    
    $current_grade_items = grade_item::fetch_all(array('categoryid'=>$grade_category->id,'itemtype'=>'local/fuzzylogic', 'courseid'=>$structure->courseid));
    
    $concepts = local_fuzzylogic_get_conceptstructure($structureid, true);
    
    foreach ($concepts as $concept) {
      $params['itemname'] = $concept->name;
      $params['gradetype'] = GRADE_TYPE_VALUE;
      $params['grademax']  = 1;
      $params['grademin']  = 0;
      $params['idnumber'] = $concept->shorname;
      $params['categoryid'] = $grade_category->id;
      
      grade_update('local/fuzzylogic', $structure->courseid, 'mod', 'local_fuzzylogic', $concept->conceptid, 1, null, $params);
    }
    
    $criterias = local_fuzzylogic_get_criterias($structureid, true, 0, 1000);
    $globalcriteria = $criterias['globalcriteria'];
    $criterias = $criterias['criterias'];
    
    foreach ($criterias as $criteria) {
      $params['itemname'] = $criteria->name;
      $params['gradetype'] = GRADE_TYPE_VALUE;
      $params['grademax']  = 1;
      $params['grademin']  = 0;
      $params['idnumber'] = $criteria->shorname;
      $params['categoryid'] = $grade_category->id;
      
      grade_update('local/fuzzylogic', $structure->courseid, 'mod', 'local_fuzzylogic', $criteria->id, 2, null, $params);
    }
    
    $params['itemname'] = $globalcriteria->name;
    $params['gradetype'] = GRADE_TYPE_VALUE;
    $params['grademax']  = 1;
    $params['grademin']  = 0;
    $params['idnumber'] = $globalcriteria->shorname;
    $params['categoryid'] = $grade_category->id;

    grade_update('local/fuzzylogic', $structure->courseid, 'mod', 'local_fuzzylogic', $globalcriteria->id, 2, null, $params);
    
    
}

function local_fuzzylogic_get_xml_structure ($sourcestrctureid){
  global $DB;
  $fuzzylogic_node = '<?xml version="1.0" encoding="UTF-8"?>';
  
  $structure = local_fuzzylogic_get_structure($sourcestrctureid);   
  //Create fuzzylogic_structure
  if (empty ($structure->id) ) return false;
  
  $node = local_fuzzylogic_createnode('name', $structure->name, true);
  $node .= local_fuzzylogic_createnode('description', $structure->description, true);
  
  $structurenode = local_fuzzylogic_createnode('structure', $node);
  
  //Create fuzzylogic_concepts and fuzzylogic concepts_structure
  $conceptstructures = local_fuzzylogic_get_conceptstructure($sourcestrctureid);
  
  $concepts_node .= '';
  foreach($conceptstructures as $conceptstructure) {
      
      $concept = local_fuzzylogic_get_concept($conceptstructure->conceptid);
      $conceptnode = local_fuzzylogic_createnode('name', $concept->name, true);
      $conceptnode .= local_fuzzylogic_createnode('shortname', $concept->shortname, true);
      $conceptnode .= local_fuzzylogic_createnode('description', $concept->description, true);
           
      $conceptassesments = local_fuzzylogic_get_assessments($conceptstructure->conceptid, 'concept', false, array('id'=>$conceptstructure->conceptid));
      $assesments_node = '';
      //Create conceptassessment
      foreach ($conceptassesments as $conceptassesment) {
          $assesmentnode = local_fuzzylogic_createnode( 'linguistictag', $conceptassesment->linguistictag, true);
          $assesmentnode .= local_fuzzylogic_createnode('feedback', $conceptassesment->feedback, true);       
          $assesments_node .= local_fuzzylogic_createnode('assesment', $assesmentnode, false, array(
                                                          'id'=> $conceptassesment->id,
                                                          'a'=> $conceptassesment->param_a,
                                                          'b'=> $conceptassesment->param_b,
                                                          'c'=> $conceptassesment->param_c,
                                                          'd'=> $conceptassesment->param_d,
                                                          'e'=> $conceptassesment->param_e,
                                                          'type'=> $conceptassesment->assesmenttype
                                                        )); 
      }
      
      $conceptnode .= local_fuzzylogic_createnode('assesments', $assesments_node); 
      
      $concepts_node .= local_fuzzylogic_createnode('concept', $conceptnode, false, array ('id'=>$conceptstructure->id));
  }
  
  $concepts_node = local_fuzzylogic_createnode('concepts', $concepts_node);
  
  //Create fuzzylogic_criteria
  $criterias = local_fuzzylogic_get_criterias($sourcestrctureid);
  //Agrego criterio global a lista de criterios
  $criterias['globalcriteria']->isGlobal = true;
  $criterias['criterias'][] = $criterias['globalcriteria'];
  
  $criterias_node = '';
 
  foreach($criterias['criterias'] as $criteria){
   $isGlobal = ($criteria->isGlobal === true) ? 'true' : 'false';  
   $criterianode = local_fuzzylogic_createnode('name', $criteria->name, true);
   $criterianode .= local_fuzzylogic_createnode('shortname', $criteria->shortname, true);
   $criterianode .= local_fuzzylogic_createnode('description', $criteria->description, true);
   
   $criteriassessments = local_fuzzylogic_get_assessments($criteria->id, 'criteria');
   $assesments_node = '';
   //Create criteriaassessment
    foreach ($criteriassessments as $criteriassessment) {
          $assesmentnode = local_fuzzylogic_createnode( 'linguistictag', $criteriassessment->linguistictag, true);
          $assesmentnode .= local_fuzzylogic_createnode('feedback', $criteriassessment->feedback, true);       
          $assesments_node .= local_fuzzylogic_createnode('assesment', $assesmentnode, false, array(
                                                          'id'=> $criteriassessment->id,
                                                          'a'=> $criteriassessment->param_a,
                                                          'b'=> $criteriassessment->param_b,
                                                          'c'=> $criteriassessment->param_c,
                                                          'd'=> $criteriassessment->param_d,
                                                          'e'=> $criteriassessment->param_e,
                                                          'type'=> $criteriassessment->assesmenttype
                                                        )); 
      }
    
    $criterianode .= local_fuzzylogic_createnode('assesments', $assesments_node); 
    
    //Create criteria_entry
    $criteria_entries = local_fuzzylogic_get_criteriaentries($criteria->id, ($isGlobal == 'true') ? 'criteria' : 'concept');
    $entries_node = '';
    foreach($criteria_entries['entries'] as $criteriaentry) {
        $entries_node .= local_fuzzylogic_createnode('entry', null, false, array(
                                                                                    'id'=>$criteriaentry->id,
                                                                                    'entryid'=>$criteriaentry->entryid, 
                                                                                    'type'=>$criteriaentry->entrytype
                                                                                ));
    }
    $criterianode .= local_fuzzylogic_createnode('entries', $entries_node);
    
    //Create criteria_rules
    $criteria_rules = local_fuzzylogic_get_criteriarules($criteria->id);
    $rules_node = '';    
    foreach($criteria_rules['rules'] as $criteria_rule){
        $rules_node .= local_fuzzylogic_createnode('rule', $criteria_rule->antecedents, true, array (
                                                                                'consequent' => $criteria_rule->consequent
                                                                                ));
    }
    
    $criterianode .= local_fuzzylogic_createnode('rules', $rules_node);
    
    $criterias_node .= local_fuzzylogic_createnode('criteria', $criterianode, false, array (
                                                                                             'id'=>$criteria->id, 
                                                                                             'global'=>$isGlobal, 
                                                                                             't_norma'=> $criteria->t_norma,
                                                                                             't_conorma'=> $criteria->t_conorma
                                                                                            ));
  }
  
  $criterias_node = local_fuzzylogic_createnode('criterias', $criterias_node);

  $fuzzylogic_node .=  local_fuzzylogic_createnode('fuzzylogic', $structurenode . $concepts_node . $criterias_node);
  return $fuzzylogic_node;
}


function local_fuzzylogic_import_xml_structure ($courseid, $data){
    global $DB;
    
    $csmapping  = array();
    $criteriamapping  = array();
    $assesstmentmapping  = array();
    $criteriaentiresmapping  = array();
    
    $structure = new stdClass();
    
    $structure_node = $data->STRUCTURE;
    $concepts_node = $data->CONCEPTS;
    $criterias_node = $data->CRITERIAS;
    
    
    $structure->name = (string) $structure_node->NAME;
    $structure->description = (string) $structure_node->DESCRIPTION;
    $structure->courseid = $courseid;
    $structure->id = local_fuzzylogic_add_structure($structure);
    
    $structure_node->addAttribute( 'courseid', $courseid );
    $structure_node->addAttribute( 'id', $structure->id );
    

    
    foreach ($concepts_node->CONCEPT as $nodeconcept) {
        $concept = new stdClass();
        $concept->courseid =  $courseid;
        $concept->name = (string) $nodeconcept->NAME;
        $concept->shortname = local_fuzzylogic_generateshortname ( (string) $nodeconcept->SHORTNAME, 'concept');
        $concept->description = (string) $nodeconcept->DESCRIPTION;
        
        try {
             $concept->id = local_fuzzylogic_add_concept($concept);
        } catch (Exception $exc) {
              $concept = $DB->get_record('fuzzylogic_concepts', array('shortname'=>$concept->shortname, 'courseid'=>$coursetargetid));
        }
        
        if ( empty($concept->id) ) return false;
        
        $conceptstructure = new stdClass();
        $conceptstructure->structureid = $structure->id;
        $conceptstructure->conceptid = $concept->id;
        
        $conceptstructure->id = local_fuzzylogic_add_conceptstructure($conceptstructure);
        $nodeconcept->addAttribute( 'conceptstructureid', $conceptstructure->id );
        $csmapping[(int) $nodeconcept['id']] = $conceptstructure->id;

        foreach ($nodeconcept->ASSESMENTS->ASSESMENT as $nodeassesment) {
            $assesment = new stdClass();
            $assesment->instanceid = $concept->id;
            $assesment->linguistictag = (string) $nodeassesment->LINGUISTICTAG;
            $assesment->feedback = (string) $nodeassesment->FEEDBACK;
            $assesment->param_a = (string) $nodeassesment['a'];
            $assesment->param_b = (string) $nodeassesment['b'];
            $assesment->param_c = (string) $nodeassesment['c'];
            $assesment->param_d = (string) $nodeassesment['d'];
            $assesment->param_e = (string) $nodeassesment['e'];
            $assesment->assesmenttype = (string) $nodeassesment['type'];
              
            $assesment->id = local_fuzzylogic_add_assessment($assesment);
            if ( empty($assesment->id) ) return false;
            $assesstmentmapping[(int) $nodeassesment['id']] = $assesment->id;
        }
       
    } 
    
    foreach ($criterias_node->CRITERIA as $nodecriteria) {
        $criteria = new stdClass();
        $criteria->name = (string) $nodecriteria->NAME;
        $criteria->shortname = local_fuzzylogic_generateshortname ( (string) $nodecriteria->SHORTNAME, 'criteria');
        $criteria->description = (string) $nodecriteria->DESCRIPTION;
        $criteria->global = ( (string) $nodecriteria['global'] == 'true') ? 1 : 0;
        $criteria->t_norma = (int) $nodecriteria['t_norma'];
        $criteria->t_conorma = (int) $nodecriteria['t_conorma'];
        $criteria->structureid = $structure->id;
              
        try {
            $criteria->id = local_fuzzylogic_add_criteria($criteria);
        } catch (Exception $exc) {
             $criteria = $DB->get_record('fuzzylogic_criteria', array('shortname'=>$concept->shortname, 'structureid'=>$structure->id));
        }
        if ( empty($criteria->id) ) return false;
        $criteriamapping[ (int) $nodecriteria['id']] = $criteria->id;
        
        foreach ($nodecriteria->ASSESMENTS->ASSESMENT as $nodeassesment) {
            $assesment = new stdClass();
            $assesment->instanceid = $criteria->id;
            $assesment->linguistictag = (string) $nodeassesment->LINGUISTICTAG;
            $assesment->feedback = (string) $nodeassesment->FEEDBACK;
            $assesment->param_a = (string) $nodeassesment['a'];
            $assesment->param_b = (string) $nodeassesment['b'];
            $assesment->param_c = (string) $nodeassesment['c'];
            $assesment->param_d = (string) $nodeassesment['d'];
            $assesment->param_e = (string) $nodeassesment['e'];
            $assesment->assesmenttype = (string) $nodeassesment['type'];
              
            $assesment->id = local_fuzzylogic_add_assessment($assesment);
            if ( empty($assesment->id) ) return false;
            $assesstmentmapping[(int) $nodeassesment['id']] = $assesment->id;
        }
        
        foreach($nodecriteria->ENTRIES->ENTRY as $nodeentry) {
            $criteriaentry = new stdClass();
            $criteriaentry->criteriaid = $criteria->id;
            $criteriaentry->entryid = ((string) $nodeentry['type'] == 'concept') ? $csmapping[ (int) $nodeentry['entryid']] : $criteriamapping[ (int) $nodeentry['entryid']];
            $criteriaentry->entrytype = (string) $nodeentry['type'];
            
            $criteriaentry->id = local_fuzzylogic_add_criteriaentry($criteriaentry);
            if ( empty($criteriaentry->id) ) return false;
            $criteriaentiresmapping[(int) $nodeentry['id']] = $criteriaentry->id;
        }
        
        foreach($nodecriteria->RULES->RULE as $noderule){
            $criteria_rule->criteriaid = $criteria->id;

            $elements = explode('&', (string) $noderule);
            foreach($elements as &$element){
                list($entry, $assesst) = explode('=', $element);
                $entry = $criteriaentiresmapping[$entry];
                $assesst = $assesstmentmapping[$assesst];
                $element = "$entry=$assesst";
            }

            $criteria_rule->antecedents = implode('&', $elements);
            $criteria_rule->consequent = $assesstmentmapping[ (int) $noderule['consequent']];
            $criteria_rule->hash = sha1((string) $criteria_rule->antecedents);
            $criteria_rule->id = local_fuzzylogic_add_criteriarule($criteria_rule);
            if ( empty($criteria_rule->id) ) return false;
        }
        
    }
    return true;
    
}

function local_fuzzylogic_duplicate_concept ($sourceconceptid, $courseid=null) {
    global $DB;
    $concept = local_fuzzylogic_get_concept($sourceconceptid);
    if ( empty($concept->id) ) return false;
    
    unset($concept->id);
    if ( !empty($courseid) ) $concept->courseid = $courseid;
    $concept->shortname = local_fuzzylogic_generateshortname ($concept->shortname, 'concept');
    
    try {
         $concept->id = local_fuzzylogic_add_concept($concept);
    } catch (Exception $exc) {
          return false;
    }

    if ( empty($concept->id) ) return false;


    $conceptassesments = local_fuzzylogic_get_assessments($sourceconceptid, 'concept');

    //Create conceptassessment
    foreach ($conceptassesments as $conceptassesment) {
        $sourceconceptassesment = $conceptassesment->id;
        unset($conceptassesment->id);
        $conceptassesment->instanceid = $concept->id;
        $conceptassesment->id = local_fuzzylogic_add_assessment($conceptassesment);
        if ( empty($conceptassesment->id) ) return false;         
    }

    return true;
    
}

function local_fuzzylogic_duplicate_criteria ($sourcecriteriaid, $structureid=null) {
   global $DB;
   
   $criteria = local_fuzzylogic_get_criteria($sourcecriteriaid);
   unset($criteria->id);
   $criteria->shortname = local_fuzzylogic_generateshortname ($criteria->shortname, 'criteria');
   if (!empty($structureid)) $criteria->structureid = $structurid; 
   try {
        $criteria->id = local_fuzzylogic_add_criteria($criteria);
   } catch (Exception $exc) {
         return false;
   }
   if ( empty($criteria->id) ) return false;
      
   $criteriassessments = local_fuzzylogic_get_assessments($sourcecriteriaid, 'criteria');
   $assesstmentmapping = array();
   //Create criteriaassessment
    foreach ($criteriassessments as $criteriassessment) {
        $sourcecriteriassessment = $criteriassessment->id;
        unset($criteriassessment->id);
        $criteriassessment->instanceid = $criteria->id;
        $criteriassessment->id = local_fuzzylogic_add_assessment($criteriassessment);
        if ( empty($criteriassessment->id) ) return false;
        $assesstmentmapping[$sourcecriteriassessment] = $criteriassessment->id;
    }
    
    $criteriaentiresmapping = array();
    //Create criteria_entry
    $criteria_entries = local_fuzzylogic_get_criteriaentries($sourcecriteriaid);
    
    foreach($criteria_entries['entries'] as $criteriaentry) {
        $sourcecriteriaentry = $criteriaentry->id; 
        unset($criteriaentry->id);
        $criteriaentry->criteriaid = $criteria->id;
        $criteriaentry->id = local_fuzzylogic_add_criteriaentry($criteriaentry);
        if ( empty($criteriaentry->id) ) return false;
        $criteriaentiresmapping[$sourcecriteriaentry] = $criteriaentry->id;
    }
    
    //Create criteria_rules
    $criteria_rules = local_fuzzylogic_get_criteriarules($sourcecriteriaid);
    
    foreach($criteria_rules['rules'] as $criteria_rule){
        unset($criteria_rule->id);
        $criteria_rule->criteriaid = $criteria->id;
        
        $elements = explode('&',$criteria_rule->antecedents);
        foreach($elements as &$element){
            list($entry, $assesst) = explode('=', $element);
            $entry = $criteriaentiresmapping[$entry];
            $element = "$entry=$assesst";
        }
        
        $criteria_rule->antecedents = implode('&', $elements);
        $criteria_rule->hash = sha1($criteria_rule->antecedents);
        $criteria_rule->consequent = $assesstmentmapping[$criteria_rule->consequent];
        $criteria_rule->id = local_fuzzylogic_add_criteriarule($criteria_rule);
        if ( empty($criteria_rule->id) ) return false;
    }

  return true;
}



function local_fuzzylogic_generateshortname ($shortname, $instance) {
    global $DB;
    $table = ($instance == 'concept') ? 'fuzzylogic_concepts' : 'fuzzylogic_criteria';
    
    if ( !$DB->record_exists($table, array('shortname'=>$shortname) ) ) {
        return $shortname;
    } else {
       $aux = explode('_', $shortname);
       if (count($aux) > 1 && is_numeric($aux[count($aux)-1]) ) {
           $aux[count($aux)-1] += 1;
           $shortname = implode ('_', $aux);
       } else {
           $shortname .= '_1';
       } 

        return local_fuzzylogic_generateshortname($shortname, $instance);
    }
   
}

function local_fuzzylogic_createnode ($tag, $content = null, $cdata=false, $attributes = array()) {
    $tag = strtoupper($tag);
    $attr = '';
    if (count($attributes) > 0) {
        foreach ($attributes as $k=>$v) {
            $attr .= " $k=\"$v\"";
        }
    }
    if (empty ($content) ) return "<$tag $attr/>";
    $content = ($cdata) ? "<![CDATA[$content]]>" : $content;
    return "<$tag $attr>$content</$tag>";
}
