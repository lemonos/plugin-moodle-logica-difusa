<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Results Page
 *
 * @package    local_fuzzylogic
 * @copyright  2013 Oscar Ruesga <oscar@ruesga.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once (dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/lib.php');
require_once(dirname(__FILE__).'/locallib.php');

define('FUZZYLOGIC_ACTION_REPORT_SHOW_USERS', 1);
define('FUZZYLOGIC_ACTION_REPORT_USER',       2);
define('FUZZYLOGIC_ACTION_REPORT_OWN_USER',   3);

$contextid = required_param('cid', PARAM_INT);
$structureid  = required_param('sid', PARAM_INT );
$userid  = optional_param('uid', 0, PARAM_INT );

list($context, $course, $cm) = get_context_info_array($contextid);
require_login($course, true);

$structure = $DB->get_record('fuzzylogic_structure', array('id'=>$structureid));

if ($userid == 0) {
    require_capability('local/fuzzylogic:viewreport', $context);

    $action = FUZZYLOGIC_ACTION_REPORT_SHOW_USERS;
    $urlparams = array('cid' => $contextid, 'sid'=>$structureid);
    $title = get_string('results', 'local_fuzzylogic');
    
    
} else {
    if ( has_capability('local/fuzzylogic:viewownreport', $context) ) {
        $action = FUZZYLOGIC_ACTION_REPORT_USER;   
        $urlparams = array('cid' => $contextid, 'sid'=>$structureid, 'uid'=>$userid);
        $title = get_string('results', 'local_fuzzylogic');
    } else if( has_capability('local/fuzzylogic:viewreport', $context) ) {
        $action = FUZZYLOGIC_ACTION_REPORT_OWN_USER;
        $urlparams = array('cid' => $contextid, 'sid'=>$structureid, 'uid'=>$userid);
        $title = get_string('results', 'local_fuzzylogic');
    } else {
        require_capability('local/fuzzylogic:viewownreport', $context);
    }
}

$PAGE->set_url(new moodle_url('/local/fuzzylogic/report.php', $urlparams));
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->set_pagelayout('standard');
 
echo $OUTPUT->header();
echo $OUTPUT->heading( $structure->name );
        
echo $OUTPUT->box($structure->description, 'generalbox boxwidthnormal boxaligncenter');
echo '<br />';

switch ($action) {
   case  FUZZYLOGIC_ACTION_REPORT_SHOW_USERS:
        //selecciono el intento mayor de la estructura
        $attemptid = $DB->get_field('fuzzylogic_results', 'MAX(attemptid)', array('structureid'=>$structureid)); 

        $usersids = $DB->get_records_select('fuzzylogic_results', 'structureid=? AND attemptid=? GROUP BY userid', array($structureid, $attemptid), '', 'userid');
        
        $data = array();
        
        foreach($usersids as $userid) {
             $user = $DB->get_record('user', array('id'=>$userid->userid));
             
             $line = array();
             $line[] = format_string($user->firstname);
             $line[] = format_string($user->lastname);


             $buttons = array();
             $buttons[] = html_writer::link(new moodle_url('/local/fuzzylogic/report.php', array('cid'=>$contextid, 'sid'=>$structureid,'uid'=>$user->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('i/grades'), 'alt'=>get_string('user_reports', 'local_fuzzylogic'), 'class'=>'iconsmall')));
             
             if (has_capability('local/fuzzylogic:reevaluate', $context)) {
              $buttons[] = html_writer::link(new moodle_url('/local/fuzzylogic/report.php', array('cid'=>$contextid, 'sid'=>$structureid,'uid'=>$user->id,'r'=>1)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/calc'), 'alt'=>get_string('reevaluate', 'local_fuzzylogic'), 'class'=>'iconsmall')));
             }
             
            if (has_capability('local/fuzzylogic:viewlog', $context)) {
              $buttons[] = html_writer::link(new moodle_url('/local/fuzzylogic/log.php', array('cid'=>$contextid, 'sid'=>$structureid,'uid'=>$user->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('i/agg_sum'), 'alt'=>get_string('viewlog', 'local_fuzzylogic'), 'class'=>'iconsmall')));
            }
              $line[] = implode(' ', $buttons);

             $data[] = $line;
         }
         $table = new html_table();
         $table->head  = array(get_string('firstname'), get_string('lastname'),'');
         $table->size  = array('45%', '45%', '10%');
         $table->align = array('left', 'left', 'center');
         $table->width = '80%';
         $table->data  = $data;
         echo html_writer::table($table);
       
       
       break;
           
   case FUZZYLOGIC_ACTION_REPORT_USER:
   case FUZZYLOGIC_ACTION_REPORT_OWN_USER:

        //selecciono el intento mayor de la estructura
        $attemptid = $DB->get_field('fuzzylogic_results', 'MAX(attemptid)', array('structureid'=>$structureid)); 
       
       $reevaluate  = optional_param('r', 0, PARAM_BOOL );
       if ($reevaluate==1) {
            require_capability('local/fuzzylogic:reevaluate', $context);
            
            $quizid = $DB->get_field('fuzzylogic_questionconcepts', 'quizid', array('structureid'=>$structureid));
            local_fuzzylogic_generate_results ($userid, $quizid, $attemptid);
        }

        $results = $DB->get_records('fuzzylogic_results', array('structureid'=>$structureid, 'attemptid'=>$attemptid));

        if ( local_fuzzylogic_validatestructure( $structureid ) ) {
            
            $quizid = $DB->get_field('fuzzylogic_questionconcepts', 'quizid', array('structureid'=>$structureid), IGNORE_MULTIPLE);
            $moduleid = get_coursemodule_from_instance('quiz', $quizid, $course->id);
            
            $url = new moodle_url( '/mod/quiz/view.php', array('id'=>$moduleid->id) );
            echo  $OUTPUT->single_button($url,
                    get_string('quiz_access', 'local_fuzzylogic'), 'get',
                    array('class' => 'continuebutton'));
            if ( count($results) <= 0 ) {
                echo get_string('quiz_not_evaluate','local_fuzzylogic');
                echo $OUTPUT->footer();
                die;
            }
        } else {
            echo get_string('quiz_not_available','local_fuzzylogic');
            echo $OUTPUT->footer();
            die;
        }
        
        
        
        
        $result_concepts = $result_criterias = $result_global_criteria = array();
        foreach ($results as $result) {
          switch ($result->type){
              case FUZZYLOGIC_TYPE_CONCEPT:
                  $result_concepts[] = $result;
                  break;
              case FUZZYLOGIC_TYPE_CRITERIA:
                  $result_criterias[] = $result;
                  break;
              case FUZZYLOGIC_TYPE_GLOBALCRITERIA:
                  $result_global_criteria[] = $result;
                  break;
          }  
        }
        
               
       echo $OUTPUT->heading(get_string('concepts', 'local_fuzzylogic'));
       $data = array();

        foreach($result_concepts as $result_concept) {
             $concept = $DB->get_record('fuzzylogic_concepts', array('id'=>$result_concept->instanceid));
             
             $assesstment = $DB->get_record('fuzzylogic_assessment', array('id'=>$result_concept->bestassessmentid), 'linguistictag, feedback');
             $line = array();
             $line[] = $concept->name . ' [' . $concept->shortname . ']';
             $line[] = $result_concept->score;
             $line[] = $assesstment->linguistictag;
             $line[] = $assesstment->feedback;

             $data[] = $line;
         }
         $table = new html_table();
         $table->head  = array(get_string('concept', 'local_fuzzylogic'), get_string('score', 'local_fuzzylogic'), get_string('linguistictag', 'local_fuzzylogic'), get_string('feedback', 'local_fuzzylogic'));
        $table->size  = array('30%', '10%', '30%','30%');
        $table->align = array('left', 'center', 'left', 'left');
         $table->width = '80%';
         $table->data  = $data;
         echo html_writer::table($table);
         
       echo $OUTPUT->heading(get_string('criterias', 'local_fuzzylogic'));
       
       $data = array();

        foreach($result_criterias as $result_criteria) {
             $criteria = $DB->get_record('fuzzylogic_criteria', array('id'=>$result_criteria->instanceid));
             $assesstment = $DB->get_record('fuzzylogic_assessment', array('id'=>$result_criteria->bestassessmentid), 'linguistictag, feedback');
             $line = array();
             $line[] = $criteria->name . ' [' . $criteria->shortname . ']';
             $line[] = $result_criteria->score;
             $line[] = $assesstment->linguistictag;
             $line[] = $assesstment->feedback;

             $data[] = $line;
         }
        $table = new html_table();
        $table->head  = array(get_string('criteria', 'local_fuzzylogic'), get_string('score', 'local_fuzzylogic'), get_string('linguistictag', 'local_fuzzylogic'), get_string('feedback', 'local_fuzzylogic'));
        $table->size  = array('30%', '10%', '30%','30%');
        $table->align = array('left', 'center', 'left', 'left');
        $table->width = '80%';
        $table->data  = $data;
        echo html_writer::table($table);

        echo $OUTPUT->heading(get_string('globalcriteria', 'local_fuzzylogic'));
        $data = array();

        $resultcriteria = reset($result_global_criteria);
        $criteria = $DB->get_record('fuzzylogic_criteria', array('id'=>$resultcriteria->instanceid));
        $assesstment = $DB->get_record('fuzzylogic_assessment', array('id'=>$resultcriteria->bestassessmentid), 'linguistictag, feedback');
        $line = array();
        $line[] = $criteria->name . ' [' . $criteria->shortname . ']';
        $line[] = $resultcriteria->score;
        $line[] = $assesstment->linguistictag;
        $line[] = $assesstment->feedback;

        $data[] = $line;
    
        $table = new html_table();
        $table->head  = array(get_string('globalcriteria', 'local_fuzzylogic'), get_string('score', 'local_fuzzylogic'), get_string('linguistictag', 'local_fuzzylogic'), get_string('feedback', 'local_fuzzylogic'));
        $table->size  = array('30%', '10%', '30%','30%');
        $table->align = array('left', 'center', 'left', 'left');
        $table->width = '80%';
        $table->data  = $data;
        echo html_writer::table($table);
         
        
       break;

}
echo $OUTPUT->footer();