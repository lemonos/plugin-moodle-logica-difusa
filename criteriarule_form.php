<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The form used at the criteria rule editor page is defined here
 *
 * @package    local_fuzzylogic
 * @copyright  2013 Oscar Ruesga <oscar@ruesga.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/lib/formslib.php');


/**
 * Defines the criterias rules edit form
 *
 * @package    local_fuzzylogic
 * @copyright  2013 Oscar Ruesga <oscar@ruesga.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class local_fuzzylogic_criteriarule_form extends moodleform {

    /**
     * Form element definition
     */
    public function definition() {
        $form = $this->_form;
        
        $criteria               = $this->_customdata['criteria']; 

        $opt_criteria_assess =  $this->_customdata['opt_criteria_assess']; 
        $opt_criteria_assess = array (0=>get_string('selectconsequent', 'local_fuzzylogic')) + $opt_criteria_assess;
        $criteriarules          = $this->_customdata['criteriarules']; 
        
        $form->addElement('hidden', 'cid', $this->_customdata['cid']);
        $form->setType('cid', PARAM_INT);
           
        $form->addElement('hidden', 'criteriaid', $criteria->id );
        $form->setType('criteriaid', PARAM_INT);
        
        $form->addElement('hidden', 'g', $this->_customdata['global']);
        $form->setType('g', PARAM_BOOL);
        
        
        
        
        
        //General
        $form->addElement('header', 'criteriashdr', get_string('general'));
         
        // t-norma.
        $opt_tnorma = array(
                              0=>get_string('selecttnorma', 'local_fuzzylogic'),
                              FUZZYLOGIC_TNORMA_LUKASIEWICZ=>get_string('lukasiewicz', 'local_fuzzylogic'),
                              FUZZYLOGIC_TNORMA_PRODUCTO=>get_string('tnorma_producto', 'local_fuzzylogic'),
                              FUZZYLOGIC_TNORMA_MINIMO=>get_string('tnorma_minimo', 'local_fuzzylogic'),
                            );
        $form->addElement('select', 'tnorma', get_string('tnorma', 'local_fuzzylogic'), $opt_tnorma);
        $form->setDefault('tnorma', $criteria->t_norma);
        $form->setType('tnorma', PARAM_INT);
        
        // t-conorma.

        $opt_tconorma = array(
                              0=>get_string('selecttconorma', 'local_fuzzylogic'),
                              FUZZYLOGIC_TCONORMA_LUKASIEWICZ=>get_string('lukasiewicz', 'local_fuzzylogic'),
                              FUZZYLOGIC_TCONORMA_SUMAPRODUCTO=>get_string('tconorma_sumaproducto', 'local_fuzzylogic'),
                              FUZZYLOGIC_TCONORMA_MAXIMO=>get_string('tconorma_maximo', 'local_fuzzylogic'),
                            );
        $form->addElement('select', 'tconorma', get_string('tconorma', 'local_fuzzylogic'), $opt_tconorma);
        $form->setDefault('tconorma', $criteria->t_conorma);
        $form->setType('tconorma', PARAM_INT);
        
         // Rules.
        
        $counter = 0;
        foreach ($criteriarules as $criteriarule){
          $form->addElement('header', 'criteriarule_'.$counter.'hdr', get_string('rule', 'local_fuzzylogic', ($counter+1) ));
          $form->addElement('hidden', 'criteriarule_id['.$counter .']', $criteriarule->id);
          $form->setType('criteriarule_id['.$counter .']', PARAM_INT);

          $form->addElement('static', 'criteriarule_antecedent_'.$counter .'_tag', get_string('antecedent', 'local_fuzzylogic'), '<pre>' . $criteriarule->humanphrase . '</pre>');
          
          $form->addElement('select', 'criteriarule_consequent['.$counter .']', get_string('consequent', 'local_fuzzylogic'), $opt_criteria_assess);
          $form->setType('criteriarule_consequent['.$counter .']', PARAM_INT);
          $form->setDefault('criteriarule_consequent['.$counter .']', $criteriarule->consequent);
          
          $counter++;
        }
        

        $this->add_action_buttons();
        
        $this->set_data($criteria);
    }

    /**
     * Setup the form depending on current values. This method is called after definition(),
     * data submission and set_data().
     * All form setup that is dependent on form values should go in here.
     *
     * We remove the element status if there is no current status (i.e. guide is only being created)
     * so the users do not get confused
     */
    public function definition_after_data() {
       
    }

    /**
     * Form validation.
     * If there are errors return array of errors ("fieldname"=>"error message"),
     * otherwise true if ok.
     *
     * @param array $data array of ("fieldname"=>value) of submitted data
     * @param array $files array of uploaded files "element_name"=>tmp_file_path
     * @return array of "element_name"=>"error_description" if there are errors,
     *               or an empty array if everything is OK (true allowed for backwards compatibility too).
     */
    public function validation($data, $files) {
      global $DB;  
      
      $err = parent::validation($data, $files);
      
      if ($data['tnorma']=="0")
        $err['tnorma'] = get_string('invalidselect', 'local_fuzzylogic');
      if ($data['tconorma']=="0")
        $err['tconorma'] = get_string('invalidselect', 'local_fuzzylogic');
      
      /*foreach ($data['criteriarule_consequent'] as $num=>$consequent) { 
        if ($consequent == "0"){
          $err['criteriarule_consequent['.$num.']'] = get_string('invalidselect', 'local_fuzzylogic');
        }
      }*/
 
      return $err;
    }

    /**
     * Return submitted data if properly submitted or returns NULL if validation fails or
     * if there is no submitted data.
     *
     * @return object submitted data; NULL if not valid or not submitted or cancelled
     */
    public function get_data() {
        $data = parent::get_data();

        return $data;
    }

}
