<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Concepts editor Page
 *
 * @package    local_fuzzylogic
 * @copyright  2013 Oscar Ruesga <oscar@ruesga.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


if (!defined('AJAX_SCRIPT')) {
    define('AJAX_SCRIPT', true);
}

require_once (dirname(dirname(dirname(__FILE__))).'/config.php');

require_once(dirname(__FILE__).'/locallib.php');


$criteriaid = required_param('cid', PARAM_INT);
$globalcriteria = optional_param('global', false, PARAM_BOOL);
$scores = required_param('scores', PARAM_RAW);

$scores = json_decode ($scores);

$result = new stdClass();

$num_elements = count($scores);

if($num_elements <2 || $num_elements > 3){
        $result->success = false;
        $result->data = null; 
}


$data = local_fuzzylogic_algorithm::get_G_criteria_values($criteriaid, $scores, $globalcriteria);



$result->success = true;
$result->data = $data;

echo json_encode ($result);

?>
